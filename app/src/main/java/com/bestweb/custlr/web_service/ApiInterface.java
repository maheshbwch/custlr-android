package com.bestweb.custlr.web_service;

import com.bestweb.custlr.models.AddToCartModel;
import com.bestweb.custlr.utils.APIS;

import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Url;

public interface ApiInterface {

    //LOGIN API
    @POST(APIS.LOGIN_API)
    @Headers({"Content-Type: application/json;charset=UTF-8"})
    Call<String> getLoginResponse(@Body String jsonBody);

    //GET CATEGORIES HOME
    @POST(APIS.HOME_API)
    @Headers({"Content-Type: application/json;charset=UTF-8"})
    Call<String> getCategoriesHome(@Body String jsonBody);

    //GET PRODUCTS DETAILS
    @POST(APIS.PRODUCTS_DETAILS)
    @Headers({"Content-Type: application/json;charset=UTF-8"})
    Call<String> getProductsDetails(@Body String jsonBody);

    //ADD WISHLIST
    @POST(APIS.ADD_WISHLIST)
    @Headers({"Content-Type: application/json;charset=UTF-8"})
    Call<String> addToWishList(@Body String jsonBody);

    //ADD WISHLIST
    @POST(APIS.REMOVE_WISHLIST)
    @Headers({"Content-Type: application/json;charset=UTF-8"})
    Call<String> removeFromWishList(@Body String jsonBody);

    //GET PRODUCTS LIST
    @POST(APIS.PRODUCTS_LIST)
    @Headers({"Content-Type: application/json;charset=UTF-8"})
    Call<String> getProductsList(@Body String jsonBody);

    //REGISTER USER
    @POST(APIS.REGSITER_API)
    @Headers({"Content-Type: application/json;charset=UTF-8"})
    Call<String> registerUser(@Body String jsonBody);

    //FORGOT PASSWORD
    @POST(APIS.FORGOT_PASSWORD)
    @Headers({"Content-Type: application/json;charset=UTF-8"})
    Call<String> forgotPass(@Body String jsonBody);

    //UPDATE PASSWORD
    @POST(APIS.UPDATE_PASSWORD)
    @Headers({"Content-Type: application/json;charset=UTF-8"})
    Call<String> updatePassword(@Body String jsonBody);

    //GET CUSTOMER
    @POST(APIS.GET_CUSTOMER)
    @Headers({"Content-Type: application/json;charset=UTF-8"})
    Call<String> getCustomerDetails(@Body String jsonBody);

    //UPDATE USER_IMAGE
    @POST(APIS.UPDATE_USER_IMAGE)
    @Headers({"Content-Type: application/json;charset=UTF-8"})
    Call<String> updateUserImage(@Body String jsonBody);

    //UPDATE CUSTOMER
    @POST(APIS.UPDATE_CUSTOMER)
    @Headers({"Content-Type: application/json;charset=UTF-8"})
    Call<String> updateCustomer(@Body String jsonBody);

    //SEARCH ITEMS
    @POST(APIS.SEARCH)
    @Headers({"Content-Type: application/json;charset=UTF-8"})
    Call<String> getSearchResult(@Body String jsonBody);

    //ADD REVIEW
    @POST(APIS.ADD_REVIEW)
    @Headers({"Content-Type: application/json;charset=UTF-8"})
    Call<String> addReview(@Body String jsonBody);

    //https://custlr.com.my/wp-json/wc/v1/products/4735/reviews

    @GET
    @Headers({"Content-Type: application/json;charset=UTF-8"})
    Call<String> getReviewsList(@Url String url);

    //@GET("wp-json/wc/v2//products/{product_id}/reviews")
    //Call<String> getReviewsList(@Path(value = "product_id", encoded = true) String productId);

    //GET MEASUREMENT VALUES
    @POST(APIS.GET_MEASUREMENT_VALUES)
    @FormUrlEncoded
    Call<String> getMeasurementValues(@Field("product_id") String productId);

    /*ADD TO CART*/
    @Multipart
    @POST(APIS.ADD_TO_CART)
    Call<String> addToCart(@PartMap() Map<String, RequestBody> partMap, @Part MultipartBody.Part file1, @Part MultipartBody.Part file2);

    //GET PRODUCTS LIST
    @POST(APIS.MY_ORDERS)
    @Headers({"Content-Type: application/json;charset=UTF-8"})
    Call<String> getMyOrdersList(@Body String jsonBody);

    //GET CART VALUES
    @POST(APIS.GET_CART)
    @FormUrlEncoded
    Call<String> getCartList(@Field("user_id") String user_id);


    //GET CART VALUES
    @POST(APIS.GET_CART_AND_WISH_LIST_COUNT)
    @FormUrlEncoded
    Call<String> getCartAndWishListCount(@Field("user_id") String user_id);

    //GET CART VALUES
    @POST(APIS.DELETE_CART_ITEM)
    @FormUrlEncoded
    Call<String> deleteCartItem(@Field("product_id") String product_id, @Field("user_id") String user_id);


    //CHECKOUT PRODUCT
    @POST(APIS.CHECKOUT_PRODUCT)
    @Headers({"Content-Type: application/json;charset=UTF-8"})
    Call<String> checkOutProduct(@Body String jsonBody);

    //WISH LIST IDS
    @POST(APIS.WISH_LIST_IDS)
    @Headers({"Content-Type: application/json;charset=UTF-8"})
    Call<String> getWishListIds(@Body String jsonBody);

    //GET MEASUREMENTS PROFILE
    @POST(APIS.GET_MEASUREMENTS_PROFILE)
    @FormUrlEncoded
    Call<String> getMeasurements(@Field("user_id") String user_id);

    //LOGOUT PAYMENT
    @POST(APIS.LOGOUT_PAYMENT)
    @Headers({"Content-Type: application/json;charset=UTF-8"})
    Call<String> logoutPayment(@Body String jsonBody);

    //UPDATE MEASUREMENTS
    @POST(APIS.UPDATE_MEASUREMENT)
    @FormUrlEncoded
    Call<String> updateMeasurements(@Field("user_id") String user_id, @Field("sholder_size") String shoulder_size, @Field("chest_size") String chest_size, @Field("arm_size") String arm_size,
                                    @Field("arm_length_size") String arm_length_size, @Field("waist_size") String waist_size);

}
