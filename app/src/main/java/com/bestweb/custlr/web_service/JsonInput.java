package com.bestweb.custlr.web_service;

import com.bestweb.custlr.utils.MyUtils;

import org.json.JSONException;
import org.json.JSONObject;

public class JsonInput {


    public static String getLoginJsonObject(String userName, String password, String deviceToken, String deviceType) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("email", userName);
            jsonObject.put("password", password);
            jsonObject.put("deveice_token", deviceToken);
            jsonObject.put("device_type", deviceType);
            return "" + jsonObject.toString();
        } catch (JSONException e) {
            return "";
        }
    }

    public static String getHomeJsonObject(String userId, String appVer) {
        JSONObject jsonObject = new JSONObject();
        try {
            if (MyUtils.checkStringValue(userId)) {
                jsonObject.put("user_id", userId);
            }
            jsonObject.put("app-ver", appVer);
            return "" + jsonObject.toString();
        } catch (JSONException e) {
            return "";
        }
    }

    public static String getWishListIds(String userId) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("user_id", userId);
            return "" + jsonObject.toString();
        } catch (JSONException e) {
            return "";
        }
    }

    public static String getProductDetails(String productId) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("include", productId);
            return "" + jsonObject.toString();
        } catch (JSONException e) {
            return "";
        }
    }

    public static String registerJson(String userName, String email, String mobileNo, String password, String deviceType) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("email", email);
            jsonObject.put("username", userName);
            jsonObject.put("mobile", mobileNo);
            jsonObject.put("password", password);
            jsonObject.put("device_type", deviceType);
            return "" + jsonObject.toString();
        } catch (JSONException e) {
            return "";
        }
    }

    public static String forgtoPassJson(String email) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("email", email);
            return "" + jsonObject.toString();
        } catch (JSONException e) {
            return "";
        }
    }

    public static String updatePassword(String email, String password, String pin) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("email", email);
            jsonObject.put("password", password);
            jsonObject.put("key", pin);
            return "" + jsonObject.toString();
        } catch (JSONException e) {
            return "";
        }
    }

    public static String productList(String categoryId, int pageNumber, String sortBy) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("category", categoryId);
            jsonObject.put("page", pageNumber);
            jsonObject.put("order_by", sortBy);
            return "" + jsonObject.toString();
        } catch (JSONException e) {
            return "";
        }
    }

    public static String getCustomerDetails(String userId) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("user_id", userId);
            return "" + jsonObject.toString();
        } catch (JSONException e) {
            return "";
        }
    }

    public static String updateUserImage(JSONObject object, String userId) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("user_image", object);
            jsonObject.put("user_id", userId);
            return "" + jsonObject.toString();
        } catch (JSONException e) {
            return "";
        }
    }

    public static String updateCustomerJSON(String firstName, String lastName, String address, String state, String city, String postcode, String phoneNumber, String dob, String userId) {
        try {
            JSONObject jsonObject = new JSONObject();

            jsonObject.put("first_name", firstName);
            jsonObject.put("last_name", lastName);
            jsonObject.put("user_id", userId);
            jsonObject.put("dob", dob);


            JSONObject billingObject = new JSONObject();
            billingObject.put("address_1", address);
            billingObject.put("city", city);
            billingObject.put("phone", phoneNumber);
            billingObject.put("postcode", postcode);
            billingObject.put("state", state);

            jsonObject.put("billing", billingObject);

            return "" + jsonObject.toString();
        } catch (JSONException e) {
            return "";
        }
    }


    public static String searchInput(String searchHint, boolean isAppValidation) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("search", searchHint);
            jsonObject.put("is_app_validation", isAppValidation);
            return "" + jsonObject.toString();
        } catch (JSONException e) {
            return "";
        }
    }

    public static String wishListInput(String userId, String productId) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("user_id", userId);
            jsonObject.put("product_id", productId);
            return "" + jsonObject.toString();
        } catch (JSONException e) {
            return "";
        }
    }

    public static String addReviewInput(String email, String name, String productId, String comment, int rating, String userId) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("emailcustomer", email);
            jsonObject.put("namecustomer", name);
            jsonObject.put("product", productId);
            jsonObject.put("comment", comment);
            jsonObject.put("ratestar", rating);
            if (MyUtils.checkStringValue(userId)) {
                jsonObject.put("user_id", userId);
            }
            return "" + jsonObject.toString();
        } catch (JSONException e) {
            return "";
        }
    }


    public static String myOrdersInput(int pageNumber, String userId) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("page", pageNumber);
            jsonObject.put("customer", userId);
            return "" + jsonObject.toString();
        } catch (JSONException e) {
            return "";
        }
    }


}
