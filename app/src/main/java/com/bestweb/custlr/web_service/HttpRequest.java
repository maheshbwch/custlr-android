package com.bestweb.custlr.web_service;

import com.bestweb.custlr.utils.APIS;
import com.google.gson.Gson;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;
import se.akerfeldt.okhttp.signpost.OkHttpOAuthConsumer;
import se.akerfeldt.okhttp.signpost.SigningInterceptor;

public class HttpRequest {

    private static Retrofit retrofit = null;
    private static Retrofit retrofitScalars = null;
    private static Retrofit retrofitScalarsWC = null;

    public static Retrofit getInstance() {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(APIS.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    //.client(getClient())
                    .build();
        }
        return retrofit;
    }

    public static Retrofit getScalarsInstance() {
        if (retrofitScalars == null) {
            retrofitScalars = new Retrofit.Builder()
                    .baseUrl(APIS.BASE_URL)
                    .addConverterFactory(ScalarsConverterFactory.create())
                    .client(getClient())
                    .build();
        }
        return retrofitScalars;
    }

    public static Retrofit getScalarsInstanceWC() {
        if (retrofitScalarsWC == null) {
            retrofitScalarsWC = new Retrofit.Builder()
                    .baseUrl(APIS.BASE_URL)
                    .addConverterFactory(ScalarsConverterFactory.create())
                    .client(getTimeoutClient())
                    .build();
        }
        return retrofitScalarsWC;
    }


    private static OkHttpClient getClient() {
        OkHttpOAuthConsumer consumer = new OkHttpOAuthConsumer(APIS.CONSUMERKEY, APIS.CONSUMERSECRET);
        consumer.setTokenWithSecret(APIS.OAUTH_TOKEN, APIS.OAUTH_TOKEN_SECRET);

        return new OkHttpClient.Builder()
                .addInterceptor(new SigningInterceptor(consumer))
                .build();
    }

    private static OkHttpClient getTimeoutClient() {

        return new OkHttpClient.Builder()
                .connectTimeout(1, TimeUnit.MINUTES)
                .readTimeout(1, TimeUnit.MINUTES)
                .writeTimeout(1, TimeUnit.MINUTES)
                .build();

    }


    /**
     * JSON to Object  -> getObjectFromJSON
     */
    public Object getObjectFromJSON(String responseValue, Class<?> classname) {
        Gson gDataBean = new Gson();
        return gDataBean.fromJson(responseValue, classname);
    }
}
