package com.bestweb.custlr.dialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import com.bestweb.custlr.R;
import com.bestweb.custlr.adapters.SizeAdapter;
import com.bestweb.custlr.fragments.bottom_sheet_dialog.BottomSheetExpendable;
import com.bestweb.custlr.interfaces.OnSizeMeasure;
import com.bestweb.custlr.models.MeasureSizeInch;
import com.bestweb.custlr.models.StyleModel;
import com.bestweb.custlr.utils.Constants;

import java.util.ArrayList;

public class MeasurementSizeDialog {


    private AlertDialog alertDialog = null;
    private TextView countTxt;
    private ViewPager viewPager;
    private LinearLayout linearNext, linearBack;
    //private ArrayList<MeasureSizeInch> arrayList = new ArrayList<>();
    private ImageView imgClose;
    private int startPosition = 0;

    private static final int startLimit = 0;
    private int endLimit;


    private Activity activity;
    private String isFrom;
    private ArrayList<MeasureSizeInch> measureSizeInchArrayList;
    private OnSizeMeasure onSizeMeasureBody;
    private ArrayList<StyleModel> styleModelArrayList = new ArrayList<>();


    public MeasurementSizeDialog(Activity activity, String isFrom, ArrayList<MeasureSizeInch> measureSizeInchArrayList, OnSizeMeasure onSizeMeasureBody, ArrayList<StyleModel> styleModelArrayList) {
        this.activity = activity;
        this.isFrom = isFrom;
        this.measureSizeInchArrayList = measureSizeInchArrayList;
        this.onSizeMeasureBody = onSizeMeasureBody;
        this.styleModelArrayList = styleModelArrayList;
    }


    public void showAlertDialog() {

        LayoutInflater inflater = activity.getLayoutInflater();
        View view = inflater.inflate(R.layout.fragment_bottom_sheet_dialog, null);

        countTxt = view.findViewById(R.id.fbs_countTxt);
        viewPager = view.findViewById(R.id.fbs_view_pager);

        linearNext = view.findViewById(R.id.fbs_linearNext);
        linearBack = view.findViewById(R.id.fbs_linearBack);
        imgClose = view.findViewById(R.id.fbs_imgClose);


        endLimit = measureSizeInchArrayList.size() > 0 ? (measureSizeInchArrayList.size() - 1) : 0;

        OnSizeMeasure onSizeMeasure = new OnSizeMeasure() {
            @Override
            public void onSizeMeasure(int index, String value) {

                Log.e("onSizeMeasure", "index:" + index + " val:" + value);

                startPosition = index;
                populateCountTxt(index);


                MeasureSizeInch measureSizeInch = measureSizeInchArrayList.get(index);
                measureSizeInch.setValue(value);

                onSizeMeasureBody.onSizeMeasure(index, value);


            }
        };

        final AlertDialog.Builder alert = new AlertDialog.Builder(activity);
        alert.setView(view);
        alert.setCancelable(false);
        alertDialog = alert.create();
        alertDialog.show();

        SizeAdapter sizeAdapter = new SizeAdapter(activity, measureSizeInchArrayList, onSizeMeasure, alertDialog);
        viewPager.setAdapter(sizeAdapter);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                startPosition = position;
                viewPager.setCurrentItem(startPosition);
                populateCountTxt(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });


        if (isFrom.equalsIgnoreCase(Constants.SHOULDER)) {
            startPosition = 0;
        } else if (isFrom.equalsIgnoreCase(Constants.CHEST)) {
            startPosition = 1;
        } else if (isFrom.equalsIgnoreCase(Constants.ARM)) {
            startPosition = 2;
        } else if (isFrom.equalsIgnoreCase(Constants.ARM_LENGTH)) {
            startPosition = 3;
        } else if (isFrom.equalsIgnoreCase(Constants.WAIST)) {
            startPosition = 4;
        }
        //recyclerView.scrollToPosition(startPosition);
        viewPager.setCurrentItem(startPosition);
        populateCountTxt(startPosition);


        linearNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (startPosition < endLimit) {
                    startPosition = startPosition + 1;
                    populateCountTxt(startPosition);
                    //recyclerView.scrollToPosition(startPosition);
                    viewPager.setCurrentItem(startPosition);

                } else {

                    AppCompatActivity appCompatActivity = (AppCompatActivity) activity;

                    if (styleModelArrayList.size() > 0) {
                        BottomSheetExpendable bottomSheetExpendable = new BottomSheetExpendable(styleModelArrayList);
                        bottomSheetExpendable.show(appCompatActivity.getSupportFragmentManager(), "tag");
                    }

                    dismissDialog(alertDialog);
                }

            }
        });
        linearBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (startPosition > startLimit) {
                    startPosition = startPosition - 1;
                    populateCountTxt(startPosition);
                    //recyclerView.scrollToPosition(startPosition);
                    viewPager.setCurrentItem(startPosition);
                }

            }
        });


        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismissDialog(alertDialog);
            }
        });


    }


    private void populateCountTxt(int position) {
        countTxt.setText("step " + (position + 1) + "/" + (endLimit + 1));
        if (position == startLimit) {
            if (linearBack.getVisibility() == View.VISIBLE) {
                linearBack.setVisibility(View.INVISIBLE);
            }
        } else {
            if (linearBack.getVisibility() == View.INVISIBLE) {
                linearBack.setVisibility(View.VISIBLE);
            }
        }
    }


    private void dismissDialog(AlertDialog alertDialog) {
        if (alertDialog != null && alertDialog.isShowing()) {
            alertDialog.dismiss();
        }
    }
}
