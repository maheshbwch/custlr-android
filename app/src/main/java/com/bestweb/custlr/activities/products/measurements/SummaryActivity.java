package com.bestweb.custlr.activities.products.measurements;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.bestweb.custlr.R;
import com.bestweb.custlr.activities.HomeActivity;
import com.bestweb.custlr.activities.LoginActivity;
import com.bestweb.custlr.models.AddToCartModel;
import com.bestweb.custlr.utils.Constants;
import com.bestweb.custlr.utils.MyUtils;
import com.bestweb.custlr.utils.PreferenceHandler;
import com.bestweb.custlr.utils.RequestParamUtils;
import com.bestweb.custlr.web_service.ApiInterface;
import com.bestweb.custlr.web_service.HttpRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.github.siyamed.shapeimageview.mask.PorterShapeImageView;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.io.File;
import java.util.HashMap;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SummaryActivity extends AppCompatActivity {

    private Context context;
    private ImageView imgBack;
    private TextView txtHeader, txtSize, txtWeight, txtHeight, txtRemarks;
    private TextView txtPrdName, txtPrdPrice, txtShoulder, txtChest, txtArm, txtArmLength, txtWaist;
    private LinearLayout linearA4Fit, linearBodyMeasure, linearSize, linearAddCart;

    private ProgressDialog progressDialog = null;
    private PorterShapeImageView imageView;

    String isPageFrom = "";
    String productId = "";
    String productName = "";
    String productPrice = "";
    String imgUrl = "";

    String shoulderValue = "";
    String chestValue = "";
    String armValue = "";
    String armLengthValue = "";
    String waistValue = "";

    String size = "";
    String weight = "";
    String height = "";
    String userId = "";

    String collar_name = "", collar_type = "", collar_value = "", collar_section = "", collar_key = "", collar_image = "", cuff_name = "", cuff_type = "";
    String cuff_value = "", cuff_section = "", cuff_key = "", cuff_image = "", sleeve_name = "", sleeve_type = "", sleeve_value = "", sleeve_section = "";
    String sleeve_key = "", sleeve_image = "", fitting_name = "", fitting_type = "", fitting_value = "", fitting_section = "", fitting_key = "", fitting_image = "";
    String chest_pocket_name = "", chest_pocket_type = "", chest_pocket_value = "", chest_pocket_section = "", chest_pocket_key = "", chest_pocket_imag = "";


    String sholder_class = "", sholder_name = "", sholder_value = "", sholder_section = "", chest_class = "", chest_name = "";
    String chest_value = "", chest_section = "", arm_class = "", arm_name = "", arm_value = "", arm_section = "";
    String arm_Length_class = "", arm_length_name = "", arm_length_value = "", arm_length_section = "", waist_length_class = "", waist_length_name = "";
    String waist_length_value = "", waist_length_section = "";

    String size_class = "", size_name = "", size_key = "", size_section = "";

    String weight_class = "", weight_name = "", weight_section = "";
    String height_class = "", height_name = "", height_section = "";

    String front_image_class = "", front_image_name = "", front_image_section = "";
    String side_image_class = "", side_image_name = "", side_image_section = "";


    String remark_class = "", remark_name = "", remark_section = "";

    String imageFront = "";
    String imageSide = "";

    String remarks = "";

    boolean isImageLoaded = false;
    HashMap<String, String> map = new HashMap<>();
    HashMap<String, String> mapHeightWeight = new HashMap<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_summary);
        context = SummaryActivity.this;

        init();

    }

    private void init() {

        imgBack = findViewById(R.id.imgBack);
        txtHeader = findViewById(R.id.txtHeader);
        linearSize = findViewById(R.id.as_linearSize);
        linearA4Fit = findViewById(R.id.as_linear_a4Fit);
        linearBodyMeasure = findViewById(R.id.as_linearBodyMeasure);
        linearAddCart = findViewById(R.id.as_bottomLinear);
        txtRemarks = findViewById(R.id.as_txtRemarks);

        txtPrdName = findViewById(R.id.as_prdName);
        txtPrdPrice = findViewById(R.id.as_prdPrice);
        imageView = findViewById(R.id.as_imgProduct);

        txtShoulder = findViewById(R.id.as_txtShoulder);
        txtChest = findViewById(R.id.as_txtChest);
        txtArm = findViewById(R.id.as_txtArm);
        txtArmLength = findViewById(R.id.as_txtArmLength);
        txtWaist = findViewById(R.id.as_txtWaist);

        txtWeight = findViewById(R.id.as_a4_waist);
        txtHeight = findViewById(R.id.as_a4_height);

        txtSize = findViewById(R.id.as_txtSize);

        txtHeader.setText("SUMMARY");

        Bundle bundle = getIntent().getExtras();


        if (bundle != null) {
            isPageFrom = bundle.getString(Constants.IS_FROM);
            productId = bundle.getString(Constants.PRODUCT_ID);
            productName = bundle.getString(Constants.PRODUCT_NAME);
            productPrice = bundle.getString(Constants.PRODUCT_PRICE);
            imgUrl = bundle.getString(Constants.PRODUCT_IMAGE_URL);


            shoulderValue = bundle.getString(Constants.SHOULDER);
            chestValue = bundle.getString(Constants.CHEST);
            armValue = bundle.getString(Constants.ARM);
            armLengthValue = bundle.getString(Constants.ARM_LENGTH);
            waistValue = bundle.getString(Constants.WAIST);

            remarks = bundle.getString(Constants.REMARKS);

            size = bundle.getString(Constants.SIZE);
            map = (HashMap<String, String>) bundle.getSerializable(Constants.STYLE_MAP);
            mapHeightWeight = (HashMap<String, String>) bundle.getSerializable(Constants.HEIGHT_WEIGHT);

            imageFront = bundle.getString(Constants.IMAGE_FRONT);
            imageSide = bundle.getString(Constants.IMAGE_SIDE);

            weight = bundle.getString(Constants.WEIGHT);
            height = bundle.getString(Constants.HEIGHT);


        }

        loadProductValues();
        setUpViews();
        getStyleValues();

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                MyUtils.openOverrideAnimation(false, SummaryActivity.this);
            }
        });

        linearAddCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                checkIfUserLoggedIn();


            }
        });


    }

    private void intentToLoginPage() {
        Intent intent = new Intent(context, LoginActivity.class);
        startActivityForResult(intent, Constants.LOGIN_CODE);
        MyUtils.openOverrideAnimation(false, SummaryActivity.this);
    }

    private void getStyleValues() {

        if (isPageFrom.equals(Constants.IS_FROM_MEASUREMENT)) {
            collar_name = map.get("collar_name");
            collar_type = map.get("collar_type");
            collar_value = map.get("collar_value");
            collar_section = map.get("collar_section");
            collar_key = map.get("collar_key");
            collar_image = map.get("collar_image");

            cuff_name = map.get("cuff_name");
            cuff_type = map.get("cuff_type");
            cuff_value = map.get("cuff_value");
            cuff_section = map.get("cuff_section");
            cuff_key = map.get("cuff_key");
            cuff_image = map.get("cuff_image");

            sleeve_name = map.get("sleeve_name");
            sleeve_type = map.get("sleeve_type");
            sleeve_value = map.get("sleeve_value");
            sleeve_section = map.get("sleeve_section");
            sleeve_key = map.get("sleeve_key");
            sleeve_image = map.get("sleeve_image");

            fitting_name = map.get("fitting_name");
            fitting_type = map.get("fitting_type");
            fitting_value = map.get("fitting_value");
            fitting_section = map.get("fitting_section");
            fitting_key = map.get("fitting_key");
            fitting_image = map.get("fitting_image");

            chest_pocket_name = map.get("chest_pocket_name");
            chest_pocket_type = map.get("chest_pocket_type");
            chest_pocket_value = map.get("chest_pocket_value");
            chest_pocket_section = map.get("chest_pocket_section");
            chest_pocket_key = map.get("chest_pocket_key");
            chest_pocket_imag = map.get("chest_pocket_image");


            sholder_class = map.get("sholder_class");
            sholder_name = map.get("sholder_name");
            sholder_value = map.get("sholder_value");
            sholder_section = map.get("sholder_section");

            chest_class = map.get("chest_class");
            chest_name = map.get("chest_name");
            chest_value = map.get("chest_value");
            chest_section = map.get("chest_section");

            arm_class = map.get("arm_class");
            arm_name = map.get("arm_name");
            arm_value = map.get("arm_value");
            arm_section = map.get("arm_section");

            arm_Length_class = map.get("arm_Length_class");
            arm_length_name = map.get("arm_length_name");
            arm_length_value = map.get("arm_length_value");
            arm_length_section = map.get("arm_length_section");

            waist_length_class = map.get("waist_length_class");
            waist_length_name = map.get("waist_length_name");
            waist_length_value = map.get("waist_length_value");
            waist_length_section = map.get("waist_length_section");


            size_class = map.get("size_class");
            size_name = map.get("size_name");
            size_key = map.get("size_key");
            size_section = map.get("size_section");


            remark_class = map.get("remark_class");
            remark_name = map.get("remark_name");
            remark_section = map.get("remark_section");

        }


        if (isPageFrom.equals(Constants.IS_FROM_A4FIT)) {


            collar_name = mapHeightWeight.get("collar_name");
            collar_type = mapHeightWeight.get("collar_type");
            collar_value = mapHeightWeight.get("collar_value");
            collar_section = mapHeightWeight.get("collar_section");
            collar_key = mapHeightWeight.get("collar_key");
            collar_image = mapHeightWeight.get("collar_image");

            cuff_name = mapHeightWeight.get("cuff_name");
            cuff_type = mapHeightWeight.get("cuff_type");
            cuff_value = mapHeightWeight.get("cuff_value");
            cuff_section = mapHeightWeight.get("cuff_section");
            cuff_key = mapHeightWeight.get("cuff_key");
            cuff_image = mapHeightWeight.get("cuff_image");

            sleeve_name = mapHeightWeight.get("sleeve_name");
            sleeve_type = mapHeightWeight.get("sleeve_type");
            sleeve_value = mapHeightWeight.get("sleeve_value");
            sleeve_section = mapHeightWeight.get("sleeve_section");
            sleeve_key = mapHeightWeight.get("sleeve_key");
            sleeve_image = mapHeightWeight.get("sleeve_image");

            fitting_name = mapHeightWeight.get("fitting_name");
            fitting_type = mapHeightWeight.get("fitting_type");
            fitting_value = mapHeightWeight.get("fitting_value");
            fitting_section = mapHeightWeight.get("fitting_section");
            fitting_key = mapHeightWeight.get("fitting_key");
            fitting_image = mapHeightWeight.get("fitting_image");

            chest_pocket_name = mapHeightWeight.get("chest_pocket_name");
            chest_pocket_type = mapHeightWeight.get("chest_pocket_type");
            chest_pocket_value = mapHeightWeight.get("chest_pocket_value");
            chest_pocket_section = mapHeightWeight.get("chest_pocket_section");
            chest_pocket_key = mapHeightWeight.get("chest_pocket_key");
            chest_pocket_imag = mapHeightWeight.get("chest_pocket_image");

            weight_class = mapHeightWeight.get("weight_class");
            weight_name = mapHeightWeight.get("weight_name");
            weight_section = mapHeightWeight.get("weight_section");

            height_class = mapHeightWeight.get("height_class");
            height_name = mapHeightWeight.get("height_name");
            height_section = mapHeightWeight.get("height_section");


            front_image_class = mapHeightWeight.get("front_image_class");
            front_image_name = mapHeightWeight.get("front_image_name");
            front_image_section = mapHeightWeight.get("front_image_section");

            side_image_class = mapHeightWeight.get("side_image_class");
            side_image_name = mapHeightWeight.get("side_image_name");
            side_image_section = mapHeightWeight.get("side_image_section");

            remark_class = mapHeightWeight.get("remark_class");
            remark_name = mapHeightWeight.get("remark_name");
            remark_section = mapHeightWeight.get("remark_section");
        }

    }

    private void loadProductValues() {
        if (MyUtils.checkStringValue(productName)) {
            txtPrdName.setText(productName);
        }
        if (MyUtils.checkStringValue(productPrice)) {
            txtPrdPrice.setText("RM " + productPrice);
        }
        if (MyUtils.checkStringValue(imgUrl)) {
            loadImageSource(imgUrl);
        }
    }


    private void setUpViews() {
        if (isPageFrom.equalsIgnoreCase(Constants.IS_FROM_MEASUREMENT)) {

            linearBodyMeasure.setVisibility(View.VISIBLE);
            linearA4Fit.setVisibility(View.GONE);

            setBodyMeasurementValues();

        } else if (isPageFrom.equalsIgnoreCase(Constants.IS_FROM_A4FIT)) {

            linearA4Fit.setVisibility(View.VISIBLE);
            linearSize.setVisibility(View.GONE);
            linearBodyMeasure.setVisibility(View.GONE);

            setA4FitMeasurement();
        }
        txtRemarks.setText(MyUtils.checkStringValue(remarks) ? remarks : "-");
    }


    private void setBodyMeasurementValues() {

        txtShoulder.setText(MyUtils.checkStringValue(shoulderValue) ? shoulderValue : "-");
        txtChest.setText(MyUtils.checkStringValue(chestValue) ? chestValue : "-");
        txtArm.setText(MyUtils.checkStringValue(armValue) ? armValue : "-");
        txtArmLength.setText(MyUtils.checkStringValue(armLengthValue) ? armLengthValue : "-");
        txtWaist.setText(MyUtils.checkStringValue(waistValue) ? waistValue : "-");


        if (MyUtils.checkStringValue(size)) {
            txtSize.setText(size);
            linearSize.setVisibility(View.VISIBLE);
        } else {
            linearSize.setVisibility(View.GONE);
        }

    }

    private void setA4FitMeasurement() {
        txtWeight.setText(MyUtils.checkStringValue(weight) ? weight : "-");
        txtHeight.setText(MyUtils.checkStringValue(height) ? height : "-");
    }


    private void loadImageSource(String imageFilePath) {
        Glide.with(SummaryActivity.this).load(imageFilePath).placeholder(R.drawable.profile_picture_big).listener(new RequestListener<Drawable>() {
            @Override
            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                isImageLoaded = false;
                return false;
            }

            @Override
            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                isImageLoaded = true;
                return false;
            }
        }).into(imageView);
    }


    private void addCart() {

        try {
            HashMap<String, RequestBody> map = new HashMap<>();

            String postType = PreferenceHandler.getPreferenceFromString(context, "postType");
            if (postType.equals(Constants.BODY_MEASURE_POST)) {
                map.put("post_type", RequestBody.create(MediaType.parse("text/plain"), "mesurment_type"));
            }
            if (postType.equals(Constants.A4FIT_MEASURE_POST)) {
                map.put("post_type", RequestBody.create(MediaType.parse("text/plain"), "a4_type"));
            }
            if (postType.equals(Constants.STANDARD_SIZE_POST)) {
                map.put("post_type", RequestBody.create(MediaType.parse("text/plain"), "select_type"));
            }

            map.put("user_id", RequestBody.create(MediaType.parse("text/plain"), userId));
            map.put("product_id", RequestBody.create(MediaType.parse("text/plain"), productId));
            map.put("quantity", RequestBody.create(MediaType.parse("text/plain"), "1"));
            map.put("collar_name", RequestBody.create(MediaType.parse("text/plain"), collar_name != null ? collar_name : ""));
            map.put("collar_type", RequestBody.create(MediaType.parse("text/plain"), collar_type != null ? collar_type : ""));
            map.put("collar_value", RequestBody.create(MediaType.parse("text/plain"), collar_value != null ? collar_value : ""));
            map.put("collar_section", RequestBody.create(MediaType.parse("text/plain"), collar_section != null ? collar_section : ""));
            map.put("collar_key", RequestBody.create(MediaType.parse("text/plain"), collar_key != null ? collar_key : ""));
            map.put("collar_image", RequestBody.create(MediaType.parse("text/plain"), collar_image != null ? collar_image : ""));
            map.put("cuff_name", RequestBody.create(MediaType.parse("text/plain"), cuff_name != null ? cuff_name : ""));
            map.put("cuff_type", RequestBody.create(MediaType.parse("text/plain"), cuff_type != null ? cuff_type : ""));
            map.put("cuff_value", RequestBody.create(MediaType.parse("text/plain"), cuff_value != null ? cuff_value : ""));
            map.put("cuff_section", RequestBody.create(MediaType.parse("text/plain"), cuff_section != null ? cuff_section : ""));
            map.put("cuff_key", RequestBody.create(MediaType.parse("text/plain"), cuff_key != null ? cuff_key : ""));
            map.put("cuff_image", RequestBody.create(MediaType.parse("text/plain"), cuff_image != null ? cuff_image : ""));
            map.put("sleeve_name", RequestBody.create(MediaType.parse("text/plain"), sleeve_name != null ? sleeve_name : ""));
            map.put("sleeve_type", RequestBody.create(MediaType.parse("text/plain"), sleeve_type != null ? sleeve_type : ""));
            map.put("sleeve_value", RequestBody.create(MediaType.parse("text/plain"), sleeve_value != null ? sleeve_value : ""));
            map.put("sleeve_section", RequestBody.create(MediaType.parse("text/plain"), sleeve_section != null ? sleeve_section : ""));
            map.put("sleeve_key", RequestBody.create(MediaType.parse("text/plain"), sleeve_key != null ? sleeve_key : ""));
            map.put("sleeve_image", RequestBody.create(MediaType.parse("text/plain"), sleeve_image != null ? sleeve_image : ""));
            map.put("fitting_name", RequestBody.create(MediaType.parse("text/plain"), fitting_name != null ? fitting_name : ""));
            map.put("fitting_type", RequestBody.create(MediaType.parse("text/plain"), fitting_type != null ? fitting_type : ""));
            map.put("fitting_value", RequestBody.create(MediaType.parse("text/plain"), fitting_value != null ? fitting_value : ""));
            map.put("fitting_section", RequestBody.create(MediaType.parse("text/plain"), fitting_section != null ? fitting_section : ""));
            map.put("fitting_key", RequestBody.create(MediaType.parse("text/plain"), fitting_key != null ? fitting_key : ""));
            map.put("fitting_image", RequestBody.create(MediaType.parse("text/plain"), fitting_image != null ? fitting_image : ""));
            map.put("chest_pocket_name", RequestBody.create(MediaType.parse("text/plain"), chest_pocket_name != null ? chest_pocket_name : ""));
            map.put("chest_pocket_type", RequestBody.create(MediaType.parse("text/plain"), chest_pocket_type != null ? chest_pocket_type : ""));
            map.put("chest_pocket_value", RequestBody.create(MediaType.parse("text/plain"), chest_pocket_value != null ? chest_pocket_value : ""));
            map.put("chest_pocket_section", RequestBody.create(MediaType.parse("text/plain"), chest_pocket_section != null ? chest_pocket_section : ""));
            map.put("chest_pocket_key", RequestBody.create(MediaType.parse("text/plain"), chest_pocket_key != null ? chest_pocket_key : ""));
            map.put("chest_pocket_image", RequestBody.create(MediaType.parse("text/plain"), chest_pocket_imag != null ? chest_pocket_imag : ""));
            map.put("sholder_class", RequestBody.create(MediaType.parse("text/plain"), sholder_class != null ? sholder_class : ""));
            map.put("sholder_name", RequestBody.create(MediaType.parse("text/plain"), sholder_name != null ? sholder_name : ""));
            map.put("sholder_value", RequestBody.create(MediaType.parse("text/plain"), sholder_value != null ? sholder_value : ""));
            map.put("sholder_section", RequestBody.create(MediaType.parse("text/plain"), sholder_section != null ? sholder_section : ""));
            map.put("chest_class", RequestBody.create(MediaType.parse("text/plain"), chest_class != null ? chest_class : ""));
            map.put("chest_name", RequestBody.create(MediaType.parse("text/plain"), chest_name != null ? chest_name : ""));
            map.put("chest_value", RequestBody.create(MediaType.parse("text/plain"), chest_value != null ? chest_value : ""));
            map.put("chest_section", RequestBody.create(MediaType.parse("text/plain"), chest_section != null ? chest_section : ""));
            map.put("arm_class", RequestBody.create(MediaType.parse("text/plain"), arm_class != null ? arm_class : ""));
            map.put("arm_name", RequestBody.create(MediaType.parse("text/plain"), arm_name != null ? arm_name : ""));
            map.put("arm_value", RequestBody.create(MediaType.parse("text/plain"), arm_value != null ? arm_value : ""));
            map.put("arm_section", RequestBody.create(MediaType.parse("text/plain"), arm_section != null ? arm_section : ""));
            map.put("arm_Length_class", RequestBody.create(MediaType.parse("text/plain"), arm_Length_class != null ? arm_Length_class : ""));
            map.put("arm_length_name", RequestBody.create(MediaType.parse("text/plain"), arm_length_name != null ? arm_length_name : ""));
            map.put("arm_length_value", RequestBody.create(MediaType.parse("text/plain"), arm_length_value != null ? arm_length_value : ""));
            map.put("arm_length_section", RequestBody.create(MediaType.parse("text/plain"), arm_length_section != null ? arm_length_section : ""));
            map.put("waist_length_class", RequestBody.create(MediaType.parse("text/plain"), waist_length_class != null ? waist_length_class : ""));
            map.put("waist_length_name", RequestBody.create(MediaType.parse("text/plain"), waist_length_name != null ? waist_length_name : ""));
            map.put("waist_length_value", RequestBody.create(MediaType.parse("text/plain"), waist_length_value != null ? waist_length_value : ""));
            map.put("waist_length_section", RequestBody.create(MediaType.parse("text/plain"), waist_length_section != null ? waist_length_section : ""));
            map.put("weight_class", RequestBody.create(MediaType.parse("text/plain"), weight_class != null ? weight_class : ""));
            map.put("weight_name", RequestBody.create(MediaType.parse("text/plain"), weight_name != null ? weight_name : ""));
            map.put("weight_value", RequestBody.create(MediaType.parse("text/plain"), weight != null ? weight : ""));
            map.put("weight_section", RequestBody.create(MediaType.parse("text/plain"), weight_section != null ? weight_section : ""));
            map.put("height_class", RequestBody.create(MediaType.parse("text/plain"), height_class != null ? height_class : ""));
            map.put("height_name", RequestBody.create(MediaType.parse("text/plain"), height_name != null ? height_name : ""));
            map.put("height_value", RequestBody.create(MediaType.parse("text/plain"), height != null ? height : ""));
            map.put("height_section", RequestBody.create(MediaType.parse("text/plain"), height_section != null ? height_section : ""));
            map.put("remark_class", RequestBody.create(MediaType.parse("text/plain"), remark_class != null ? remark_class : ""));
            map.put("remark_name", RequestBody.create(MediaType.parse("text/plain"), remark_name != null ? remark_name : ""));
            map.put("remark_value", RequestBody.create(MediaType.parse("text/plain"), remarks != null ? remarks : ""));
            map.put("remark_section", RequestBody.create(MediaType.parse("text/plain"), remark_section != null ? remark_section : ""));
            map.put("size_class", RequestBody.create(MediaType.parse("text/plain"), size_class != null ? size_class : ""));
            map.put("size_name", RequestBody.create(MediaType.parse("text/plain"), size_name != null ? size_name : ""));
            map.put("size_value", RequestBody.create(MediaType.parse("text/plain"), size != null ? size : ""));
            map.put("size_key", RequestBody.create(MediaType.parse("text/plain"), size_key != null ? size_key : ""));
            map.put("size_section", RequestBody.create(MediaType.parse("text/plain"), size_section != null ? size_section : ""));
            map.put("front_image_class", RequestBody.create(MediaType.parse("text/plain"), front_image_class != null ? front_image_class : ""));
            map.put("front_image_name", RequestBody.create(MediaType.parse("text/plain"), front_image_name != null ? front_image_name : ""));
            map.put("front_image_section", RequestBody.create(MediaType.parse("text/plain"), front_image_section != null ? front_image_section : ""));
            map.put("side_image_class", RequestBody.create(MediaType.parse("text/plain"), side_image_class != null ? side_image_class : ""));
            map.put("side_image_name", RequestBody.create(MediaType.parse("text/plain"), side_image_name != null ? side_image_name : ""));
            map.put("side_image_section", RequestBody.create(MediaType.parse("text/plain"), side_image_section != null ? side_image_section : ""));


            MultipartBody.Part body1;
            if (MyUtils.checkStringValue(imageFront)) {
                File file1 = new File(imageFront);
                RequestBody requestFile1 = RequestBody.create(MediaType.parse("*/*"), file1);
                body1 = MultipartBody.Part.createFormData("front_image", file1.getName(), requestFile1);
            } else {
                body1 = MultipartBody.Part.createFormData("front_image", "");
            }

            MultipartBody.Part body2;
            if (MyUtils.checkStringValue(imageSide)) {
                File file2 = new File(imageSide);
                RequestBody requestFile2 = RequestBody.create(MediaType.parse("*/*"), file2);
                body2 = MultipartBody.Part.createFormData("side_image", file2.getName(), requestFile2);
            } else {
                body2 = MultipartBody.Part.createFormData("side_image", "");
            }


            progressDialog = MyUtils.showProgressLoader(context, "Please Wait...");

            ApiInterface apiService = HttpRequest.getScalarsInstanceWC().create(ApiInterface.class);
            Call<String> call = apiService.addToCart(map, body1, body2);
            call.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    MyUtils.dismissProgressLoader(progressDialog);
                    try {
                        Log.e("response", "code" + response.code());
                        int resCode = response.code();
                        if (resCode == Constants.SUCCESS_START) {
                            if (response != null) {
                                Gson gson = new GsonBuilder().serializeNulls().create();
                                AddToCartModel addToCartResponse = gson.fromJson(response.body(), new TypeToken<AddToCartModel>() {
                                }.getType());
                                String responseCode = addToCartResponse.getStatus();
                                if (responseCode.equalsIgnoreCase("1")) {
                                    Intent i = new Intent(context, HomeActivity.class);
                                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(i);
                                    finish();

                                } else {
                                    MyUtils.ShowLongToast(context, ": " + addToCartResponse.getMessage());
                                }

                            } else {
                                MyUtils.ShowLongToast(context, "Unable to add product");
                            }
                        } else {
                            MyUtils.ShowLongToast(context, "Unable to add product");
                        }
                    } catch (Exception e) {
                        MyUtils.dismissProgressLoader(progressDialog);
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {

                    MyUtils.dismissProgressLoader(progressDialog);
                    MyUtils.ShowLongToast(context, "Unable to add product");
                    Log.e("on failure", "on failure" + t.getMessage());
                }
            });
        } catch (Exception e) {
            MyUtils.dismissProgressLoader(progressDialog);
            Log.e("exp", "code" + e.getMessage());
        }
    }

    private void checkIfUserLoggedIn() {
        userId = MyUtils.getPreferences(context).getString(RequestParamUtils.ID, "");

        if (MyUtils.checkIfUserLoggedIn(context) && MyUtils.checkStringValue(userId)) {
            if (MyUtils.isInternetConnected(SummaryActivity.this)) {
                addCart();
            } else {
                MyUtils.ShowLongToast(SummaryActivity.this, "Please Check your Internet");
            }
        } else {
            intentToLoginPage();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constants.LOGIN_CODE) {
            if (resultCode == Constants.LOGIN_SUCCESS) {
                checkIfUserLoggedIn();
            } else if (resultCode == Constants.LOGIN_FAILED) {
                if (!MyUtils.checkIfUserLoggedIn(context)) {
                    finish();
                }
            }
        }
    }
}
