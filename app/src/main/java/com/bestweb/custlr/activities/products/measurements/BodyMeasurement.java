package com.bestweb.custlr.activities.products.measurements;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.bestweb.custlr.R;
import com.bestweb.custlr.dialogs.MeasurementSizeDialog;
import com.bestweb.custlr.fragments.bottom_sheet_dialog.BottomSheetExpendable;
import com.bestweb.custlr.interfaces.DialogInterface;
import com.bestweb.custlr.interfaces.OnSizeMeasure;
import com.bestweb.custlr.models.GetMeasurementsModel;
import com.bestweb.custlr.models.MeasureSizeInch;
import com.bestweb.custlr.models.MeasurementValuesModel;
import com.bestweb.custlr.models.SizeModel;
import com.bestweb.custlr.models.StyleModel;
import com.bestweb.custlr.utils.Constants;
import com.bestweb.custlr.utils.CustomDialog;
import com.bestweb.custlr.utils.MyUtils;
import com.bestweb.custlr.utils.RequestParamUtils;
import com.bestweb.custlr.web_service.ApiInterface;
import com.bestweb.custlr.web_service.HttpRequest;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BodyMeasurement extends AppCompatActivity implements View.OnClickListener, OnSizeMeasure {

    private Context context;
    private ImageView imgBack, featuredImageView, imgTap, imgCut;
    private TextView txtHeader;
    private LinearLayout linearSummary, linearShoulder, linearChest, linearArm, linearArmLength, linearWaist;
    private OnSizeMeasure onSizeMeasure;
    private TextView txtShoulder, txtChest, txtArm, txtArmLength, txtWaist;
    private EditText edtRemarks;
    private ProgressDialog progressDialog = null;


    String productId = "";
    String productName = "";
    String productPrice = "";
    String imgUrl = "";
    String size = "";
    String featuredImageUrl = "";
    String styleJsonArray = "";
    String measurementValues = "";
    String sizeValues = "";
    private String userId = "";

    ArrayList<MeasureSizeInch> measureSizeInchArrayList = new ArrayList<>();
    ArrayList<StyleModel> styleModelArrayList = new ArrayList<>();
    ArrayList<MeasurementValuesModel> measurementValueArrayList = new ArrayList<>();
    ArrayList<SizeModel> sizeModelArrayList = new ArrayList<>();
    HashMap<String, String> map = new HashMap<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_body_measurement);
        context = BodyMeasurement.this;
        onSizeMeasure = this;

        init();

        listener();

    }

    private void init() {

        userId = MyUtils.getPreferences(context).getString(RequestParamUtils.ID, "");

        Bundle bundle = getIntent().getExtras();
        productId = bundle.getString(Constants.PRODUCT_ID);
        productName = bundle.getString(Constants.PRODUCT_NAME);
        productPrice = bundle.getString(Constants.PRODUCT_PRICE);
        imgUrl = bundle.getString(Constants.PRODUCT_IMAGE_URL);
        featuredImageUrl = bundle.getString(Constants.FEATURED_IMAGE_URL);
        size = bundle.getString(Constants.SIZE);
        styleJsonArray = bundle.getString(Constants.STYLE_JSON_ARRAY);
        measurementValues = bundle.getString(Constants.MEASUREMENT_VALUES);
        sizeValues = bundle.getString(Constants.SELECT);

        imgBack = findViewById(R.id.imgBack);
        txtHeader = findViewById(R.id.txtHeader);
        featuredImageView = findViewById(R.id.ab_featuredImageView);
        linearSummary = findViewById(R.id.linear_summary_button);
        edtRemarks = findViewById(R.id.ab_Remarks);

        linearShoulder = findViewById(R.id.ab_linear_shoulder);
        linearChest = findViewById(R.id.ab_linear_chest);
        linearArm = findViewById(R.id.ab_linear_arm);
        linearArmLength = findViewById(R.id.ab_linear_arm_length);
        linearWaist = findViewById(R.id.ab_linear_waist);

        imgCut = findViewById(R.id.ab_linearCut);
        imgTap = findViewById(R.id.ab_imgTap);


        txtShoulder = findViewById(R.id.ab_txtShoulder);
        txtChest = findViewById(R.id.ab_txtChest);
        txtArm = findViewById(R.id.ab_txtArm);
        txtArmLength = findViewById(R.id.ab_txtArmLength);
        txtWaist = findViewById(R.id.ab_txtWaist);

        txtHeader.setText("BODY MEASUREMENTS");
    }


    private void listener() {

        Glide.with(context).load(R.drawable.body_measurements).into(featuredImageView);

        imgBack.setOnClickListener(this);
        linearSummary.setOnClickListener(this);
        linearShoulder.setOnClickListener(this);
        linearChest.setOnClickListener(this);
        linearArm.setOnClickListener(this);
        linearArmLength.setOnClickListener(this);
        linearWaist.setOnClickListener(this);
        imgTap.setOnClickListener(this);
        imgCut.setOnClickListener(this);

        measureSizeInchArrayList = getMeasureInchArrayList();

        if (MyUtils.checkStringValue(styleJsonArray)) {
            Type listType = new TypeToken<ArrayList<StyleModel>>() {
            }.getType();
            Gson gson = new GsonBuilder().serializeNulls().create();
            styleModelArrayList = gson.fromJson(styleJsonArray, listType);

            setAllTheDefaultChildSelected();
        }

        if (MyUtils.checkStringValue(measurementValues)) {
            Type listType = new TypeToken<ArrayList<MeasurementValuesModel>>() {
            }.getType();
            Gson gson = new GsonBuilder().serializeNulls().create();
            measurementValueArrayList = gson.fromJson(measurementValues, listType);
        }
        if (MyUtils.checkStringValue(sizeValues)) {
            Type listType = new TypeToken<ArrayList<SizeModel>>() {
            }.getType();
            Gson gson = new GsonBuilder().serializeNulls().create();
            sizeModelArrayList = gson.fromJson(sizeValues, listType);
        }

        if (MyUtils.checkStringValue(featuredImageUrl)) {
            Glide.with(context).load(featuredImageUrl).into(featuredImageView);
        }


        getMeasurementValues();

    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.imgBack:
                finish();
                MyUtils.openOverrideAnimation(false, BodyMeasurement.this);
                break;

            case R.id.linear_summary_button:
                validateBodyMeasurement();
                break;

            case R.id.ab_linear_shoulder:
                measureBodySize(Constants.SHOULDER);
                break;

            case R.id.ab_linear_chest:
                measureBodySize(Constants.CHEST);
                break;

            case R.id.ab_linear_arm:
                measureBodySize(Constants.ARM);
                break;

            case R.id.ab_linear_arm_length:
                measureBodySize(Constants.ARM_LENGTH);
                break;

            case R.id.ab_linear_waist:
                measureBodySize(Constants.WAIST);
                break;

            case R.id.ab_imgTap:
                measureBodySize(Constants.SHOULDER);
                break;

            case R.id.ab_linearCut:
                if (styleModelArrayList.size() > 0) {
                    BottomSheetExpendable bottomSheetExpendable = new BottomSheetExpendable(styleModelArrayList);
                    bottomSheetExpendable.show(getSupportFragmentManager(), "tag");
                }
                break;

        }
    }

    private void setAllTheDefaultChildSelected() {
        if (styleModelArrayList.size() > 0) {
            for (int i = 0; i < styleModelArrayList.size(); i++) {
                if (styleModelArrayList.get(i).getValue().size() > 0) {
                    styleModelArrayList.get(i).getValue().get(0).setSelected(true);
                }
            }
        }
    }


    private void measureBodySize(String bodyPart) {
        if (styleModelArrayList.size() > 0) {
            MeasurementSizeDialog measurementSizeDialog = new MeasurementSizeDialog(BodyMeasurement.this, bodyPart, measureSizeInchArrayList, onSizeMeasure, styleModelArrayList);
            measurementSizeDialog.showAlertDialog();
        }
    }


    private ArrayList<MeasureSizeInch> getMeasureInchArrayList() {
        ArrayList<MeasureSizeInch> arrayList = new ArrayList<>();
        arrayList.add(new MeasureSizeInch(R.drawable.measure_shoulder_size, "Shoulder Size", "Please enter your Shoulder Size(Inch)", "Enter shoulder size(Inch)", 0, ""));
        arrayList.add(new MeasureSizeInch(R.drawable.measure_chest_size, "Chest Size", "Please enter your Chest Size(Inch)", "Enter chest size(Inch)", 1, ""));
        arrayList.add(new MeasureSizeInch(R.drawable.measure_arm_size, "Arm Size", "Please enter your Arm Size(Inch)", "Enter arm size(Inch)", 2, ""));
        arrayList.add(new MeasureSizeInch(R.drawable.measure_arm_length, "Arm Length", "Please enter your Arm Length(Inch)", "Enter arm length(Inch)", 3, ""));
        arrayList.add(new MeasureSizeInch(R.drawable.measure_waist_size, "Waist Size", "Please enter your Waist Size(Inch)", "Enter waist size(Inch)", 4, ""));
        return arrayList;
    }

    @Override
    public void onSizeMeasure(int index, String value) {

        if (MyUtils.checkStringValue(value)) {
            if (index == Constants.SHOULDER_INDEX) {
                txtShoulder.setText(MyUtils.checkStringValue(value) ? value : "0.0");
                measurementValueArrayList.get(0).setValue(value);
            } else if (index == Constants.CHEST_INDEX) {
                txtChest.setText(MyUtils.checkStringValue(value) ? value : "0.0");
                measurementValueArrayList.get(1).setValue(value);
            } else if (index == Constants.ARM_INDEX) {
                txtArm.setText(MyUtils.checkStringValue(value) ? value : "0.0");
                measurementValueArrayList.get(2).setValue(value);
            } else if (index == Constants.ARM_LENGTH_INDEX) {
                txtArmLength.setText(MyUtils.checkStringValue(value) ? value : "0.0");
                measurementValueArrayList.get(3).setValue(value);
            } else if (index == Constants.WAIST_INDEX) {
                txtWaist.setText(MyUtils.checkStringValue(value) ? value : "0.0");
                measurementValueArrayList.get(4).setValue(value);
            }
        }


    }

    private void validateBodyMeasurement() {

        String shoulderValue = txtShoulder.getText().toString();
        String chestValue = txtChest.getText().toString();
        String armValue = txtArm.getText().toString();
        String armLengthValue = txtArmLength.getText().toString();
        String waistValue = txtWaist.getText().toString();
        String remarks = edtRemarks.getText().toString();

        if (MyUtils.checkStringValue(shoulderValue) && shoulderValue.equalsIgnoreCase("0.0")) {
            showAlertToUser(Constants.DIALOG_WARNING, "Shoulder Value required");
        } else if (MyUtils.checkStringValue(chestValue) && chestValue.equalsIgnoreCase("0.0")) {
            showAlertToUser(Constants.DIALOG_WARNING, "Chest Value required");
        } else if (MyUtils.checkStringValue(armValue) && armValue.equalsIgnoreCase("0.0")) {
            showAlertToUser(Constants.DIALOG_WARNING, "Arm Value required");
        } else if (MyUtils.checkStringValue(armLengthValue) && armLengthValue.equalsIgnoreCase("0.0")) {
            showAlertToUser(Constants.DIALOG_WARNING, "Arm Length Value required");
        } else if (MyUtils.checkStringValue(waistValue) && waistValue.equalsIgnoreCase("0.0")) {
            showAlertToUser(Constants.DIALOG_WARNING, "Waist Value required");
        } else if (!MyUtils.checkStringValue(edtRemarks.getText().toString())) {
            MyUtils.ShowLongToast(BodyMeasurement.this, "Please Enter Remarks");
        } else {
            putValuesToHashMap();
            intentToSummaryPage(shoulderValue, chestValue, armValue, armLengthValue, waistValue, remarks, map);
        }


    }

    private void putValuesToHashMap() {

        if (styleModelArrayList.size() > 0) {

            for (int i = 0; i < styleModelArrayList.size(); i++) {

                String groupName = styleModelArrayList.get(i).getName();
                String section = styleModelArrayList.get(i).getSection();
                ArrayList<StyleModel.Value> childArrayList = styleModelArrayList.get(i).getValue();

                for (int k = 0; k < childArrayList.size(); k++) {
                    boolean isSelected = childArrayList.get(k).isSelected();
                    if (isSelected) {
                        String styleName = childArrayList.get(k).getDetails();
                        String styleType = childArrayList.get(k).getType();
                        String image = childArrayList.get(k).getImage();

                        if (groupName.equalsIgnoreCase("Collar")) {

                            map.put("collar_name", groupName);
                            map.put("collar_type", styleType);
                            map.put("collar_value", styleName);
                            map.put("collar_section", section);
                            map.put("collar_key", styleName + "_1");
                            map.put("collar_image", image);
                        }

                        if (groupName.equalsIgnoreCase("Cuff")) {

                            map.put("cuff_name", groupName);
                            map.put("cuff_type", styleType);
                            map.put("cuff_value", styleName);
                            map.put("cuff_section", section);
                            map.put("cuff_key", styleName + "_1");
                            map.put("cuff_image", image);
                        }

                        if (groupName.equalsIgnoreCase("Sleeve")) {

                            map.put("sleeve_name", groupName);
                            map.put("sleeve_type", styleType);
                            map.put("sleeve_value", styleName);
                            map.put("sleeve_section", section);
                            map.put("sleeve_key", styleName + "_1");
                            map.put("sleeve_image", image);
                        }

                        if (groupName.equalsIgnoreCase("Fitting")) {

                            map.put("fitting_name", groupName);
                            map.put("fitting_type", styleType);
                            map.put("fitting_value", styleName);
                            map.put("fitting_section", section);
                            map.put("fitting_key", styleName + "_1");
                            map.put("fitting_image", image);
                        }

                        if (groupName.equalsIgnoreCase("Chest Pocket")) {

                            map.put("chest_pocket_name", groupName);
                            map.put("chest_pocket_type", styleType);
                            map.put("chest_pocket_value", styleName);
                            map.put("chest_pocket_section", section);
                            map.put("chest_pocket_key", styleName + "_1");
                            map.put("chest_pocket_image", image);
                        }


                    }
                }
            }

        }


        if (measurementValueArrayList.size() > 0) {

            for (int i = 0; i < measurementValueArrayList.size(); i++) {

                String css_class = "";
                css_class = measurementValueArrayList.get(i).getCssclass();
                String section = "";
                section = measurementValueArrayList.get(i).getSection();
                String name = "";
                name = measurementValueArrayList.get(i).getName();
                String value = "";
                value = measurementValueArrayList.get(i).getValue();

                if (name.equalsIgnoreCase("Shoulder")) {
                    map.put("sholder_class", css_class);
                    map.put("sholder_name", name);
                    map.put("sholder_value", value);
                    map.put("sholder_section", section);
                }

                if (name.equalsIgnoreCase("Chest")) {
                    map.put("chest_class", css_class);
                    map.put("chest_name", name);
                    map.put("chest_value", value);
                    map.put("chest_section", section);
                }

                if (name.equalsIgnoreCase("Arm")) {
                    map.put("arm_class", css_class);
                    map.put("arm_name", name);
                    map.put("arm_value", value);
                    map.put("arm_section", section);
                }

                if (name.equalsIgnoreCase("Arm Length")) {
                    map.put("arm_Length_class", css_class);
                    map.put("arm_length_name", name);
                    map.put("arm_length_value", value);
                    map.put("arm_length_section", section);
                }

                if (name.equalsIgnoreCase("Waist Length")) {
                    map.put("waist_length_class", css_class);
                    map.put("waist_length_name", name);
                    map.put("waist_length_value", value);
                    map.put("waist_length_section", section);
                }

                if (name.equalsIgnoreCase("Remark")) {
                    map.put("remark_class", css_class);
                    map.put("remark_name", name);
                    map.put("remark_section", section);
                }

            }
        }


        if (sizeModelArrayList.size() > 0) {

            for (int i = 0; i < sizeModelArrayList.size(); i++) {

                String section = "";
                section = sizeModelArrayList.get(i).getSection();
                String name = "";
                name = sizeModelArrayList.get(i).getName();
                String cssClass = "";
                cssClass = sizeModelArrayList.get(i).getCssclass();

                map.put("size_class", cssClass);
                map.put("size_name", name);
                map.put("size_key", name + "_0");
                map.put("size_section", section);

            }
        }

    }

    private void intentToSummaryPage(String shoulderValue, String chestValue, String armValue, String armLengthValue, String waistValue, String remarks, HashMap<String, String> hashMap) {
        Bundle bundle = new Bundle();
        bundle.putString(Constants.IS_FROM, Constants.IS_FROM_MEASUREMENT);
        bundle.putString(Constants.PRODUCT_ID, productId);
        bundle.putString(Constants.PRODUCT_NAME, productName);
        bundle.putString(Constants.PRODUCT_PRICE, productPrice);
        bundle.putString(Constants.PRODUCT_IMAGE_URL, imgUrl);

        bundle.putString(Constants.SHOULDER, shoulderValue);
        bundle.putString(Constants.CHEST, chestValue);
        bundle.putString(Constants.ARM, armValue);
        bundle.putString(Constants.ARM_LENGTH, armLengthValue);
        bundle.putString(Constants.WAIST, waistValue);
        bundle.putString(Constants.SIZE, size);
        bundle.putString(Constants.REMARKS, remarks);
        bundle.putSerializable(Constants.STYLE_MAP, hashMap);

        Intent i = new Intent(BodyMeasurement.this, SummaryActivity.class);
        i.putExtras(bundle);
        startActivity(i);
        MyUtils.openOverrideAnimation(true, BodyMeasurement.this);
    }


    private void showAlertToUser(String statusCode, String statusMessage) {
        CustomDialog customDialog = new CustomDialog(BodyMeasurement.this, statusCode, statusMessage, new DialogInterface() {
            @Override
            public void onButtonClicked() {

            }

            @Override
            public void onDismissedClicked() {

            }
        });
        customDialog.showDialog();
    }


    private void getMeasurementValues() {
        try {
            progressDialog = MyUtils.showProgressLoader(context, "Loading...");
            ApiInterface apiService = HttpRequest.getScalarsInstanceWC().create(ApiInterface.class);
            Call<String> call = apiService.getMeasurements(userId);
            call.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    MyUtils.dismissProgressLoader(progressDialog);
                    try {
                        if (response != null) {
                            Gson gson = new GsonBuilder().serializeNulls().create();
                            GetMeasurementsModel responseMeasurements = gson.fromJson(response.body(), new TypeToken<GetMeasurementsModel>() {
                            }.getType());
                            if (responseMeasurements != null) {
                                updateUI(responseMeasurements);
                            }
                        }

                    } catch (Exception e) {
                        Log.e("exception33", ":" + e.getMessage());
                    }
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    Log.e("exception11", ":" + t.getMessage());
                    MyUtils.dismissProgressLoader(progressDialog);

                }
            });
        } catch (Exception e) {
            Log.e("exception22", ":" + e);
            MyUtils.dismissProgressLoader(progressDialog);
        }
    }

    private void updateUI(GetMeasurementsModel response) {
        txtShoulder.setText(response.getBodyMesurments().get(0).getMeasurementsShirtShoulder());
        txtChest.setText(response.getBodyMesurments().get(0).getMeasurementsShirtChest());
        txtArm.setText(response.getBodyMesurments().get(0).getMeasurementsShirtArmSize());
        txtArmLength.setText(response.getBodyMesurments().get(0).getMeasurementsShirtArmLength());
        txtWaist.setText(response.getBodyMesurments().get(0).getMeasurementsShirtWaist());
    }
}
