package com.bestweb.custlr.activities;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.ProgressBar;

import androidx.appcompat.app.AppCompatActivity;
import com.bestweb.custlr.R;
import com.bestweb.custlr.utils.MyUtils;
import com.bestweb.custlr.utils.RequestParamUtils;
import com.bestweb.custlr.web_service.ApiInterface;
import com.bestweb.custlr.web_service.HttpRequest;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CheckoutActivity extends AppCompatActivity {

    private Context context;
    private ImageView imgBack;
    private WebView webView;
    private ProgressBar progressBar;

    private String thankYouMainUrl = null;
    private String thankYouUrl = null;
    private String checkOutUrl = null;
    private String homeUrl = null;
    private String jsonInput = null;

    private boolean isfirstLoad = false;
    List<String> checkoutURLList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout);
        context = CheckoutActivity.this;

        init();
    }


    @SuppressLint("JavascriptInterface")
    private void init() {
        imgBack = findViewById(R.id.ach_imgBack);
        webView = findViewById(R.id.ach_webView);
        progressBar = findViewById(R.id.ach_progressBar);

        Bundle bundle = getIntent().getExtras();
        thankYouMainUrl = bundle.getString(RequestParamUtils.THANKYOU);
        thankYouUrl = bundle.getString(RequestParamUtils.THANKYOUEND);
        checkOutUrl = bundle.getString(RequestParamUtils.CHECKOUT_URL);
        homeUrl = bundle.getString(RequestParamUtils.HOME_URL);
        jsonInput = bundle.getString(RequestParamUtils.CHECKOUT_JSON_INPUT);

        Log.e("jsonInput", ":" + jsonInput);

        checkoutURLList.add(thankYouMainUrl);
        checkoutURLList.add(thankYouUrl);

        webView.getSettings().setLoadsImagesAutomatically(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setDomStorageEnabled(true);
        webView.addJavascriptInterface(new WebAppInterface(this), "Android");
        webView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            CookieManager.getInstance().setAcceptThirdPartyCookies(webView, true);
        }

        webView.setWebViewClient(new myWebClient());
        CookieManager.getInstance().setAcceptCookie(true);

        progressBar.setVisibility(View.VISIBLE);

        webView.postUrl(checkOutUrl, jsonInput.getBytes());

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (webView.canGoBack()) {
                    webView.goBack();
                } else {
                    CookieManager.getInstance().removeAllCookie();
                    webView.clearCache(true);
                    webView.clearHistory();
                    clearCookies(context);
                    logoutUser();
                }

            }
        });
    }


    @SuppressWarnings("deprecation")
    public void clearCookies(Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
            Log.e("log", "Using clearCookies code for API >=" + String.valueOf(Build.VERSION_CODES.LOLLIPOP_MR1));
            CookieManager.getInstance().removeAllCookies(null);
            CookieManager.getInstance().flush();
        } else {
            Log.e("log", "Using clearCookies code for API <" + String.valueOf(Build.VERSION_CODES.LOLLIPOP_MR1));
            CookieSyncManager cookieSyncMngr = CookieSyncManager.createInstance(context);
            cookieSyncMngr.startSync();
            CookieManager cookieManager = CookieManager.getInstance();
            cookieManager.removeAllCookie();
            cookieManager.removeSessionCookie();
            cookieSyncMngr.stopSync();
            cookieSyncMngr.sync();
        }


    }

    //Custom WebViewClient
    public class myWebClient extends WebViewClient {
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);

            Log.e("Responce", "Url called");

            String text = "";

            String[] separated = url.split("\\?");
            String checkurl = separated[0]; // this will contain "Fruit"

            for (int i = 0; i < checkoutURLList.size(); i++) {
                String value = checkoutURLList.get(i);
                if ((value.charAt(0) + "").contains("/")) {

                    Log.e("fullURL: ", "" + value);
                    StringBuilder sb = new StringBuilder(value);
                    sb.deleteCharAt(0);
                    value = sb.toString();

                    Log.e("Deleted url      : ", "" + value);
                }
                if ((value.charAt(value.length() - 1) + "").contains("/")) {

                    StringBuilder sb = new StringBuilder(value);
                    sb.deleteCharAt(value.length() - 1);
                    value = sb.toString();
                }

                if (!value.equals("") && value != null) {


                    if (checkurl.contains(value)) {
                        //text = Constant.CheckoutURL.get(i);
                        text = value;
                        break;
                    }
                }
            }
            Log.e("shouldOverride: ", "" + text);


            if (MyUtils.checkStringValue(text) && checkurl.contains(text)) {
                if (isfirstLoad) {

                    Intent intent = new Intent(context, ThankYouActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    webView.clearCache(true);
                    webView.clearHistory();
                    clearCookies(context);
                    isfirstLoad = false;
                } else {
                    Log.e("Else Condition ", "Called");
                }
            }
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            // TODO Auto-generated method stub

            view.loadUrl(url);
            return true;

        }

        @Override
        public void onPageCommitVisible(WebView view, String url) {
            super.onPageCommitVisible(view, url);

        }

        @Override
        public void onPageFinished(WebView view, String url) {
            // TODO Auto-generated method stub
            // progress.setVisibility(View.GONE);
            super.onPageFinished(view, url);

        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK) && webView.canGoBack()) {
            webView.goBack();
            return true;
        } else {
            CookieManager.getInstance().removeAllCookie();
            webView.clearCache(true);
            webView.clearHistory();
            clearCookies(context);
            logoutUser();
        }

        return super.onKeyDown(keyCode, event);
    }


    public class WebAppInterface {
        Context mContext;
        WebAppInterface(Context c) {
            mContext = c;
        }
        @JavascriptInterface
        public void showToast(final String toast) {
            Log.e("Title is ", toast);

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    dismissProgress();
                    if (toast != null) {
                        CookieManager.getInstance().setAcceptCookie(true);
                        webView.loadUrl(toast);
                        webView.setVisibility(View.VISIBLE);
                        isfirstLoad = true;
                        dismissProgress();
                    }
                }
            });

        }
    }

    private void dismissProgress() {
        if (progressBar.getVisibility() == View.VISIBLE) {
            progressBar.setVisibility(View.GONE);
        }
    }

    private void logoutUser() {
        try {
            ApiInterface apiService = HttpRequest.getScalarsInstance().create(ApiInterface.class);
            Call<String> call = apiService.logoutPayment("");
            call.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    try {
                        String jsonResponse = response.body();

                        Log.e("logout_response", ":" + jsonResponse);

                        if (jsonResponse != null && jsonResponse.length() > 0) {
                            try {
                                JSONObject jsonObj = new JSONObject(jsonResponse);
                                String status = jsonObj.getString("status");
                                if (status.equals("success")) {
                                    finish();
                                }
                            } catch (Exception e) {
                                Log.e("error", e.getMessage());
                            }
                        }

                    } catch (Exception e) {
                        Log.e("exception33", ":" + e.getMessage());
                    }
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    Log.e("exception11", ":" + t.getMessage());
                }
            });
        } catch (Exception e) {
            Log.e("exception22", ":" + e.getMessage());
        }
    }


}
