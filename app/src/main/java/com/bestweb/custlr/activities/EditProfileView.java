package com.bestweb.custlr.activities;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.bestweb.custlr.R;
import com.bestweb.custlr.interfaces.CustomAlertInterface;
import com.bestweb.custlr.interfaces.UploadAlertListener;
import com.bestweb.custlr.models.CustomerResponse;
import com.bestweb.custlr.utils.Constants;
import com.bestweb.custlr.utils.CustomAlert;
import com.bestweb.custlr.utils.MyUtils;
import com.bestweb.custlr.utils.PermissionAlert;
import com.bestweb.custlr.utils.PermissionRequest;
import com.bestweb.custlr.utils.PreferenceHandler;
import com.bestweb.custlr.utils.RealPathUtil;
import com.bestweb.custlr.utils.RequestParamUtils;
import com.bestweb.custlr.utils.UploadAlert;
import com.bestweb.custlr.web_service.ApiInterface;
import com.bestweb.custlr.web_service.HttpRequest;
import com.bestweb.custlr.web_service.JsonInput;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONObject;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditProfileView extends AppCompatActivity {

    private TextView txtHeader, txtName, txtEmail, txtAddress, txtDOB, txtPhoneNo;
    private ImageView imgBack;
    private LinearLayout linearLogout, linearAccountsInfo, linearWishList, linearBodyMeasure;
    private Context context;
    private CircleImageView circleImageView;

    private ProgressBar imageProgress;
    private ProgressDialog progressDialog = null;

    private String imageFilePath = null, userId = "";
    boolean isImageLoaded = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile_view);
        context = EditProfileView.this;

        init();
        listenter();

        getUserDetails();


    }

    private void init() {


        userId = MyUtils.getPreferences(context).getString(RequestParamUtils.ID, "");

        imgBack = findViewById(R.id.imgBack);
        txtHeader = findViewById(R.id.txtHeader);
        linearLogout = findViewById(R.id.aep_linearLogout);
        linearAccountsInfo = findViewById(R.id.aep_linearAccountInfo);
        linearWishList = findViewById(R.id.aep_linearWishList);
        linearBodyMeasure = findViewById(R.id.aep_linearBodyMeasure);
        circleImageView = findViewById(R.id.img_Profile);

        imageProgress = findViewById(R.id.aep__imageProgress);


        txtName = findViewById(R.id.aep_txtName);
        txtEmail = findViewById(R.id.aep_txtEmail);
        txtAddress = findViewById(R.id.aep_txtAddress);
        txtDOB = findViewById(R.id.aep_txtDOB);
        txtPhoneNo = findViewById(R.id.aep_txtPhoneNo);
        txtHeader.setText("EDIT PROFILE");


    }

    private void listenter() {

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                finish();
                MyUtils.openOverrideAnimation(false, EditProfileView.this);

            }
        });

        linearLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                CustomAlert customAlert = new CustomAlert(context, "Logout", "Do you wish to logout the app?", "YES", "NO", new CustomAlertInterface() {
                    @Override
                    public void onPositiveButtonClicked() {
                        PreferenceHandler.ClearPreferences(context);
                        intentToLoginPage();
                    }

                    @Override
                    public void onNegativeButtonClicked() {

                    }
                });
                customAlert.showDialog();
            }
        });


        linearAccountsInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(context, EditProfileActivity.class));
                MyUtils.openOverrideAnimation(true, EditProfileView.this);

            }
        });

        linearWishList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, WishlistActivity.class);
                startActivityForResult(intent, Constants.PRODUCTS);
                MyUtils.openOverrideAnimation(true, EditProfileView.this);
            }
        });

        linearBodyMeasure.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(context, ProfileBodyMeasure.class));
                MyUtils.openOverrideAnimation(true, EditProfileView.this);
            }
        });


        circleImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showUploadAlert();

            }
        });

    }

    private void intentToLoginPage() {
        Intent intent = new Intent(context, LoginActivity.class);
        startActivityForResult(intent, Constants.LOGIN_CODE);
        MyUtils.openOverrideAnimation(false, EditProfileView.this);
    }


    private void showUploadAlert() {
        UploadAlert uploadAlert = new UploadAlert(EditProfileView.this, new UploadAlertListener() {
            @Override
            public void onChooseFileClicked() {
                if (PermissionRequest.askForPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE, Constants.WRITE_STORAGE_FILES_PERMISSION, EditProfileView.this)) {
                    intentToImageSelection();
                }
            }

            @Override
            public void onCaptureCameraClicked() {
                if (PermissionRequest.askForPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE, Constants.WRITE_STORAGE_CAMERA_PERMISSION, EditProfileView.this)) {
                    intentToCameraApp();
                }
            }
        });
        uploadAlert.showAlertDialog();
    }


    private void intentToImageSelection() {
        isImageLoaded = false;
        imageFilePath = null;
        MyUtils.intentToImageSelection(EditProfileView.this);
    }

    private void intentToCameraApp() {
        isImageLoaded = false;
        imageFilePath = null;
        imageFilePath = MyUtils.intentToCameraApp(EditProfileView.this);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.e("reqCode", ": " + requestCode);
        Log.e("resultCode", ": " + resultCode);
        try {
            if (resultCode == Activity.RESULT_OK) {
                if (requestCode == Constants.PICK_IMAGE_FROM_FILES) {

                    imageFilePath = RealPathUtil.getPath(EditProfileView.this, data.getData());
                    Log.e("selectedFile", "path:" + imageFilePath);

                    if (MyUtils.isInternetConnected(context)) {
                        if (MyUtils.checkStringValue(imageFilePath)) {
                            loadImageSource(imageFilePath);
                            uploadUserImage(imageFilePath);
                        }
                    } else {
                        MyUtils.ShowLongToast(context, "Check your internet connection");
                    }
                } else if (requestCode == Constants.CAPTURE_IMAGE_FROM_CAMERA) {
                    if (MyUtils.isInternetConnected(context)) {
                        if (MyUtils.checkStringValue(imageFilePath)) {
                            Log.e("capturedImagePath", "path:" + imageFilePath);
                            if (MyUtils.checkStringValue(imageFilePath)) {
                                loadImageSource(imageFilePath);
                                uploadUserImage(imageFilePath);
                            }
                        }
                    } else {
                        MyUtils.ShowLongToast(context, "Check your internet connection");
                    }
                }
            }
            if (resultCode == Constants.LOGIN_FAILED) {
                setResult(Constants.LOGIN_FAILED);
                finish();
            }

        } catch (Exception e) {
            Log.e("FileSelectorActivity", "File select error", e);
        }
    }

    private void loadImageSource(String imageFilePath) {
        imageProgress.setVisibility(View.VISIBLE);
        Glide.with(EditProfileView.this).load(imageFilePath).placeholder(R.drawable.profile_picture_big).listener(new RequestListener<Drawable>() {
            @Override
            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                imageProgress.setVisibility(View.GONE);
                isImageLoaded = false;
                return false;
            }

            @Override
            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                imageProgress.setVisibility(View.GONE);
                isImageLoaded = true;
                return false;
            }
        }).into(circleImageView);
    }

    public void uploadUserImage(String imageFilePath) {
        try {
            String base64 = MyUtils.getBase64FromPath(imageFilePath);
            uploadImage(base64);
        } catch (Exception e) {
            Log.e("error", e.getMessage());
        }
    }


    private void uploadImage(String base64String) {
        try {
            JSONObject object = new JSONObject();
            object.put(RequestParamUtils.data, base64String);
            object.put(RequestParamUtils.name, "image.jpg");

            progressDialog = MyUtils.showProgressLoader(context, "Uploading..");
            ApiInterface apiService = HttpRequest.getScalarsInstance().create(ApiInterface.class);
            Call<String> call = apiService.updateUserImage(JsonInput.updateUserImage(object, userId));
            call.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    MyUtils.dismissProgressLoader(progressDialog);
                    try {
                        if (response != null) {
                            Gson gson = new GsonBuilder().serializeNulls().create();
                            CustomerResponse customerResponse = gson.fromJson(response.body(), new TypeToken<CustomerResponse>() {
                            }.getType());

                            if (customerResponse != null) {
                                if (customerResponse.getStatus().equalsIgnoreCase(Constants.SUCCESS)) {
                                    MyUtils.ShowLongToast(context, "Profile Image Successfully Uploaded");
                                    PreferenceHandler.storePreference(context, Constants.UPLOAD_URL, customerResponse.getPgsProfileImage());
                                } else {
                                    MyUtils.ShowLongToast(context, "Upload Failed Try Again ..");
                                }

                            }
                        }

                    } catch (Exception e) {
                        Log.e("exception33", ":" + e.getMessage());
                    }
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    Log.e("exception11", ":" + t.getMessage());
                    MyUtils.dismissProgressLoader(progressDialog);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            MyUtils.dismissProgressLoader(progressDialog);
        }


    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case Constants.WRITE_STORAGE_FILES_PERMISSION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    intentToImageSelection();
                    return;
                } else {
                    if (this == null) {
                        return;
                    }
                    if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                        Log.e("permission", "denied");
                        return;
                    } else {
                        Log.e("permission", "completely denied");

                        PermissionAlert permissionAlert = new PermissionAlert(this, "STORAGE");
                        permissionAlert.show();
                    }
                }
                break;
            case Constants.WRITE_STORAGE_CAMERA_PERMISSION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    intentToCameraApp();

                    return;
                } else {
                    if (this == null) {
                        return;
                    }
                    if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                        Log.e("permission", "denied");
                        return;
                    } else {
                        Log.e("permission", "completely denied");

                        PermissionAlert permissionAlert = new PermissionAlert(this, "STORAGE");
                        permissionAlert.show();
                    }
                }
                break;
            default:
                break;
        }
    }


    private void getUserDetails() {
        try {
            progressDialog = MyUtils.showProgressLoader(context, "Fetching Details...");
            ApiInterface apiService = HttpRequest.getScalarsInstance().create(ApiInterface.class);
            Call<String> call = apiService.getCustomerDetails(JsonInput.getCustomerDetails(userId));
            call.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    MyUtils.dismissProgressLoader(progressDialog);
                    try {
                        if (response != null) {
                            Gson gson = new GsonBuilder().serializeNulls().create();
                            CustomerResponse customerResponse = gson.fromJson(response.body(), new TypeToken<CustomerResponse>() {
                            }.getType());

                            //datumArrayList = new ArrayList<>(Arrays.asList(customerResponse.getMetaData()));

                            if (customerResponse != null) {
                                updateUI(customerResponse);

                            }
                        }

                    } catch (Exception e) {
                        Log.e("exception33", ":" + e.getMessage());
                    }
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    Log.e("exception11", ":" + t.getMessage());
                    MyUtils.dismissProgressLoader(progressDialog);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            MyUtils.dismissProgressLoader(progressDialog);
        }


    }

    private void updateUI(CustomerResponse response) {

        String userName = response.getUsername();
        String email = response.getEmail();

        Log.e("email", ":" + email);

        String address = response.getBilling().getAddress1();
        String phoneNo = response.getBilling().getPhone();
        String profilePic = response.getPgsProfileImage();

        Log.e("profilePic",":"+profilePic);

        if (MyUtils.checkStringValue(userName)) {
            txtName.setText(userName);
        }
        if (MyUtils.checkStringValue(email)) {
            txtEmail.setText(email);
        }
        if (MyUtils.checkStringValue(address)) {
            txtAddress.setText(address);
        }

        if (MyUtils.checkStringValue(phoneNo)) {
            txtPhoneNo.setText(phoneNo);
        }

        if (MyUtils.checkStringValue(profilePic)) {
            loadImageSource(profilePic);
        }

        updateDOBToUI();


        // String dob = "", mobileNo = "";

       /* for (int i = 0; i < arrayList.size(); i++) {
            String key = arrayList.get(i).getKey();
            if (key.equalsIgnoreCase("mobile")) {
                mobileNo = arrayList.get(i).getValue();
            } else if (key.equalsIgnoreCase("dob")) {
                dob = arrayList.get(i).getValue();
            }
        }

        if (MyUtils.checkStringValue(dob)) {
            txtDOB.setText(dob);
        }  if (MyUtils.checkStringValue(mobileNo)) {
            txtPhoneNo.setText(mobileNo);
        }*/


    }


    private void updateDOBToUI() {
        String dateOfBirth = MyUtils.getPreferences(context).getString(RequestParamUtils.DATE_OF_BIRTH, "");
        if (MyUtils.checkStringValue(dateOfBirth)) {
            txtDOB.setText(dateOfBirth);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateDOBToUI();
    }
}
