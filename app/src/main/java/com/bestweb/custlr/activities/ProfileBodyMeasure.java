package com.bestweb.custlr.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.bestweb.custlr.R;
import com.bestweb.custlr.models.GetMeasurementsModel;
import com.bestweb.custlr.models.UpdateMeasurementModel;
import com.bestweb.custlr.utils.MyUtils;
import com.bestweb.custlr.utils.RequestParamUtils;
import com.bestweb.custlr.web_service.ApiInterface;
import com.bestweb.custlr.web_service.HttpRequest;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileBodyMeasure extends AppCompatActivity implements View.OnClickListener {

    private TextView txtHeader;
    private ImageView imgBack,featuredImageView;
    private EditText txtShoulder, txtChest, txtArm, txtArmLength, txtWaist;
    private ProgressDialog progressDialog = null;
    private LinearLayout linearShoulder, linearChest, linearArm, linearArmLength, linearWaist;
    private RelativeLayout relativeSummary;

    private String userId = "";
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_body_measurements);
        context = ProfileBodyMeasure.this;

        userId = MyUtils.getPreferences(context).getString(RequestParamUtils.ID, "");

        imgBack = findViewById(R.id.imgBack);
        txtHeader = findViewById(R.id.txtHeader);
        txtHeader.setText("BODY MEASUREMENTS");


        featuredImageView = findViewById(R.id.ab_featuredImageView);
        txtShoulder = findViewById(R.id.ap_txtShoulder);
        txtChest = findViewById(R.id.ap_txtChest);
        txtArm = findViewById(R.id.ap_txtArm);
        txtArmLength = findViewById(R.id.ap_txtArmLength);
        txtWaist = findViewById(R.id.ap_txtWaist);

        relativeSummary = findViewById(R.id.apb_linearSummaryButton);
        linearShoulder = findViewById(R.id.apb_linear_shoulder);
        linearChest = findViewById(R.id.apb_linear_chest);
        linearArm = findViewById(R.id.apb_linear_arm);
        linearArmLength = findViewById(R.id.apb_linear_arm_length);
        linearWaist = findViewById(R.id.apb_linear_waist);

        listener();

    }

    private void listener() {
        Glide.with(context).load(R.drawable.body_measurements).into(featuredImageView);

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                MyUtils.openOverrideAnimation(false, ProfileBodyMeasure.this);
            }
        });

        relativeSummary.setOnClickListener(this);
        linearShoulder.setOnClickListener(this);
        linearChest.setOnClickListener(this);
        linearArm.setOnClickListener(this);
        linearArmLength.setOnClickListener(this);
        linearWaist.setOnClickListener(this);

        getMeasurementValues();
    }


    private void getMeasurementValues() {
        try {
            progressDialog = MyUtils.showProgressLoader(context, "Loading...");
            ApiInterface apiService = HttpRequest.getScalarsInstanceWC().create(ApiInterface.class);
            Call<String> call = apiService.getMeasurements(userId);
            call.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    MyUtils.dismissProgressLoader(progressDialog);
                    try {
                        if (response != null) {
                            Gson gson = new GsonBuilder().serializeNulls().create();
                            GetMeasurementsModel responseMeasurements = gson.fromJson(response.body(), new TypeToken<GetMeasurementsModel>() {
                            }.getType());
                            if (responseMeasurements != null) {
                                updateUI(responseMeasurements);
                            }
                        }

                    } catch (Exception e) {
                        Log.e("exception33", ":" + e.getMessage());
                    }
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    Log.e("exception11", ":" + t.getMessage());
                    MyUtils.dismissProgressLoader(progressDialog);

                }
            });
        } catch (Exception e) {
            Log.e("exception22", ":" + e);
            MyUtils.dismissProgressLoader(progressDialog);
        }
    }


    private void updateMeasurement(String shoulder, String chest, String arm, String armLength, String waist) {
        try {
            progressDialog = MyUtils.showProgressLoader(context, "Loading...");
            ApiInterface apiService = HttpRequest.getScalarsInstanceWC().create(ApiInterface.class);
            Call<String> call = apiService.updateMeasurements(userId, shoulder, chest, arm, armLength, waist);
            call.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    MyUtils.dismissProgressLoader(progressDialog);
                    try {
                        if (response != null) {
                            Gson gson = new GsonBuilder().serializeNulls().create();
                            UpdateMeasurementModel customerResponse = gson.fromJson(response.body(), new TypeToken<UpdateMeasurementModel>() {
                            }.getType());
                            if (customerResponse != null) {
                                if (customerResponse.getStatus().equalsIgnoreCase("1")) {
                                    Intent i = new Intent(context, EditProfileView.class);
                                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(i);
                                    finish();
                                    MyUtils.ShowLongToast(context, "Profile Successfully Updated");
                                } else {
                                    MyUtils.ShowLongToast(context, "Update Failed Try Again ..");
                                }
                            }
                        }

                    } catch (Exception e) {
                        Log.e("exception33", ":" + e.getMessage());
                    }
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    Log.e("exception11", ":" + t.getMessage());
                    MyUtils.dismissProgressLoader(progressDialog);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            MyUtils.dismissProgressLoader(progressDialog);
        }


    }

    private void updateUI(GetMeasurementsModel response) {
        txtShoulder.setText(response.getBodyMesurments().get(0).getMeasurementsShirtShoulder());
        txtChest.setText(response.getBodyMesurments().get(0).getMeasurementsShirtChest());
        txtArm.setText(response.getBodyMesurments().get(0).getMeasurementsShirtArmSize());
        txtArmLength.setText(response.getBodyMesurments().get(0).getMeasurementsShirtArmLength());
        txtWaist.setText(response.getBodyMesurments().get(0).getMeasurementsShirtWaist());

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.apb_linearSummaryButton:
                validateBodyMeasurement();
                break;

        }
    }

    private void validateBodyMeasurement() {

        String shoulderValue = txtShoulder.getText().toString();
        String chestValue = txtChest.getText().toString();
        String armValue = txtArm.getText().toString();
        String armLengthValue = txtArmLength.getText().toString();
        String waistValue = txtWaist.getText().toString();

        if (MyUtils.isInternetConnected(context)) {
            updateMeasurement(shoulderValue, chestValue, armValue, armLengthValue, waistValue);

        } else {
            MyUtils.ShowLongToast(ProfileBodyMeasure.this, "Please Check your Internet");
        }


    }


}
