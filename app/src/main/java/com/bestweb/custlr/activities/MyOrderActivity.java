package com.bestweb.custlr.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bestweb.custlr.R;
import com.bestweb.custlr.adapters.MyOrderAdapter;
import com.bestweb.custlr.interfaces.RecyclerClickListener;
import com.bestweb.custlr.models.Orders;
import com.bestweb.custlr.utils.Constants;
import com.bestweb.custlr.utils.MyUtils;
import com.bestweb.custlr.utils.RequestParamUtils;
import com.bestweb.custlr.web_service.ApiInterface;
import com.bestweb.custlr.web_service.HttpRequest;
import com.bestweb.custlr.web_service.JsonInput;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyOrderActivity extends AppCompatActivity {

    private Context context;
    private TextView titleTxt;
    private ImageView imgBack;
    private RecyclerView recyclerView;
    private LinearLayoutManager linearLayoutManager;
    private ProgressBar progressBar, bottomProgressBar;
    private RelativeLayout emptyViewRelative;
    private TextView emptyTxtTitle, emptyTxtContent;

    private String userId = null;

    private ArrayList<Orders> ordersArrayList = new ArrayList<>();
    private MyOrderAdapter myOrderAdapter = null;

    private boolean isLoading = true;
    private int pageNumber = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wishlist);
        context = MyOrderActivity.this;

        init();
    }


    private void init() {

        titleTxt = findViewById(R.id.aw_titleNameTxt);
        imgBack = findViewById(R.id.aw_imgBack);
        recyclerView = findViewById(R.id.aw_recycler);
        progressBar = findViewById(R.id.aw_progressBar);
        bottomProgressBar = findViewById(R.id.aw_bottomProgressBar);

        emptyViewRelative = findViewById(R.id.aw_emptyViewRelative);
        emptyTxtTitle = findViewById(R.id.aw_emptyTxtTitle);
        emptyTxtContent = findViewById(R.id.aw_emptyTxtContent);

        titleTxt.setText("MY ORDERS");

        linearLayoutManager = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);

        checkIfUserLoggedIn();

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                closePage();
            }
        });
    }

    private void checkIfUserLoggedIn() {
        userId = MyUtils.getPreferences(context).getString(RequestParamUtils.ID, "");

        if (MyUtils.checkIfUserLoggedIn(context) && MyUtils.checkStringValue(userId)) {


            getOrderList(1, true);


        } else {

            intentToLoginPage();
        }
    }


    private void getOrderList(int pageNumber, final boolean isLoadingForFirstTime) {
        try {

            if (isLoadingForFirstTime) {
                ordersArrayList.clear();
                progressBar.setVisibility(View.VISIBLE);
            } else {
                bottomProgressBar.setVisibility(View.VISIBLE);
            }
            ApiInterface apiService = HttpRequest.getScalarsInstance().create(ApiInterface.class);
            Log.e("myOrders", ":" + "pageNumber: " + pageNumber + " -- " + "sortType :" + userId);

            Call<String> call = apiService.getMyOrdersList(JsonInput.myOrdersInput(pageNumber, userId));
            call.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    hideProgressBar(isLoadingForFirstTime);
                    try {
                        String jsonResponse = response.body();

                        Log.e("prdResponse", ":" + jsonResponse);

                        if (jsonResponse != null) {

                            //{"status":"error","message":"No product found"}

                            Type listType = new TypeToken<ArrayList<Orders>>() {
                            }.getType();
                            Gson gson = new GsonBuilder().serializeNulls().create();
                            ArrayList<Orders> arrayList = gson.fromJson(jsonResponse, listType);

                            ordersArrayList.addAll(arrayList);

                            adaptOrdersList(ordersArrayList, isLoadingForFirstTime);


                        } else {
                            showErrorCase(isLoadingForFirstTime);
                        }

                    } catch (Exception e) {
                        if (!isLoadingForFirstTime) {
                            isLoading = isLoadingForFirstTime;
                        }

                        Log.e("exception33", ":" + e.getMessage());
                        showErrorCase(isLoadingForFirstTime);
                    }
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    Log.e("exception11", ":" + t.getMessage());
                    hideProgressBar(isLoadingForFirstTime);
                    showErrorCase(isLoadingForFirstTime);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            hideProgressBar(isLoadingForFirstTime);
            showErrorCase(isLoadingForFirstTime);
        }
    }


    private void adaptOrdersList(ArrayList<Orders> ordersArrayList, boolean isLoadingForFirstTime) {
        if (ordersArrayList.size() > 0) {

            resetView(isLoadingForFirstTime);

            Log.e("size", ":" + ordersArrayList.size());

            if (myOrderAdapter != null) {
                myOrderAdapter.notifyDataSetChanged();
                isLoading = true;
            } else {

                RecyclerClickListener recyclerClickListener = new RecyclerClickListener() {
                    @Override
                    public void onItemClicked(int position) {


                    }
                };

                myOrderAdapter = new MyOrderAdapter(context, ordersArrayList, recyclerClickListener);
                recyclerView.setAdapter(myOrderAdapter);

            }

            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                    if (dy > 0) //check for scroll down
                    {
                        int visibleItemCount = linearLayoutManager.getChildCount();
                        int totalItemCount = linearLayoutManager.getItemCount();
                        int pastVisibleItems = linearLayoutManager.findFirstVisibleItemPosition();

                        if (isLoading) {
                            if ((visibleItemCount + pastVisibleItems) >= totalItemCount) {
                                isLoading = false;
                                Log.e("recyclerView", "bottom_reached");
                                pageNumber = pageNumber + 1;

                                getOrderList(pageNumber, false);

                                Log.e("page_number", "number:" + pageNumber);
                            }
                        }
                    }
                }
            });


        } else {
            showErrorCase(isLoadingForFirstTime);
        }
    }


    private void hideProgressBar(boolean isLoadingForFirstTime) {
        if (isLoadingForFirstTime) {
            if (progressBar.getVisibility() == View.VISIBLE) {
                progressBar.setVisibility(View.GONE);
            }
        } else {
            hideBottomProgressBar();
        }
    }


    private void hideBottomProgressBar() {
        if (bottomProgressBar.getVisibility() == View.VISIBLE) {
            bottomProgressBar.setVisibility(View.GONE);
        }
    }

    private void showErrorCase(boolean isLoadingForFirstTime) {

        if (isLoadingForFirstTime) {

            if (recyclerView.getVisibility() == View.VISIBLE) {
                recyclerView.setVisibility(View.GONE);
            }

            if (emptyViewRelative.getVisibility() == View.GONE) {
                emptyViewRelative.setVisibility(View.VISIBLE);

                emptyTxtTitle.setText("Empty Orders");
                emptyTxtContent.setText("There is no item in orders");
            }
        } else {
            hideBottomProgressBar();
        }
    }

    private void resetView(boolean isLoadingForFirstTime) {

        if (isLoadingForFirstTime) {
            if (recyclerView.getVisibility() == View.GONE) {
                recyclerView.setVisibility(View.VISIBLE);
            }

            if (emptyViewRelative.getVisibility() == View.VISIBLE) {
                emptyViewRelative.setVisibility(View.GONE);
            }
        }
    }


    private void intentToLoginPage() {
        Intent intent = new Intent(context, LoginActivity.class);
        startActivityForResult(intent, Constants.LOGIN_CODE);
        MyUtils.openOverrideAnimation(false, MyOrderActivity.this);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constants.LOGIN_CODE) {
            if (resultCode == Constants.LOGIN_SUCCESS) {

                checkIfUserLoggedIn();

            } else if (resultCode == Constants.LOGIN_FAILED) {
                if (!MyUtils.checkIfUserLoggedIn(context)) {
                    finish();
                }
            }
        }
    }

    @Override
    public void onBackPressed() {
        closePage();
    }

    private void closePage() {
        finish();
        MyUtils.openOverrideAnimation(false, MyOrderActivity.this);
    }


}
