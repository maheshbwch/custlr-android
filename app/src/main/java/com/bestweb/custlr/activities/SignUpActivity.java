package com.bestweb.custlr.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import com.bestweb.custlr.R;
import com.bestweb.custlr.interfaces.DialogInterface;
import com.bestweb.custlr.models.RegisterResponse;
import com.bestweb.custlr.utils.APIS;
import com.bestweb.custlr.utils.Constants;
import com.bestweb.custlr.utils.CustomDialog;
import com.bestweb.custlr.utils.MyUtils;
import com.bestweb.custlr.web_service.ApiInterface;
import com.bestweb.custlr.web_service.HttpRequest;
import com.bestweb.custlr.web_service.JsonInput;
import com.bumptech.glide.Glide;
import com.google.android.material.textfield.TextInputEditText;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignUpActivity extends AppCompatActivity {

    private ImageView backgroundImage;
    private TextInputEditText edtUsername, edtEmail, edtContact, edtPassword, edtConfirmPass;
    private CardView btnSignUp;
    private TextView txtAlreadyAccount;
    private Context context;
    private ProgressDialog progressDialog = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        context = SignUpActivity.this;

        backgroundImage = findViewById(R.id.as_backgroundImage);
        edtUsername = findViewById(R.id.al_edtUsername);
        edtEmail = findViewById(R.id.al_edtEmail);
        edtContact = findViewById(R.id.al_edtMobNo);
        edtPassword = findViewById(R.id.al_edtPassword);
        edtConfirmPass = findViewById(R.id.al_edtConfirmPassword);
        btnSignUp = findViewById(R.id.al_loginCardView);
        txtAlreadyAccount = findViewById(R.id.al_alreadySignUp);

        Glide.with(context).load(R.drawable.login).into(backgroundImage);

        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                userSignUp();

            }
        });

        txtAlreadyAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                intentToLoginPage();
            }
        });

    }

    private void intentToLoginPage() {
        Intent intent = new Intent(context, LoginActivity.class);
        startActivityForResult(intent, Constants.LOGIN_CODE);
        MyUtils.openOverrideAnimation(false, SignUpActivity.this);
        finish();
    }


    private void userSignUp() {

        String userName = edtUsername.getText().toString().trim();
        String email = edtEmail.getText().toString().trim();
        String mobileNo = edtContact.getText().toString().trim();
        String password = edtPassword.getText().toString().trim();
        String confirmPassword = edtConfirmPass.getText().toString().trim();

        if (!MyUtils.checkStringValue(userName)) {
            showAlertToUser(Constants.DIALOG_WARNING, "User Name required");
        } else if (!MyUtils.checkStringValue(email)) {
            showAlertToUser(Constants.DIALOG_WARNING, "Email required");
        } else if (!MyUtils.checkStringValue(mobileNo)) {
            showAlertToUser(Constants.DIALOG_WARNING, "Mobile Number required");
        } else if (!MyUtils.checkStringValue(password)) {
            showAlertToUser(Constants.DIALOG_WARNING, "Password required");
        } else if (!MyUtils.checkStringValue(confirmPassword)) {
            showAlertToUser(Constants.DIALOG_WARNING, "Confirm Password required");
        } else if (!password.equals(confirmPassword)) {
            showAlertToUser(Constants.DIALOG_WARNING, "Confirm Password Mismatch");
        } else if (MyUtils.isInternetConnected(context)) {
            registerUser(userName, email, mobileNo, password);
        } else {
            showAlertToUser(Constants.DIALOG_WARNING, getString(R.string.no_internet));
        }
    }

    public void registerUser(String userName, String email, String mobileNo, String password) {


        try {
            progressDialog = MyUtils.showProgressLoader(context, "Registering...");
            ApiInterface apiService = HttpRequest.getScalarsInstance().create(ApiInterface.class);
            Call<String> call = apiService.registerUser(JsonInput.registerJson(userName, email, mobileNo, password, APIS.DEVICE_TYPE));
            call.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    MyUtils.dismissProgressLoader(progressDialog);
                    try {
                        if (response != null) {
                            Gson gson = new GsonBuilder().serializeNulls().create();
                            RegisterResponse registerResponse = gson.fromJson(response.body(), new TypeToken<RegisterResponse>() {
                            }.getType());

                            if (registerResponse != null) {
                                String status = registerResponse.getStatus();
                                if (MyUtils.checkStringValue(status) && status.equals(Constants.SUCCESS)) {
                                    Log.e("status", ": " + status);

                                    MyUtils.ShowLongToast(context, "Successfully Registered..");
                                    finish();


                                } else {
                                    String message = registerResponse.getMessage();

                                    if (MyUtils.checkStringValue(message)) {
                                        MyUtils.ShowLongToast(context, "" + message);
                                    } else {
                                        MyUtils.ShowLongToast(context, "Unable to login,try again later");
                                    }
                                }
                            }
                        }

                    } catch (Exception e) {
                        Log.e("exception33", ":" + e.getMessage());
                    }
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    Log.e("exception11", ":" + t.getMessage());
                    MyUtils.dismissProgressLoader(progressDialog);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            MyUtils.dismissProgressLoader(progressDialog);
        }


    }

    private void showAlertToUser(String statusCode, String statusMessage) {
        CustomDialog customDialog = new CustomDialog(SignUpActivity.this, statusCode, statusMessage, new DialogInterface() {
            @Override
            public void onButtonClicked() {

            }

            @Override
            public void onDismissedClicked() {

            }
        });
        customDialog.showDialog();
    }
}
