package com.bestweb.custlr.activities;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.bestweb.custlr.R;
import com.bestweb.custlr.models.CustomerResponse;
import com.bestweb.custlr.utils.Constants;
import com.bestweb.custlr.utils.MyUtils;
import com.bestweb.custlr.utils.RequestParamUtils;
import com.bestweb.custlr.web_service.ApiInterface;
import com.bestweb.custlr.web_service.HttpRequest;
import com.bestweb.custlr.web_service.JsonInput;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.util.Calendar;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditProfileActivity extends AppCompatActivity {
    private TextView txtHeader, txtDOB;
    private ImageView imgBack;
    private Context context;
    private EditText edtFirstName, edtLastName, edtAddress, edtState, edtCity, edtPostcode, edtEmail, edtPhone;
    private ProgressDialog progressDialog = null;
    //private ArrayList<CustomerResponse.MetaDatum> datumArrayList = new ArrayList<>();
    DatePickerDialog datePickerDialog;
    private boolean allowClose = false;
    private RelativeLayout relativeSave;
    private String userId = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        context = EditProfileActivity.this;
        init();
        listener();

        getUserDetails();

    }

    private void init() {

        userId = MyUtils.getPreferences(context).getString(RequestParamUtils.ID, "");

        imgBack = findViewById(R.id.imgBack);
        txtHeader = findViewById(R.id.txtHeader);
        txtHeader.setText("EDIT PROFILE");


        edtFirstName = findViewById(R.id.aep_edtFirstName);
        edtLastName = findViewById(R.id.aep_edtLastName);
        edtAddress = findViewById(R.id.aep_edtAddress);
        edtCity = findViewById(R.id.aep_edtState);
        edtState = findViewById(R.id.aep_edtCity);
        edtPostcode = findViewById(R.id.aep_edtPostcode);
        edtEmail = findViewById(R.id.aep_edtEmail);
        txtDOB = findViewById(R.id.aep_edtDob);
        edtPhone = findViewById(R.id.aep_edtPhone);


        relativeSave = findViewById(R.id.aep_linearSummaryButton);


    }

    private void listener() {

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                MyUtils.openOverrideAnimation(false, EditProfileActivity.this);
            }
        });

        txtDOB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openCalendarView();
            }
        });

        relativeSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (MyUtils.isInternetConnected(context)) {
                    updateCustomer();
                } else {

                    MyUtils.ShowLongToast(context, "Please Check your Internet");
                }

            }
        });

    }

    private void storeDateOfBirth(String dob) {
        SharedPreferences.Editor pre = MyUtils.getPreferences(context).edit();
        pre.putString(RequestParamUtils.DATE_OF_BIRTH, dob);
        pre.commit();
    }


    private void openCalendarView() {

        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);

        final int minYear = mYear - 8;
        final int minMonth = mMonth;
        final int minDay = mDay;

        if (txtDOB.getText().toString().equals("")) {
            mYear = minYear;
            mMonth = minMonth;
            mDay = minDay;
        } else {
            String selectedDate = txtDOB.getText().toString();
            String[] dateParts = selectedDate.split("/");
            String day = dateParts[0];
            String month = dateParts[1];
            String year = dateParts[2];

            mYear = Integer.parseInt(year);
            mMonth = Integer.parseInt(month) - 1;
            mDay = Integer.parseInt(day);
        }
        DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker view, int selectedYear,
                                  int selectedMonth, int selectedDay) {
            }
        };

        datePickerDialog = new DatePickerDialog(
                EditProfileActivity.this, datePickerListener,
                mYear, mMonth, mDay) {
            @Override
            public void onBackPressed() {
                allowClose = true;
                super.onBackPressed();
            }

            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (which == DialogInterface.BUTTON_POSITIVE) {

                    DatePicker datePicker = datePickerDialog.getDatePicker();
                    if (datePicker.getYear() < minYear || datePicker.getMonth() < minMonth && datePicker.getYear() == minYear || datePicker.getDayOfMonth() <= minDay && datePicker.getYear() == minYear && datePicker.getMonth() == minMonth) {
                        datePicker.updateDate(datePicker.getYear(), datePicker.getMonth(), datePicker.getDayOfMonth());
                        txtDOB.setText(datePicker.getDayOfMonth() + "/" + (datePicker.getMonth() + 1) + "/" + datePicker.getYear());
                        allowClose = true;
                    } else {
                        allowClose = false;
                        Toast.makeText(EditProfileActivity.this, R.string.enter_proper_detail, Toast.LENGTH_SHORT).show();
                        return;
                    }
                } else {
                    if (which == DialogInterface.BUTTON_NEGATIVE) {
                        allowClose = true;
                    }
                }
                super.onClick(dialog, which);
            }

            @Override
            public void dismiss() {
                if (allowClose) {
                    super.dismiss();
                }
            }

        };

        datePickerDialog.setCancelable(false);
        datePickerDialog.show();
    }

    private void getUserDetails() {
        try {
            progressDialog = MyUtils.showProgressLoader(context, "Fetching Details...");
            ApiInterface apiService = HttpRequest.getScalarsInstance().create(ApiInterface.class);
            Call<String> call = apiService.getCustomerDetails(JsonInput.getCustomerDetails(userId));
            call.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    MyUtils.dismissProgressLoader(progressDialog);
                    try {
                        if (response != null) {
                            Gson gson = new GsonBuilder().serializeNulls().create();
                            CustomerResponse customerResponse = gson.fromJson(response.body(), new TypeToken<CustomerResponse>() {
                            }.getType());


                            if (customerResponse != null) {

                                updateUI(customerResponse);

                            }
                        }

                    } catch (Exception e) {
                        Log.e("exception33", ":" + e.getMessage());
                    }
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    Log.e("exception11", ":" + t.getMessage());
                    MyUtils.dismissProgressLoader(progressDialog);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            MyUtils.dismissProgressLoader(progressDialog);
        }


    }


    private void updateUI(CustomerResponse response) {

        String firstName = response.getFirstName();
        String lastName = response.getLastName();
        String address = response.getBilling().getAddress1();
        String state = response.getBilling().getState();
        String city = response.getBilling().getCity();
        String postcode = response.getBilling().getPostcode();
        String phoneNo = response.getBilling().getPhone();
        String email = response.getEmail();

        String profilePic = response.getPgsProfileImage();


        if (MyUtils.checkStringValue(firstName)) {
            edtFirstName.setText(firstName);
        }
        if (MyUtils.checkStringValue(lastName)) {
            edtLastName.setText(lastName);

        }
        if (MyUtils.checkStringValue(address)) {
            edtAddress.setText(address);

        }
        if (MyUtils.checkStringValue(state)) {
            edtState.setText(state);

        }
        if (MyUtils.checkStringValue(city)) {
            edtCity.setText(city);

        }
        if (MyUtils.checkStringValue(postcode)) {
            edtPostcode.setText(postcode);

        }
        if (MyUtils.checkStringValue(email)) {
            edtEmail.setText(email);

        }

        if (MyUtils.checkStringValue(phoneNo)) {
            edtPhone.setText(phoneNo);

        }

        String dateOfBirth = MyUtils.getPreferences(context).getString(RequestParamUtils.DATE_OF_BIRTH, "");
        if (MyUtils.checkStringValue(dateOfBirth)) {
            txtDOB.setText(dateOfBirth);
        }

        /*String mobileNo = "", dob = "";

        for (int i = 0; i < arrayList.size(); i++) {

            String key = arrayList.get(i).getKey();
            if (key.equalsIgnoreCase("mobile")) {
                mobileNo = arrayList.get(i).getValue();
            } else if (key.equalsIgnoreCase("dob")) {
                dob = arrayList.get(i).getValue();
            }
        }*/


        /*if (MyUtils.checkStringValue(dob)) {
            txtDOB.setText(dob);
        }  if (MyUtils.checkStringValue(mobileNo)) {
            edtPhone.setText(mobileNo);
        }*/


    }


    private void updateCustomer() {
        try {
            progressDialog = MyUtils.showProgressLoader(context, "Loading...");
            ApiInterface apiService = HttpRequest.getScalarsInstance().create(ApiInterface.class);
            Call<String> call = apiService.updateCustomer(JsonInput.updateCustomerJSON(edtFirstName.getText().toString(), edtLastName.getText().toString(), edtAddress.getText().toString(), edtState.getText().toString(), edtCity.getText().toString(), edtPostcode.getText().toString(), edtPhone.getText().toString(), txtDOB.getText().toString(), userId));
            call.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    MyUtils.dismissProgressLoader(progressDialog);
                    try {
                        if (response != null) {
                            Gson gson = new GsonBuilder().serializeNulls().create();
                            CustomerResponse customerResponse = gson.fromJson(response.body(), new TypeToken<CustomerResponse>() {
                            }.getType());
                            if (customerResponse != null) {

                                if (customerResponse.getStatus().equalsIgnoreCase(Constants.SUCCESS)) {

                                    storeDateOfBirth(txtDOB.getText().toString());

                                    MyUtils.ShowLongToast(context, "Profile Successfully Updated");
                                } else {
                                    MyUtils.ShowLongToast(context, "Update Failed Try Again ..");
                                }

                            }
                        }

                    } catch (Exception e) {
                        Log.e("exception33", ":" + e.getMessage());
                    }
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    Log.e("exception11", ":" + t.getMessage());
                    MyUtils.dismissProgressLoader(progressDialog);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            MyUtils.dismissProgressLoader(progressDialog);
        }


    }


}
