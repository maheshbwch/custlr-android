package com.bestweb.custlr.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import com.bestweb.custlr.R;
import com.bestweb.custlr.web_service.ApiInterface;
import com.bestweb.custlr.web_service.HttpRequest;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ThankYouActivity extends AppCompatActivity {


    private Context context;
    private ImageView imgBack;
    private Button continueToShoppingButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_thank_you);
        context = ThankYouActivity.this;

        init();
    }


    private void init() {
        imgBack = findViewById(R.id.aty_imgBack);
        continueToShoppingButton = findViewById(R.id.aty_continueToShoppingButton);

        continueToShoppingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                launchHomePage();
            }
        });


        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                launchHomePage();
            }
        });


        logoutUser();

    }


    private void logoutUser() {
        try {
            ApiInterface apiService = HttpRequest.getScalarsInstance().create(ApiInterface.class);
            Call<String> call = apiService.logoutPayment("");
            call.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    try {
                        String jsonResponse = response.body();

                        Log.e("logout_response", ":" + jsonResponse);

                        if (jsonResponse != null && jsonResponse.length() > 0) {
                            try {
                                JSONObject jsonObj = new JSONObject(jsonResponse);
                                String status = jsonObj.getString("status");
                                if (status.equals("success")) {

                                }
                            } catch (Exception e) {
                                Log.e("error", e.getMessage());
                            }
                        }

                    } catch (Exception e) {
                        Log.e("exception33", ":" + e.getMessage());
                    }
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    Log.e("exception11", ":" + t.getMessage());
                }
            });
        } catch (Exception e) {
            Log.e("exception22", ":" + e.getMessage());
        }
    }


    private void launchHomePage() {
        Intent i = new Intent(context, HomeActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(i);
    }


    @Override
    public void onBackPressed() {
        launchHomePage();
    }
}
