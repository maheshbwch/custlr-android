package com.bestweb.custlr.activities;

import android.content.Context;
import android.os.Bundle;
import android.widget.LinearLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.bestweb.custlr.R;
import com.bestweb.custlr.fragments.mycart.CartFragment;

public class MycartActivity extends AppCompatActivity {

    private Context context;
    private LinearLayout linearBack, linearNext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mycart);

        context = MycartActivity.this;
        init();
        listerner();
    }

    private void init() {

        Fragment fragment = null;
        fragment = new CartFragment();
        if (fragment != null) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            //ft.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left);
            ft.replace(R.id.content_frame_myCart, fragment);
            ft.commit();
        }

    }

    private void listerner() {

    }
}
