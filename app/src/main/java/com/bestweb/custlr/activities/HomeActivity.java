package com.bestweb.custlr.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.bestweb.custlr.R;
import com.bestweb.custlr.activities.products.ProductDetailActivity;
import com.bestweb.custlr.activities.products.ProductMainActivity;
import com.bestweb.custlr.adapters.BannerAdapter;
import com.bestweb.custlr.adapters.HomeCategoryAdapter;
import com.bestweb.custlr.interfaces.RecyclerClickListener;
import com.bestweb.custlr.interfaces.SubCategoryClickListener;
import com.bestweb.custlr.interfaces.WishListListener;
import com.bestweb.custlr.models.Categories;
import com.bestweb.custlr.models.Subcategory;
import com.bestweb.custlr.models.WishList;
import com.bestweb.custlr.utils.APIS;
import com.bestweb.custlr.utils.Constants;
import com.bestweb.custlr.utils.MyUtils;
import com.bestweb.custlr.utils.PreferenceHandler;
import com.bestweb.custlr.utils.RequestParamUtils;
import com.bestweb.custlr.utils.custom_view.ShimmerLayout;
import com.bestweb.custlr.utils.custom_view.spark_button.SparkButton;
import com.bestweb.custlr.web_service.ApiInterface;
import com.bestweb.custlr.web_service.HttpRequest;
import com.bestweb.custlr.web_service.JsonInput;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.google.android.material.navigation.NavigationView;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Timer;
import java.util.TimerTask;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeActivity extends AppCompatActivity implements View.OnClickListener {

    private Context context;
    private ImageView searchImg;
    private FrameLayout wishListFrame, cartFrame;
    private ImageView menuIconText;
    private DrawerLayout drawerLayout;
    private ShimmerLayout shimmerLayout;
    private ViewPager bannerViewPager;
    private RecyclerView recyclerView;
    private LinearLayout ll_dots, linearEdtProfile;
    private ProgressDialog progressDialog = null;

    TextView wishListCountTxt, cartCountTxt, txtName;
    private CircleImageView circleImageView;

    private LinearLayout loginHeaderLinear, userLinear, loginLinear, logOutLinear, shopLinear, myOrdersLinear;

    private static int currentPage = 0;
    private static int NUM_PAGES = 0;


    ArrayList<String> arrayListBanner = new ArrayList<>();
    ArrayList<Categories> categoriesArrayList = new ArrayList<>();
    ArrayList<String> wishListIdsArrayList = new ArrayList<>();

    private HomeCategoryAdapter homeCategoryAdapter = null;


    private JSONArray categoryArray = null;
    boolean isImageLoaded = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        context = HomeActivity.this;

        menuIconText = findViewById(R.id.af_menuIconImg);
        searchImg = findViewById(R.id.ah_searchImg);

        wishListFrame = findViewById(R.id.ah_wishListFrame);
        wishListCountTxt = findViewById(R.id.ah_wishListCountTxt); //TODO ENABLE THIS

        cartFrame = findViewById(R.id.ah_cartFrame);
        cartCountTxt = findViewById(R.id.ah_cartCountTxt);

        drawerLayout = findViewById(R.id.ah_drawerLayout);
        shimmerLayout = findViewById(R.id.ah_shimmer_layout);
        bannerViewPager = findViewById(R.id.ah_bannerViewPager);
        recyclerView = findViewById(R.id.ad_homeRecylerView);

        ll_dots = findViewById(R.id.ah_dots);

        NavigationView navigationView = findViewById(R.id.ah_navigationView);
        View headerView = navigationView.getHeaderView(0);
        loginHeaderLinear = headerView.findViewById(R.id.ahn_loginHeaderLinear);
        userLinear = headerView.findViewById(R.id.ahn_userLinear);
        loginLinear = headerView.findViewById(R.id.ahn_loginLinear);
        logOutLinear = headerView.findViewById(R.id.ahn_logOutLinear);
        shopLinear = headerView.findViewById(R.id.ahn_shopLinear);
        myOrdersLinear = headerView.findViewById(R.id.ahn_myOrdersLinear);

        circleImageView = headerView.findViewById(R.id.nh_img_Profile);
        txtName = headerView.findViewById(R.id.nh_txtName);
        linearEdtProfile = headerView.findViewById(R.id.nh_linearEdtProfile);

        loginHeaderLinear.setOnClickListener(this);
        loginLinear.setOnClickListener(this);
        shopLinear.setOnClickListener(this);
        myOrdersLinear.setOnClickListener(this);
        logOutLinear.setOnClickListener(this);

        circleImageView.setOnClickListener(this);
        txtName.setOnClickListener(this);
        linearEdtProfile.setOnClickListener(this);

        listeners();
        getHomeData();


    }

    private void listeners() {

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(linearLayoutManager);

        menuIconText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openCloseDrawer();
            }
        });

        searchImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, SearchActivity.class);
                startActivityForResult(intent, Constants.PRODUCTS);
                MyUtils.openOverrideAnimation(true, HomeActivity.this);
            }
        });

        wishListFrame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, WishlistActivity.class);
                startActivityForResult(intent, Constants.PRODUCTS);
                MyUtils.openOverrideAnimation(true, HomeActivity.this);
            }
        });

        cartFrame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, CartActivity.class);
                startActivityForResult(intent, Constants.PRODUCTS);
                MyUtils.openOverrideAnimation(true, HomeActivity.this);
            }
        });

        loadProfilePicture();


    }

    private void loadImageSource(String imageFilePath) {
        Glide.with(HomeActivity.this).load(imageFilePath).placeholder(R.drawable.profile_picture_big).listener(new RequestListener<Drawable>() {
            @Override
            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                isImageLoaded = false;
                return false;
            }

            @Override
            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                isImageLoaded = true;
                return false;
            }
        }).into(circleImageView);
    }


    private void loadProfilePicture() {

        String socialImgUrl = MyUtils.getPreferences(context).getString(RequestParamUtils.SOCIAL_IMG_URL, "");
        String socialImgUrlFb = MyUtils.getPreferences(context).getString(RequestParamUtils.SOCIAL_IMG_URL_FB, "");
        String manualLoginUrl = MyUtils.getPreferences(context).getString(RequestParamUtils.PROFILE_PIC, "");
        if (MyUtils.checkStringValue(socialImgUrl)) {
            loadImageSource(socialImgUrl);
        } else if (MyUtils.checkStringValue(socialImgUrlFb)) {
            loadImageSource(socialImgUrlFb);
        } else if (MyUtils.checkStringValue(manualLoginUrl)) {
            loadImageSource(manualLoginUrl);
        }

        String displayName = MyUtils.getPreferences(context).getString(RequestParamUtils.DISPLAY_NAME, "");
        txtName.setText(displayName);

    }

    /*private void setupBadge() {
        if (textCartItemCount != null) {
            if (mCartItemCount == 0) {
                if (textCartItemCount.getVisibility() != View.GONE) {
                    textCartItemCount.setVisibility(View.GONE);
                }
            } else {
                textCartItemCount.setText(String.valueOf(Math.min(mCartItemCount, 99)));
                if (textCartItemCount.getVisibility() != View.VISIBLE) {
                    textCartItemCount.setVisibility(View.VISIBLE);
                }
            }
        }
    }*/

    private void addBottomDots(int currentPage) {

        int bannerSize = arrayListBanner.size();

        if (bannerSize > 0) {
            TextView[] dots = new TextView[bannerSize];

            ll_dots.removeAllViews();
            for (int i = 0; i < dots.length; i++) {
                dots[i] = new TextView(this);
                dots[i].setText(Html.fromHtml("&#8226;"));
                dots[i].setTextSize(40);
                dots[i].setTextColor(Color.parseColor("#D4D8DB"));
                ll_dots.addView(dots[i]);
            }

            if (dots.length > 0)
                dots[currentPage].setTextColor(Color.parseColor("#FFFFFF"));
            dots[currentPage].setTextSize(45);
        }

    }


    public void openCloseDrawer() {
        if (drawerLayout != null) {
            if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
                drawerLayout.closeDrawer(GravityCompat.START);
            } else {
                drawerLayout.openDrawer(GravityCompat.START);
            }
        }

    }

    private void closeDrawer() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        }
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            finish();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ahn_loginHeaderLinear:
                closeDrawer();
                intentToLoginPage();
                break;
            case R.id.ahn_loginLinear:
                closeDrawer();
                intentToLoginPage();
                break;
            case R.id.ahn_shopLinear:
                closeDrawer();
                intentToProductPage();
                break;
            case R.id.ahn_myOrdersLinear:
                closeDrawer();
                startActivity(new Intent(context, MyOrderActivity.class));
                MyUtils.openOverrideAnimation(true, this);
                break;
            case R.id.ahn_logOutLinear:
                PreferenceHandler.ClearPreferences(context);
                SharedPreferences preferences = getSharedPreferences(Constants.MyPREFERENCES, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = preferences.edit();
                editor.clear();
                editor.commit();
                closeDrawer();
                intentToLoginPage();
                break;
            case R.id.nh_img_Profile:
                closeDrawer();
                Intent intent = new Intent(context, EditProfileView.class);
                startActivityForResult(intent, Constants.LOGIN_CODE);
                MyUtils.openOverrideAnimation(true, HomeActivity.this);

                break;
            case R.id.nh_txtName:
                closeDrawer();
                Intent intenti = new Intent(context, EditProfileView.class);
                startActivityForResult(intenti, Constants.LOGIN_CODE);
                MyUtils.openOverrideAnimation(true, HomeActivity.this);
                break;
            case R.id.nh_linearEdtProfile:
                closeDrawer();
                startActivity(new Intent(context, EditProfileActivity.class));
                MyUtils.openOverrideAnimation(true, this);
                break;
        }
    }

    private void intentToLoginPage() {
        Intent intent = new Intent(context, LoginActivity.class);
        startActivityForResult(intent, Constants.LOGIN_CODE);
        MyUtils.openOverrideAnimation(false, HomeActivity.this);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constants.LOGIN_CODE) {
            if (resultCode == Constants.LOGIN_SUCCESS) {

                String displayName = MyUtils.getPreferences(context).getString(RequestParamUtils.DISPLAY_NAME, "");
                txtName.setText(displayName);

                userLoggedIn(true);

                loadProfilePicture();

                showWishListEnabledProducts();

            } else if (resultCode == Constants.LOGIN_FAILED) {
                if (!MyUtils.checkIfUserLoggedIn(context)) {
                    userLoggedIn(false);
                }
            }
        } else if (requestCode == Constants.PRODUCTS) {

            Log.e("show", "WishListEnabledProducts");

            showWishListEnabledProducts();

        }
    }

    private void userLoggedIn(boolean state) {
        if (state) {
            loginHeaderLinear.setVisibility(View.GONE);
            loginLinear.setVisibility(View.GONE);
            userLinear.setVisibility(View.VISIBLE);
            logOutLinear.setVisibility(View.VISIBLE);
        } else {
            loginHeaderLinear.setVisibility(View.VISIBLE);
            loginLinear.setVisibility(View.VISIBLE);
            userLinear.setVisibility(View.GONE);
            logOutLinear.setVisibility(View.GONE);
        }
    }

    private void getHomeData() {
        try {
            showShimmerLayout();
            //progressBar.setVisibility(View.VISIBLE);
            ApiInterface apiService = HttpRequest.getScalarsInstance().create(ApiInterface.class);
            Call<String> call = apiService.getCategoriesHome(JsonInput.getHomeJsonObject(getUserId(), APIS.version));
            call.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    stopShimmerAnimation();
                    //progressBar.setVisibility(View.GONE);
                    try {
                        if (response != null) {
                            Log.e("code",": "+response.code());
                            Log.e("message",": "+response.message());

                            updateSliderUI(response.body());
                        }

                    } catch (Exception e) {
                        Log.e("exception33", ":" + e.getMessage());
                    }
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    Log.e("exception11", ":" + t.getMessage());
                    //progressBar.setVisibility(View.GONE);
                    stopShimmerAnimation();
                }
            });
        } catch (Exception e) {
            stopShimmerAnimation();
            e.printStackTrace();
            //progressBar.setVisibility(View.GONE);
        }
    }


    private void showShimmerLayout() {
        shimmerLayout.setVisibility(View.VISIBLE);
        shimmerLayout.startShimmerAnimation();
    }

    private void stopShimmerAnimation() {
        if (shimmerLayout.getVisibility() == View.VISIBLE) {
            shimmerLayout.setVisibility(View.GONE);
        }
        if (shimmerLayout.isAnimationStarted()) {
            shimmerLayout.stopShimmerAnimation();
        }
    }


    private void updateSliderUI(String response) {

        try {
            JSONObject jsonObject = new JSONObject(response);
            if (jsonObject.has("main_slider")) {
                JSONArray jsonArray = jsonObject.optJSONArray("main_slider");
                for (int i = 0; i < jsonArray.length(); i++) {

                    JSONObject jsonObject1 = jsonArray.optJSONObject(i);
                    String url = jsonObject1.optString("upload_image_url");

                    arrayListBanner.add(url);
                }
                addBottomDots(0);

                loadImageBanners();
            }

            /*if (jsonObject.has("Wishlist")) {
                JSONArray wishListArray = jsonObject.optJSONArray("Wishlist");

                if (wishListArray != null && wishListArray.length() > 0) {
                    getWishListProductsIds(wishListArray);
                }
            }*/

            //main_category
            if (jsonObject.has("main_category")) {
                categoryArray = jsonObject.optJSONArray("main_category");
                Log.e("categoryArray", "length:" + categoryArray.length());
            }


            if (jsonObject.has("products_carousel")) {
                JSONObject productsObject = jsonObject.optJSONObject("products_carousel");
                if (productsObject != null) {
                    updateProductUI(productsObject);
                }
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void loadImageBanners() {

        try {
            BannerAdapter bannerAdapter = new BannerAdapter(context, arrayListBanner);
            bannerViewPager.setAdapter(bannerAdapter);
            final Handler handler = new Handler();
            final Runnable Update = new Runnable() {
                public void run() {
                    if (currentPage == NUM_PAGES) {
                        currentPage = 0;
                    }
                    bannerViewPager.setCurrentItem(currentPage++, true);
                }
            };
            Timer swipeTimer = new Timer();
            swipeTimer.schedule(new TimerTask() {
                @Override
                public void run() {
                    handler.post(Update);
                }
            }, 3000, 3000);

            bannerViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                }

                @Override
                public void onPageSelected(int position) {
                    addBottomDots(position);
                }

                @Override
                public void onPageScrollStateChanged(int state) {

                }
            });


        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    /*private void getWishListProductsIds(JSONArray wishListArray) {
        Type listType = new TypeToken<ArrayList<String>>() {
        }.getType();
        Gson gson = new GsonBuilder().serializeNulls().create();
        wishListArrayList = gson.fromJson(wishListArray.toString(), listType);
    }*/


    private void updateProductUI(JSONObject productsObject) {
        try {
            Iterator<String> keys = productsObject.keys();
            while (keys.hasNext()) {
                String key = keys.next();
                if (productsObject.has(key)) {
                    JSONObject subProductObject = productsObject.getJSONObject(key);
                    String title = subProductObject.optString("title");

                    ArrayList<Subcategory> subCategoryList = new ArrayList<>();
                    JSONArray subProductArray = subProductObject.optJSONArray("products");
                    for (int i = 0; i < subProductArray.length(); i++) {
                        JSONObject prdObject = subProductArray.optJSONObject(i);
                        String productId = prdObject.optString("id");
                        String productName = prdObject.optString("name");
                        String regularPrice = prdObject.optString("regular_price");
                        String salePrice = prdObject.optString("sale_price");
                        String prdUrl = prdObject.optString("app_thumbnail");

                        subCategoryList.add(new Subcategory(productId, prdUrl, productName, regularPrice, salePrice));
                    }

                    Categories categories = new Categories(title, subCategoryList);
                    categoriesArrayList.add(categories);

                }
            }

            loadProducts(categoriesArrayList);

        } catch (JSONException e) {
            e.printStackTrace();
        }


    }


    private void loadProducts(ArrayList<Categories> productList) {
        try {
            if (productList.size() > 0) {

                RecyclerClickListener productClickListener = new RecyclerClickListener() {
                    @Override
                    public void onItemClicked(int position) {

                        intentToProductPage();

                    }
                };


                WishListListener wishListClickListener = new WishListListener() {
                    @Override
                    public void onWishListListen(SparkButton wishListButton, String productId) {

                        String userId = MyUtils.getPreferences(context).getString(RequestParamUtils.ID, "");

                        if (MyUtils.checkIfUserLoggedIn(context) && MyUtils.checkStringValue(userId)) {

                            addProductToWishList(wishListButton.isChecked(), wishListButton, productId, userId);

                        } else {
                            disableWishListButton(wishListButton);

                            intentToLoginPage();
                        }


                        if (wishListButton.isChecked()) {
                            Log.e("wishlist", "addProduct:" + productId);
                        } else {
                            Log.e("wishlist", "removeProduct:" + productId);
                        }

                    }
                };


                SubCategoryClickListener subCategoryClickListener = new SubCategoryClickListener() {
                    @Override
                    public void onProductClicked(String productId, String productName, String productSalePrice, String productRegularPrice) {

                        Bundle bundle = new Bundle();
                        bundle.putString(Constants.PRODUCT_ID, productId);
                        bundle.putString(Constants.PRODUCT_NAME, productName);
                        bundle.putString(Constants.PRODUCT_REGULAR_PRICE, productRegularPrice);
                        bundle.putString(Constants.PRODUCT_SALE_PRICE, productSalePrice);

                        Intent intent = new Intent(context, ProductDetailActivity.class);
                        intent.putExtras(bundle);
                        startActivityForResult(intent, Constants.PRODUCTS);

                        MyUtils.openOverrideAnimation(true, (Activity) context);

                    }
                };


                homeCategoryAdapter = new HomeCategoryAdapter(context, productList, productClickListener, wishListClickListener, subCategoryClickListener);
                recyclerView.setAdapter(homeCategoryAdapter);

                showWishListEnabledProducts();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void showWishListEnabledProducts() {
        String userId = MyUtils.getPreferences(context).getString(RequestParamUtils.ID, "");
        if (MyUtils.checkIfUserLoggedIn(context) && MyUtils.checkStringValue(userId)) {

            getWishListIds(userId);

            getCartAndWishListCount(userId);

        } else {
            setUpBadgeCount(0, 0);
        }
    }


    private void getWishListIds(String userId) {
        try {
            wishListIdsArrayList.clear();
            ApiInterface apiService = HttpRequest.getScalarsInstance().create(ApiInterface.class);
            Call<String> call = apiService.getWishListIds(JsonInput.getWishListIds(userId));
            call.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    try {
                        String jsonResponse = response.body();

                        Log.e("ids_response", ":" + jsonResponse);

                        if (jsonResponse != null) {

                            JSONObject jsonObject = new JSONObject(jsonResponse);

                            String status = jsonObject.optString("status");

                            if (MyUtils.checkStringValue(status) && status.equalsIgnoreCase("success")) {

                                if (jsonObject.has("sync_list")) {
                                    JSONArray jsonArray = jsonObject.optJSONArray("sync_list");

                                    if (jsonArray != null && jsonArray.length() > 0) {

                                        for (int i = 0; i < jsonArray.length(); i++) {
                                            String prod_id = jsonArray.optJSONObject(i).optString("prod_id");
                                            wishListIdsArrayList.add(prod_id);
                                        }

                                        checkIfProductsInWishList();
                                    } else {
                                        checkIfProductsInWishList();
                                    }
                                }
                            }
                        }

                    } catch (Exception e) {
                        Log.e("exception33", ":" + e.getMessage());

                        checkIfProductsInWishList();

                    }
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    Log.e("exception11", ":" + t.getMessage());
                }
            });
        } catch (Exception e) {
            Log.e("exception22", ":" + e.getMessage());
        }
    }

    private void checkIfProductsInWishList() {

        if (categoriesArrayList.size() > 0) {
            for (int i = 0; i < categoriesArrayList.size(); i++) {

                ArrayList<Subcategory> subCatList = categoriesArrayList.get(i).getSubCategoryList();

                for (int k = 0; k < subCatList.size(); k++) {

                    String productId = subCatList.get(k).getProductId();

                    if (wishListIdsArrayList.size() > 0 && wishListIdsArrayList.contains(productId)) {

                        subCatList.get(k).setWishList(true);

                        Log.e("wishlist", "true" + " " + i + " " + "-" + " " + k + " ");

                    } else {
                        subCatList.get(k).setWishList(false);
                    }
                }
            }

            if (homeCategoryAdapter != null) {
                homeCategoryAdapter.notifyDataSetChanged();
            }
        }
    }


    private void addProductToWishList(final boolean state, final SparkButton wishListButton, final String productId, final String userId) {
        try {
            progressDialog = MyUtils.showProgressLoader(context, state ? "Adding to WishList" : "Removing from WishList");
            Call<String> call = getWishListCall(state, productId, userId);
            call.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    MyUtils.dismissProgressLoader(progressDialog);
                    try {
                        if (response != null) {
                            Log.e("wishlist", "resp:" + response.body());


                            Type type = new TypeToken<WishList>() {
                            }.getType();
                            Gson gson = new GsonBuilder().serializeNulls().create();
                            WishList wishList = gson.fromJson(response.body(), type);

                            if (wishList != null) {

                                String status = wishList.getStatus();

                                if (MyUtils.checkStringValue(status)) {
                                    if (status.equalsIgnoreCase(Constants.SUCCESS)) {

                                        String message = wishList.getMessage();

                                        MyUtils.ShowLongToast(context, "" + message);

                                        if (state) {
                                            addWishListForRelatedProducts(productId);
                                        } else {
                                            removeWishListFromRelatedProducts(productId);
                                        }

                                        getCartAndWishListCount(userId);


                                    } else if (status.equalsIgnoreCase(Constants.ERROR)) {

                                        String error = wishList.getMessage();

                                        if (MyUtils.checkStringValue(error)) {

                                            MyUtils.ShowLongToast(context, "" + error);

                                            if (!error.equalsIgnoreCase("exists")) {
                                                disableWishListButton(wishListButton);
                                            }
                                        } else {
                                            disableWishListButton(wishListButton);
                                        }
                                    }
                                } else {
                                    disableWishListButton(wishListButton);
                                }
                            }
                        }
                    } catch (Exception e) {
                        Log.e("exception33", ":" + e.getMessage());
                        disableWishListButton(wishListButton);
                    }
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    Log.e("exception11", ":" + t.getMessage());
                    MyUtils.dismissProgressLoader(progressDialog);
                    disableWishListButton(wishListButton);
                }
            });
        } catch (Exception e) {
            MyUtils.dismissProgressLoader(progressDialog);
            disableWishListButton(wishListButton);
        }
    }

    private void addWishListForRelatedProducts(String productId) {
        if (/*wishListIdsArrayList.size() > 0 &&*/ !wishListIdsArrayList.contains(productId)) {
            wishListIdsArrayList.add(productId);
            checkIfProductsInWishList();
        }
    }

    private void removeWishListFromRelatedProducts(String productId) {
        if (/*wishListIdsArrayList.size() > 0 &&*/ wishListIdsArrayList.contains(productId)) {
            wishListIdsArrayList.remove(productId);
            checkIfProductsInWishList();
        }
    }


    private Call getWishListCall(boolean status, String productId, String userId) {
        ApiInterface apiService = HttpRequest.getScalarsInstance().create(ApiInterface.class);
        if (status) {
            return apiService.addToWishList(JsonInput.wishListInput(userId, productId));
        } else {
            return apiService.removeFromWishList(JsonInput.wishListInput(userId, productId));
        }
    }

    private void disableWishListButton(SparkButton wishListButton) {
        if (wishListButton.isChecked()) {
            wishListButton.setChecked(false);
        }
    }

    private void intentToProductPage() {
        if (categoryArray != null) {
            Bundle bundle = new Bundle();
            bundle.putString(Constants.CATEGORIES_JSON, categoryArray.toString());

            Intent intent = new Intent(context, ProductMainActivity.class);
            intent.putExtras(bundle);
            startActivityForResult(intent, Constants.PRODUCTS);

            MyUtils.openOverrideAnimation(true, HomeActivity.this); //TODO CHECK
        }
    }


    @Override
    protected void onResume() {
        super.onResume();

        String userId = MyUtils.getPreferences(context).getString(RequestParamUtils.ID, "");

        if (MyUtils.checkIfUserLoggedIn(context) && MyUtils.checkStringValue(userId)) {

            userLoggedIn(true);

        } else {

            userLoggedIn(false);
        }

        String uploadUrl = PreferenceHandler.getPreferenceFromString(context, Constants.UPLOAD_URL);
        if (MyUtils.checkStringValue(uploadUrl)) {
            loadImageSource(uploadUrl);
        }

    }


    private void getCartAndWishListCount(String userId) {
        try {
            ApiInterface apiService = HttpRequest.getScalarsInstanceWC().create(ApiInterface.class);
            Call<String> call = apiService.getCartAndWishListCount(userId);
            call.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    try {
                        String jsonResponse = response.body();

                        Log.e("count_response", ":" + jsonResponse);

                        if (jsonResponse != null) {

                            int cartCount = 0;
                            int wishListCount = 0;

                            JSONObject jsonObject = new JSONObject(jsonResponse);

                            cartCount = jsonObject.optJSONObject("cart").optInt("count");
                            wishListCount = jsonObject.optJSONObject("wishlist").optInt("count");

                            setUpBadgeCount(wishListCount, cartCount);
                        }

                    } catch (Exception e) {
                        Log.e("exception33", ":" + e.getMessage());
                    }
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    Log.e("exception11", ":" + t.getMessage());
                }
            });
        } catch (Exception e) {
            Log.e("exception22", ":" + e.getMessage());
        }
    }

    private void setUpBadgeCount(int wishListCount, int cartCount) {

        Log.e("wishListCount", ":" + wishListCount);
        Log.e("cartCount", ":" + cartCount);

        if (wishListCount > 99) {
            wishListCountTxt.setText("99+");
        } else {
            wishListCountTxt.setText("" + wishListCount);
        }

        if (cartCount > 99) {
            cartCountTxt.setText("99+");
        } else {
            cartCountTxt.setText("" + cartCount);
        }
    }

    private String getUserId() {
        String userId = MyUtils.getPreferences(context).getString(RequestParamUtils.ID, "");
        if (MyUtils.checkIfUserLoggedIn(context) && MyUtils.checkStringValue(userId)) {
            return userId;
        } else {
            return null;
        }
    }


}
