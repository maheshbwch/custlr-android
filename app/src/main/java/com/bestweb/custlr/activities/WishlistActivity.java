package com.bestweb.custlr.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bestweb.custlr.R;
import com.bestweb.custlr.activities.products.ProductDetailActivity;
import com.bestweb.custlr.adapters.WishListAdapter;
import com.bestweb.custlr.interfaces.WishListClickListener;
import com.bestweb.custlr.models.Products;
import com.bestweb.custlr.models.WishList;
import com.bestweb.custlr.utils.Constants;
import com.bestweb.custlr.utils.MyUtils;
import com.bestweb.custlr.utils.RequestParamUtils;
import com.bestweb.custlr.web_service.ApiInterface;
import com.bestweb.custlr.web_service.HttpRequest;
import com.bestweb.custlr.web_service.JsonInput;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WishlistActivity extends AppCompatActivity {

    private Context context;
    private TextView titleTxt;
    private ImageView imgBack;
    private RecyclerView recyclerView;
    private LinearLayoutManager linearLayoutManager;
    private ProgressBar progressBar;
    private RelativeLayout emptyViewRelative;
    private TextView emptyTxtTitle, emptyTxtContent;
    private ProgressDialog progressDialog = null;

    private ArrayList<Products> productsArrayList = new ArrayList<>();
    private WishListAdapter wishListAdapter = null;

    private String userId = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wishlist);
        context = WishlistActivity.this;

        init();
    }

    private void init() {

        titleTxt = findViewById(R.id.aw_titleNameTxt);
        imgBack = findViewById(R.id.aw_imgBack);
        recyclerView = findViewById(R.id.aw_recycler);
        progressBar = findViewById(R.id.aw_progressBar);

        emptyViewRelative = findViewById(R.id.aw_emptyViewRelative);
        emptyTxtTitle = findViewById(R.id.aw_emptyTxtTitle);
        emptyTxtContent = findViewById(R.id.aw_emptyTxtContent);

        titleTxt.setText("MY WISHLIST");

        linearLayoutManager = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);

        checkIfUserLoggedIn();

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                closePage();
            }
        });


    }

    private void checkIfUserLoggedIn() {
        userId = MyUtils.getPreferences(context).getString(RequestParamUtils.ID, "");

        if (MyUtils.checkIfUserLoggedIn(context) && MyUtils.checkStringValue(userId)) {

            getWishListIds(userId);

        } else {

            intentToLoginPage();
        }
    }

    private void getWishListIds(String userId) {
        try {
            progressBar.setVisibility(View.VISIBLE);
            ApiInterface apiService = HttpRequest.getScalarsInstance().create(ApiInterface.class);
            Call<String> call = apiService.getWishListIds(JsonInput.getWishListIds(userId));
            call.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    try {
                        hideProgressBar();

                        String jsonResponse = response.body();

                        Log.e("ids_response", ":" + jsonResponse);

                        if (jsonResponse != null) {

                            JSONObject jsonObject = new JSONObject(jsonResponse);

                            String status = jsonObject.optString("status");

                            if (MyUtils.checkStringValue(status) && status.equalsIgnoreCase("success")) {

                                if (jsonObject.has("sync_list")) {
                                    JSONArray jsonArray = jsonObject.optJSONArray("sync_list");

                                    if (jsonArray != null && jsonArray.length() > 0) {

                                        StringBuilder productIdsBuilder = new StringBuilder("");
                                        for (int i = 0; i < jsonArray.length(); i++) {
                                            String prod_id = jsonArray.optJSONObject(i).optString("prod_id");

                                            productIdsBuilder.append(prod_id + ",");
                                        }

                                        Log.e("stringBuilder", ":" + productIdsBuilder);


                                        getProductsList("" + productIdsBuilder);

                                    } else {
                                        showErrorCase();
                                    }
                                } else {
                                    showErrorCase();
                                }
                            } else {
                                showErrorCase();
                            }
                        } else {
                            showErrorCase();
                        }

                    } catch (Exception e) {
                        showErrorCase();
                        Log.e("exception33", ":" + e.getMessage());
                    }
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    hideProgressBar();
                    showErrorCase();
                    Log.e("exception11", ":" + t.getMessage());
                }
            });
        } catch (Exception e) {
            hideProgressBar();
            showErrorCase();
            Log.e("exception22", ":" + e.getMessage());
        }
    }


    private void getProductsList(String productIds) {
        try {
            progressBar.setVisibility(View.VISIBLE);
            ApiInterface apiService = HttpRequest.getScalarsInstance().create(ApiInterface.class);
            Call<String> call = apiService.getProductsDetails(JsonInput.getProductDetails(productIds));
            call.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    progressBar.setVisibility(View.GONE);
                    try {
                        String jsonResponse = response.body();
                        if (jsonResponse != null) {
                            updateUI(jsonResponse);
                        } else {
                            showErrorCase();
                        }

                    } catch (Exception e) {
                        showErrorCase();
                        Log.e("exception33", ":" + e.getMessage());
                    }
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    Log.e("exception11", ":" + t.getMessage());
                    hideProgressBar();
                    showErrorCase();
                }
            });
        } catch (Exception e) {
            hideProgressBar();
            showErrorCase();
        }
    }


    private void updateUI(String jsonResponse) {
        Log.e("prd_detail", ":" + jsonResponse);

        productsArrayList.clear();
        Type listType = new TypeToken<ArrayList<Products>>() {
        }.getType();
        Gson gson = new GsonBuilder().serializeNulls().create();
        productsArrayList = gson.fromJson(jsonResponse, listType);

        if (productsArrayList.size() > 0) {


            WishListClickListener wishListClickListener = new WishListClickListener() {
                @Override
                public void onDeleteWishList(int position, String productId) {

                    deleteWishList(userId, productId, position);

                }

                @Override
                public void onWishListItemClicked(int position) {
                    String productId = "" + productsArrayList.get(position).getId();
                    String productName = productsArrayList.get(position).getName();
                    String productSalePrice = "" + productsArrayList.get(position).getPrice();

                    if (MyUtils.checkStringValue(productId)) {
                        Bundle bundle = new Bundle();
                        bundle.putString(Constants.PRODUCT_ID, productId);
                        bundle.putString(Constants.PRODUCT_NAME, productName);
                        bundle.putString(Constants.PRODUCT_REGULAR_PRICE, "");
                        bundle.putString(Constants.PRODUCT_SALE_PRICE, productSalePrice);

                        Intent intent = new Intent(context, ProductDetailActivity.class);
                        intent.putExtras(bundle);
                        startActivityForResult(intent, Constants.PRODUCTS);

                        MyUtils.openOverrideAnimation(true, (Activity) context);
                    }
                }
            };

            wishListAdapter = new WishListAdapter(context, productsArrayList, wishListClickListener);
            recyclerView.setAdapter(wishListAdapter);


        } else {
            showErrorCase();
        }
    }

    private void deleteWishList(final String userId, String productId, final int position) {
        try {
            progressDialog = MyUtils.showProgressLoader(context, "Removing Product..");
            ApiInterface apiService = HttpRequest.getScalarsInstance().create(ApiInterface.class);
            Call<String> call = apiService.removeFromWishList(JsonInput.wishListInput(userId, productId));
            call.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    MyUtils.dismissProgressLoader(progressDialog);
                    try {
                        String jsonResponse = response.body();
                        if (jsonResponse != null) {
                            Log.e("wishlist", "resp:" + response.body());

                            Type type = new TypeToken<WishList>() {
                            }.getType();
                            Gson gson = new GsonBuilder().serializeNulls().create();
                            WishList wishList = gson.fromJson(jsonResponse, type);

                            if (wishList != null) {

                                String status = wishList.getStatus();

                                if (MyUtils.checkStringValue(status)) {
                                    if (status.equalsIgnoreCase(Constants.SUCCESS)) {

                                        String message = wishList.getMessage();

                                        MyUtils.ShowLongToast(context, "" + message);

                                        if (wishListAdapter != null) {
                                            productsArrayList.remove(position);
                                            wishListAdapter.notifyItemRemoved(position);

                                            if (productsArrayList.size() == 0) {
                                                showErrorCase();
                                            }
                                        }


                                    } else if (status.equalsIgnoreCase(Constants.ERROR)) {

                                        String error = wishList.getMessage();

                                        MyUtils.ShowLongToast(context, "" + error);
                                    }
                                }
                            }
                        }

                    } catch (Exception e) {
                        Log.e("exception33", ":" + e.getMessage());
                    }
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    Log.e("exception11", ":" + t.getMessage());
                    MyUtils.dismissProgressLoader(progressDialog);
                }
            });
        } catch (Exception e) {
            Log.e("exception22", ":" + e.getMessage());
            MyUtils.dismissProgressLoader(progressDialog);
        }
    }


    private void showErrorCase() {

        if (recyclerView.getVisibility() == View.VISIBLE) {
            recyclerView.setVisibility(View.GONE);
        }

        if (emptyViewRelative.getVisibility() == View.GONE) {
            emptyViewRelative.setVisibility(View.VISIBLE);

            emptyTxtTitle.setText("Empty WishList");
            emptyTxtContent.setText("There is no item in wishlist");
        }
    }


    private void hideProgressBar() {
        if (progressBar.getVisibility() == View.VISIBLE) {
            progressBar.setVisibility(View.GONE);
        }
    }


    private void intentToLoginPage() {
        Intent intent = new Intent(context, LoginActivity.class);
        startActivityForResult(intent, Constants.LOGIN_CODE);
        MyUtils.openOverrideAnimation(false, WishlistActivity.this);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constants.LOGIN_CODE) {
            if (resultCode == Constants.LOGIN_SUCCESS) {

                checkIfUserLoggedIn();

            } else if (resultCode == Constants.LOGIN_FAILED) {
                if (!MyUtils.checkIfUserLoggedIn(context)) {
                    finish();
                }
            }
        } else if (requestCode == Constants.PRODUCTS) {

            Log.e("show", "WishListEnabledProducts");

            checkIfUserLoggedIn();

        }
    }


    @Override
    public void onBackPressed() {
        closePage();
    }


    private void closePage() {
        setResult(Constants.PRODUCTS);
        finish();
        MyUtils.openOverrideAnimation(false, WishlistActivity.this);
    }


}
