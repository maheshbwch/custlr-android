package com.bestweb.custlr.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bestweb.custlr.R;
import com.bestweb.custlr.activities.products.ProductDetailActivity;
import com.bestweb.custlr.adapters.SearchHomeAdapter;
import com.bestweb.custlr.interfaces.RecyclerClickListener;
import com.bestweb.custlr.models.Search;
import com.bestweb.custlr.utils.Constants;
import com.bestweb.custlr.utils.MyUtils;
import com.bestweb.custlr.web_service.ApiInterface;
import com.bestweb.custlr.web_service.HttpRequest;
import com.bestweb.custlr.web_service.JsonInput;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchActivity extends AppCompatActivity {

    private Context context;
    private RecyclerView recyclerView;
    private ProgressBar progressBar;
    private ImageView searchImage, closeImage;
    private EditText searchEdt;

    private RelativeLayout emptyViewRelative;
    private TextView emptyText;

    private ArrayList<Search> searchArrayList = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter);
        context = SearchActivity.this;

        init();
    }

    private void init() {

        searchImage = findViewById(R.id.fa_searchImage);
        searchEdt = findViewById(R.id.fa_searchEdt);
        closeImage = findViewById(R.id.af_closeImage);

        recyclerView = findViewById(R.id.af_recycler);
        progressBar = findViewById(R.id.af_progressBar);

        progressBar = findViewById(R.id.af_progressBar);
        emptyViewRelative = findViewById(R.id.af_emptyViewRelative);
        emptyText = findViewById(R.id.af_emptyText);


        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));

        searchEdt.setFocusable(true);
        searchEdt.requestFocus();

        searchImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String searchHint = searchEdt.getText().toString();

                if (searchHint.length() > 0) {

                    getSearchResults(searchHint);

                    MyUtils.hideKeyboard(SearchActivity.this);

                } else {
                    MyUtils.ShowLongToast(context, "Enter the text to search");
                }
            }
        });

        closeImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                closePage();
            }
        });

    }


    private void getSearchResults(final String searchHint) {
        try {
            progressBar.setVisibility(View.VISIBLE);
            ApiInterface apiService = HttpRequest.getScalarsInstance().create(ApiInterface.class);
            Call<String> call = apiService.getSearchResult(JsonInput.searchInput(searchHint, false));
            call.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    progressBar.setVisibility(View.GONE);
                    try {
                        if (response != null) {
                            updateUI(response.body(), searchHint);
                        }
                    } catch (Exception e) {
                        Log.e("exception33", ":" + e.getMessage());
                        showErrorCase(searchHint);
                    }
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    Log.e("exception11", ":" + t.getMessage());
                    progressBar.setVisibility(View.GONE);
                    showErrorCase(searchHint);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            progressBar.setVisibility(View.GONE);
            showErrorCase(searchHint);
        }
    }

    private void updateUI(String jsonResponse, String searchHint) {

        Log.e("prd_detail", ":" + jsonResponse);

        searchArrayList.clear();
        Type listType = new TypeToken<ArrayList<Search>>() {
        }.getType();
        Gson gson = new GsonBuilder().serializeNulls().create();
        searchArrayList = gson.fromJson(jsonResponse, listType);

        if (searchArrayList.size() > 0) {

            resetView();

            SearchHomeAdapter adapter = new SearchHomeAdapter(searchArrayList, context, new RecyclerClickListener() {
                @Override
                public void onItemClicked(int position) {

                    String productId = "" + searchArrayList.get(position).getId();
                    String productName = searchArrayList.get(position).getName();

                    if (MyUtils.checkStringValue(productId)) {
                        Bundle bundle = new Bundle();
                        bundle.putString(Constants.PRODUCT_ID, productId);
                        bundle.putString(Constants.PRODUCT_NAME, productName);
                        bundle.putString(Constants.PRODUCT_REGULAR_PRICE, "");
                        bundle.putString(Constants.PRODUCT_SALE_PRICE, "");

                        Intent intent = new Intent(context, ProductDetailActivity.class);
                        intent.putExtras(bundle);
                        startActivityForResult(intent, Constants.PRODUCTS);

                        MyUtils.openOverrideAnimation(true, SearchActivity.this);
                    }


                }
            });
            recyclerView.setAdapter(adapter);

        } else {
            showErrorCase(searchHint);
        }
    }

    private void showErrorCase(String searchHint) {
        if (recyclerView.getVisibility() == View.VISIBLE) {
            recyclerView.setVisibility(View.GONE);
        }

        if (emptyViewRelative.getVisibility() == View.GONE) {
            emptyViewRelative.setVisibility(View.VISIBLE);
            emptyText.setText("Search Result not found for " + "'" + searchHint + "'");
        }

    }


    private void resetView() {
        if (recyclerView.getVisibility() == View.GONE) {
            recyclerView.setVisibility(View.VISIBLE);
        }

        if (emptyViewRelative.getVisibility() == View.VISIBLE) {
            emptyViewRelative.setVisibility(View.GONE);
        }
    }


    @Override
    public void onBackPressed() {
        closePage();
    }

    private void closePage() {
        finish();
        MyUtils.openOverrideAnimation(false, SearchActivity.this);
    }


}
