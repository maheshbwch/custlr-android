package com.bestweb.custlr.activities.products;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.bestweb.custlr.R;
import com.bestweb.custlr.activities.LoginActivity;
import com.bestweb.custlr.models.ForgotPassResponse;
import com.bestweb.custlr.models.UpdatePassResponse;
import com.bestweb.custlr.utils.Constants;
import com.bestweb.custlr.utils.MyUtils;
import com.bestweb.custlr.web_service.ApiInterface;
import com.bestweb.custlr.web_service.HttpRequest;
import com.bestweb.custlr.web_service.JsonInput;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForgotPassword extends AppCompatActivity {

    private TextView txtForgot, txtBack;
    private Context context;
    private TextInputEditText edtEmail, edtCode, edtNewPass, edtConfirmPass;
    private Button btnReset, btnSetNewPass;
    private ProgressDialog progressDialog = null;
    private TextInputLayout emailLayout, codeLayout;
    private String otpKey = "";
    private LinearLayout linearPasswords;
    private String email = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        context = ForgotPassword.this;

        txtForgot = findViewById(R.id.txt_back);
        edtEmail = findViewById(R.id.af_edtEmail);
        edtCode = findViewById(R.id.af_edtCode);
        edtNewPass = findViewById(R.id.af_edtNewPass);
        edtConfirmPass = findViewById(R.id.af_edtConfirmPass);
        txtBack = findViewById(R.id.txt_back);

        btnReset = findViewById(R.id.btn_reset_password);
        btnSetNewPass = findViewById(R.id.btn_set_new_pass);
        emailLayout = findViewById(R.id.al_edtEmailLayout);
        codeLayout = findViewById(R.id.af_codeLayout);
        linearPasswords = findViewById(R.id.af_linearPasswords);


        txtForgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

               /* startActivity(new Intent(context, LoginActivity.class));
                MyUtils.openOverrideAnimation(false, ForgotPassword.this); //TODO CHANGE OVERRIDE TRANSITION
                finish();*/
                intentToLoginPage();


            }
        });

        btnReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                email = edtEmail.getText().toString();
                if (!MyUtils.checkStringValue(email)) {
                    MyUtils.ShowLongToast(context, "Email address required");
                } else if (!MyUtils.isValidEmail(email)) {
                    MyUtils.ShowLongToast(context, "Please enter valid email address");
                } else if (MyUtils.isInternetConnected(context)) {
                    forgetPassword(email);
                } else {
                    MyUtils.ShowLongToast(context, getString(R.string.no_internet));
                }


            }
        });

        edtCode.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (edtCode.getText().toString().equals(otpKey)) {
                    linearPasswords.setVisibility(View.VISIBLE);
                } else {
                    linearPasswords.setVisibility(View.GONE);
                }
            }
        });

        btnSetNewPass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                changeNewPassword();

            }
        });


        txtBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(context, LoginActivity.class));
                MyUtils.openOverrideAnimation(false, ForgotPassword.this); //TODO CHANGE OVERRIDE TRANSITION
                finish();
            }
        });


    }


    private void intentToLoginPage() {
        Intent intent = new Intent(context, LoginActivity.class);
        startActivityForResult(intent, Constants.LOGIN_CODE);
        MyUtils.openOverrideAnimation(false, ForgotPassword.this);
    }

    private void changeNewPassword() {


        String newPassword = edtNewPass.getText().toString();
        String confirmPassword = edtConfirmPass.getText().toString();
        String code = edtCode.getText().toString();

        if (!MyUtils.checkStringValue(code)) {

            MyUtils.ShowLongToast(context, "Code Required");
        } else if (!code.equals(otpKey)) {
            MyUtils.ShowLongToast(context, "Enter valid otp key");
        } else if (!MyUtils.checkStringValue(newPassword)) {
            MyUtils.ShowLongToast(context, "New Password Required");

        } else if (!MyUtils.checkStringValue(confirmPassword)) {
            MyUtils.ShowLongToast(context, "Confirm Password Required");

        } else if (!newPassword.equals(confirmPassword)) {
            MyUtils.ShowLongToast(context, "Confirm Password Mismatch");
        } else if (MyUtils.isInternetConnected(context)) {
            updatePassword(email, newPassword, code);
        } else {
            MyUtils.ShowLongToast(context, getString(R.string.no_internet));
        }

    }

    private void updatePassword(String email, String password, String pin) {

        try {
            progressDialog = MyUtils.showProgressLoader(context, "Updating...");
            ApiInterface apiService = HttpRequest.getScalarsInstance().create(ApiInterface.class);
            Call<String> call = apiService.updatePassword(JsonInput.updatePassword(email, password, pin));
            call.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    MyUtils.dismissProgressLoader(progressDialog);
                    try {
                        if (response != null) {
                            Gson gson = new GsonBuilder().serializeNulls().create();
                            UpdatePassResponse updatePassResponse = gson.fromJson(response.body(), new TypeToken<UpdatePassResponse>() {
                            }.getType());

                            if (updatePassResponse != null) {
                                String status = updatePassResponse.getStatus();
                                if (MyUtils.checkStringValue(status) && status.equals(Constants.SUCCESS)) {

                                    intentToLoginPage();
                                    MyUtils.ShowLongToast(context, "Your Password has been reset..");

                                } else {
                                    String message = updatePassResponse.getMessage();

                                    if (MyUtils.checkStringValue(message)) {
                                        MyUtils.ShowLongToast(context, "" + message);
                                    } else {
                                        MyUtils.ShowLongToast(context, "Unable to login,try again later");
                                    }
                                }
                            }
                        }

                    } catch (Exception e) {
                        Log.e("exception33", ":" + e.getMessage());
                    }
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    Log.e("exception11", ":" + t.getMessage());
                    MyUtils.dismissProgressLoader(progressDialog);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            MyUtils.dismissProgressLoader(progressDialog);
        }

    }


    private void forgetPassword(String email) {


        try {
            progressDialog = MyUtils.showProgressLoader(context, "Loading...");
            ApiInterface apiService = HttpRequest.getScalarsInstance().create(ApiInterface.class);
            Call<String> call = apiService.forgotPass(JsonInput.forgtoPassJson(email));
            call.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    MyUtils.dismissProgressLoader(progressDialog);
                    try {
                        if (response != null) {
                            Gson gson = new GsonBuilder().serializeNulls().create();
                            ForgotPassResponse forgotResponse = gson.fromJson(response.body(), new TypeToken<ForgotPassResponse>() {
                            }.getType());

                            if (forgotResponse != null) {
                                String status = forgotResponse.getStatus();
                                if (MyUtils.checkStringValue(status) && status.equals(Constants.SUCCESS)) {
                                    Log.e("status", ": " + status);
                                    updateUI(forgotResponse);

                                } else {
                                    String message = forgotResponse.getMessage();

                                    if (MyUtils.checkStringValue(message)) {
                                        MyUtils.ShowLongToast(context, "" + message);
                                    } else {
                                        MyUtils.ShowLongToast(context, "Unable to login,try again later");
                                    }
                                }
                            }
                        }

                    } catch (Exception e) {
                        Log.e("exception33", ":" + e.getMessage());
                    }
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    Log.e("exception11", ":" + t.getMessage());
                    MyUtils.dismissProgressLoader(progressDialog);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            MyUtils.dismissProgressLoader(progressDialog);
        }


    }

    private void updateUI(ForgotPassResponse response) {

        MyUtils.ShowLongToast(context, "Your code has been sent to your email..");
        emailLayout.setVisibility(View.GONE);
        btnReset.setVisibility(View.GONE);
        codeLayout.setVisibility(View.VISIBLE);
        btnSetNewPass.setVisibility(View.VISIBLE);
        otpKey = response.getKey();

    }


}
