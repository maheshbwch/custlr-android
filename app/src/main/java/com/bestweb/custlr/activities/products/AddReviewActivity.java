package com.bestweb.custlr.activities.products;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.bestweb.custlr.R;
import com.bestweb.custlr.interfaces.DialogInterface;
import com.bestweb.custlr.models.AddReview;
import com.bestweb.custlr.utils.Constants;
import com.bestweb.custlr.utils.CustomDialog;
import com.bestweb.custlr.utils.MyUtils;
import com.bestweb.custlr.utils.PreferenceHandler;
import com.bestweb.custlr.utils.RequestParamUtils;
import com.bestweb.custlr.web_service.ApiInterface;
import com.bestweb.custlr.web_service.HttpRequest;
import com.bestweb.custlr.web_service.JsonInput;
import com.bumptech.glide.Glide;
import com.google.android.material.textfield.TextInputEditText;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddReviewActivity extends AppCompatActivity {

    private Context context;
    private ImageView imgBack, prdImageView;
    private TextView txtHeader, prdNameTxt;

    private TextInputEditText nameEdt, emailEdt, commentEdt;
    private RatingBar ratingBar;
    private Button submitButton;
    private ProgressDialog progressDialog = null;

    private String productId;
    private String userId;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_review);
        context = AddReviewActivity.this;

        imgBack = findViewById(R.id.imgBack);
        txtHeader = findViewById(R.id.txtHeader);

        prdImageView = findViewById(R.id.ar_productImage);
        prdNameTxt = findViewById(R.id.ar_productNameTxt);

        nameEdt = findViewById(R.id.ar_nameEdt);
        emailEdt = findViewById(R.id.ar_emailEdt);
        commentEdt = findViewById(R.id.ar_commentEdt);

        ratingBar = findViewById(R.id.ar_ratingBar);
        submitButton = findViewById(R.id.ar_submitButton);

        init();

    }

    private void init() {
        txtHeader.setText("REVIEW");

        Bundle bundle = getIntent().getExtras();
        productId = bundle.getString(Constants.PRODUCT_ID);
        String productName = bundle.getString(Constants.PRODUCT_NAME);
        String productPrice = bundle.getString(Constants.PRODUCT_PRICE);
        String imgUrl = bundle.getString(Constants.PRODUCT_IMAGE_URL);

        userId = MyUtils.getPreferences(context).getString(RequestParamUtils.ID, "");


        Log.e("bundle", "value:" + productId);

        setUpProductDetails(productName, imgUrl);

        setUpUsersDetails();


        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closePage();
            }
        });


        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                validateFields();

            }
        });

    }


    private void validateFields() {
        String userName = nameEdt.getText().toString().trim();
        String email = emailEdt.getText().toString().trim();
        String comments = commentEdt.getText().toString().trim();

        if (!MyUtils.checkStringValue(userName)) {
            showAlertToUser(Constants.DIALOG_WARNING, "User Name required");
        } else if (!MyUtils.checkStringValue(email)) {
            showAlertToUser(Constants.DIALOG_WARNING, "Password required");
        } else if (!MyUtils.checkStringValue(comments)) {
            showAlertToUser(Constants.DIALOG_WARNING, "Comments required");
        } else if (MyUtils.isInternetConnected(context)) {

            int rating = Math.round(ratingBar.getRating());

            submitReview(userName, email, comments, rating);

        } else {
            showAlertToUser(Constants.DIALOG_WARNING, getString(R.string.no_internet));
        }
    }

    private void submitReview(String userName, String email, String comments, int rating) {

        Log.e("userName", ":" + userName);
        Log.e("email", ":" + email);
        Log.e("comments", ":" + comments);
        Log.e("rating", ":" + rating);

        try {
            progressDialog = MyUtils.showProgressLoader(context, "Submitting Review..");
            ApiInterface apiService = HttpRequest.getScalarsInstance().create(ApiInterface.class);
            Call<String> call = apiService.addReview(JsonInput.addReviewInput(email, userName, productId, comments, rating, userId));
            call.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    MyUtils.dismissProgressLoader(progressDialog);
                    try {
                        if (response != null) {

                            Type type = new TypeToken<AddReview>() {
                            }.getType();
                            Gson gson = new GsonBuilder().serializeNulls().create();
                            AddReview addReview = gson.fromJson(response.body(), type);

                            if (addReview != null) {

                                String status = addReview.getStatus();

                                if (MyUtils.checkStringValue(status)) {
                                    if (status.equalsIgnoreCase(Constants.SUCCESS)) {

                                        String message = addReview.getMessage();

                                        MyUtils.ShowLongToast(context, "" + message);

                                        closePage();

                                    } else if (status.equalsIgnoreCase(Constants.ERROR)) {

                                        String error = addReview.getError();

                                        MyUtils.ShowLongToast(context, "" + error);
                                    }
                                }
                            }
                        }
                    } catch (Exception e) {
                        Log.e("exception33", ":" + e.getMessage());
                    }
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    Log.e("exception11", ":" + t.getMessage());
                    MyUtils.dismissProgressLoader(progressDialog);
                }
            });
        } catch (Exception e) {
            MyUtils.dismissProgressLoader(progressDialog);
        }
    }


    private void showAlertToUser(String statusCode, String statusMessage) {
        CustomDialog customDialog = new CustomDialog(AddReviewActivity.this, statusCode, statusMessage, new DialogInterface() {
            @Override
            public void onButtonClicked() {

            }

            @Override
            public void onDismissedClicked() {

            }
        });
        customDialog.showDialog();
    }


    private void setUpUsersDetails() {

        if (checkIfUserLoggedIn()) {

            String displayName = MyUtils.getPreferences(context).getString(RequestParamUtils.DISPLAY_NAME, "");
            String displayEmail = MyUtils.getPreferences(context).getString(RequestParamUtils.DISPLAY_EMAIL, "");

            nameEdt.setText(MyUtils.checkStringValue(displayName) ? displayName : "");
            emailEdt.setText(MyUtils.checkStringValue(displayEmail) ? displayEmail : "");

        }


    }

    private boolean checkIfUserLoggedIn() {
        boolean loginState = PreferenceHandler.getPreferenceFromBoolean(context, Constants.USER_LOGIN);
        Log.e("login", "value:" + loginState);
        return loginState;
    }


    private void setUpProductDetails(String productName, String imgUrl) {
        prdNameTxt.setText(MyUtils.checkStringValue(productName) ? productName : "-");
        Glide.with(context).load(imgUrl).into(prdImageView);
    }


    private void closePage() {
        finish();
        MyUtils.openOverrideAnimation(false, AddReviewActivity.this);
    }

    @Override
    public void onBackPressed() {
        closePage();
    }

}
