package com.bestweb.custlr.activities.products;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import com.bestweb.custlr.R;
import com.bestweb.custlr.adapters.CategoryPagerAdapter;
import com.bestweb.custlr.fragments.products.ProductsFragment;
import com.bestweb.custlr.interfaces.CatSelectionListener;
import com.bestweb.custlr.models.MainCategories;
import com.bestweb.custlr.utils.Constants;
import com.bestweb.custlr.utils.MyUtils;
import com.bestweb.custlr.utils.RequestParamUtils;
import com.bestweb.custlr.web_service.ApiInterface;
import com.bestweb.custlr.web_service.HttpRequest;
import com.bestweb.custlr.web_service.JsonInput;
import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProductMainActivity extends AppCompatActivity {

    private Context context;
    private ImageView imgBack;
    private TabLayout tabLayout;
    private ViewPager viewPager;

    private ArrayList<MainCategories> mainCategoriesList = new ArrayList<>();
    private boolean isPriceLowToHigh = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_main);
        context = ProductMainActivity.this;

        imgBack = findViewById(R.id.ap_imgBack);
        tabLayout = findViewById(R.id.tab_layout);
        viewPager = findViewById(R.id.ap_view_pager);


        Bundle bundle = getIntent().getExtras();
        String categoriesJson = bundle.getString(Constants.CATEGORIES_JSON);

        if (MyUtils.checkStringValue(categoriesJson)) {

            try {
                Type listType = new TypeToken<ArrayList<MainCategories>>() {
                }.getType();
                Gson gson = new GsonBuilder().serializeNulls().create();
                mainCategoriesList = gson.fromJson(categoriesJson, listType);

                if (mainCategoriesList.size() > 0) {
                    if (mainCategoriesList.size() <= 3) {
                        tabLayout.setTabMode(TabLayout.MODE_FIXED);
                    } else {
                        tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
                    }
                    setupViewPager(viewPager, mainCategoriesList);
                    tabLayout.setupWithViewPager(viewPager);
                    viewPager.setCurrentItem(0);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }


        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                closePage();
            }
        });

    }

    private void setupViewPager(ViewPager viewPager, ArrayList<MainCategories> mainCategoriesList) {
        try {
            CategoryPagerAdapter adapter = new CategoryPagerAdapter(getSupportFragmentManager());
            for (int i = 0; i < mainCategoriesList.size(); i++) {
                String subCatName = mainCategoriesList.get(i).getMainCatName();
                String subCatId = mainCategoriesList.get(i).getMainCatId();
                adapter.addFragment(new ProductsFragment(subCatName, subCatId, mainCategoriesList, new CatSelectionListener() {
                    @Override
                    public void onCategorySelected(int position) {
                        setPager(position);
                    }
                }), subCatName);
            }
            viewPager.setAdapter(adapter);
        } catch (Exception e) {
            Log.e("exception", ":" + e.getMessage());
        }
    }


    private void setPager(int position) {
        if (viewPager != null) {
            int pagerPosition = viewPager.getCurrentItem();
            if (pagerPosition != position) {
                viewPager.setCurrentItem(position);
            }
        }
    }

    private void closePage() {
        setResult(Constants.PRODUCTS);
        finish();
        MyUtils.openOverrideAnimation(false, ProductMainActivity.this);
    }

    @Override
    public void onBackPressed() {
        closePage();
    }
}
