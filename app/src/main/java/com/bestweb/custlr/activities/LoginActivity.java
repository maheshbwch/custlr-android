package com.bestweb.custlr.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import com.bestweb.custlr.R;
import com.bestweb.custlr.activities.products.ForgotPassword;
import com.bestweb.custlr.interfaces.DialogInterface;
import com.bestweb.custlr.models.LogInResponse;
import com.bestweb.custlr.utils.APIS;
import com.bestweb.custlr.utils.Constants;
import com.bestweb.custlr.utils.CustomDialog;
import com.bestweb.custlr.utils.MyUtils;
import com.bestweb.custlr.utils.PreferenceHandler;
import com.bestweb.custlr.utils.RequestParamUtils;
import com.bestweb.custlr.web_service.ApiInterface;
import com.bestweb.custlr.web_service.HttpRequest;
import com.bestweb.custlr.web_service.JsonInput;
import com.bumptech.glide.Glide;
import com.ciyashop.library.apicall.PostApi;
import com.ciyashop.library.apicall.URLS;
import com.ciyashop.library.apicall.interfaces.OnResponseListner;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginBehavior;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.Arrays;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity implements OnResponseListner {

    String socialImgUrl = "", social_displayName = "";

    private Context context;
    private ImageView backgroundImage;
    private CardView loginCardView, facebookCardView, googleCardView;
    private TextView txtSignUp, txtForgot;
    private TextInputEditText edtUsername, edtPassword;
    private ProgressDialog progressDialog = null;

    private GoogleSignInClient mGoogleSignInClient;
    private FirebaseAuth mAuth;

    CallbackManager callbackManager;
    private String facbookImageUrl;
    private JSONObject fbjsonObject;
    private static final String TAG = LoginActivity.class.getSimpleName();

    private String deviceId = "";
    private String fcmToken = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        context = LoginActivity.this;

        backgroundImage = findViewById(R.id.al_backgroundImage);
        loginCardView = findViewById(R.id.al_loginCardView);
        facebookCardView = findViewById(R.id.al_facebookCardView);
        googleCardView = findViewById(R.id.al_googleCardView);

        txtSignUp = findViewById(R.id.al_txtSignUp);
        txtForgot = findViewById(R.id.txtForgotPass);

        edtUsername = findViewById(R.id.al_edtUsername);
        edtPassword = findViewById(R.id.al_edtPassword);

        Glide.with(context).load(R.drawable.login).into(backgroundImage);

        configureGoogleLogin();

        loginWithFB();


        new APIS();

        getFcmToken();


        loginCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                userLoginValidation();
            }
        });


        facebookCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                LoginManager.getInstance().logOut();
                LoginManager.getInstance().logInWithReadPermissions(LoginActivity.this, Arrays.asList(RequestParamUtils.email, RequestParamUtils.publicProfile));


            }
        });

        googleCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mGoogleSignInClient != null) {

                    if (mAuth!=null) {
                        mAuth.signOut();
                    }
                    mGoogleSignInClient.signOut();


                    Intent signInIntent = mGoogleSignInClient.getSignInIntent();
                    startActivityForResult(signInIntent, Constants.GOOGLE_SIGN_IN);
                }



            }
        });

        txtSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(context, SignUpActivity.class));
                MyUtils.openOverrideAnimation(true, LoginActivity.this);
            }
        });

        txtForgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(context, ForgotPassword.class));
                MyUtils.openOverrideAnimation(true, LoginActivity.this);
            }
        });


    }

    private void configureGoogleLogin() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

        mAuth = FirebaseAuth.getInstance();
    }


    private void getFcmToken() {
        deviceId = MyUtils.getDeviceId(context);
        fcmToken = PreferenceHandler.getPreferenceFromString(context, Constants.FCM_TOKEN);
        if (!MyUtils.checkStringValue(fcmToken)) {
            String refreshedToken = FirebaseInstanceId.getInstance().getToken();
            PreferenceHandler.storePreference(context, Constants.FCM_TOKEN, refreshedToken);
            fcmToken = PreferenceHandler.getPreferenceFromString(context, Constants.FCM_TOKEN);
        }

        Log.e("fcmToken", ":" + fcmToken);
    }


    public void loginWithFB() {
        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().setLoginBehavior(LoginBehavior.WEB_ONLY);

        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {

            @Override
            public void onSuccess(LoginResult loginResult) {
                String accessToken = loginResult.getAccessToken()
                        .getToken();
                Log.e("accessToken", accessToken);

                GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response) {

                                Log.e("LoginActivity", response.toString());
                                try {
                                    String id = object.getString("id");
                                    social_displayName = object.getString("first_name");
                                    facbookImageUrl = "https://graph.facebook.com/" + id + "/picture?type=large";
                                    // facbookImageUrl = "http://graph.facebook.com/" + id + "/picture?type=large";
                                    fbjsonObject = object;
                                    // if (fbjsonObject != null) {
                                    new getBitmap().execute();
//                                    }else {
//                                        socialLogin(fbjsonObject);
//                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id, name, email, gender, first_name, last_name, picture.type(large)");
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {
                // App code
            }

            @Override
            public void onError(FacebookException exception) {
                Log.e(TAG, "onError: " + exception.toString());
                // App code
            }
        });
    }

    public String getBitmap() {
        try {
            URL url = new URL(facbookImageUrl);
            try {
                Bitmap mIcon = BitmapFactory.decodeStream(url.openConnection().getInputStream());
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                mIcon.compress(Bitmap.CompressFormat.JPEG, 100, baos); //bm is the bitmap object
                byte[] b = baos.toByteArray();

                String encoded = Base64.encodeToString(b, Base64.DEFAULT);

                return encoded;
            } catch (IOException e) {
                Log.e("IOException ", e.getMessage());

            } catch (Exception e) {
                Log.e("Exception ", e.getMessage());

            }
            Log.e("Done", "Done");
        } catch (IOException e) {
            Log.e("Exception Url ", e.getMessage());
        }
        return null;
    }



    private void isUserLogin(boolean status) {
        if (status) {
            PreferenceHandler.storeBooleanValue(context, Constants.USER_LOGIN, true);
            setResult(Constants.LOGIN_SUCCESS);
            finish();
        } else {
            PreferenceHandler.storeBooleanValue(context, Constants.USER_LOGIN, false);
            setResult(Constants.LOGIN_FAILED);
            finish();
        }

    }

    private void userLoginValidation() {

        String userName = edtUsername.getText().toString().trim();
        String password = edtPassword.getText().toString().trim();

        if (!MyUtils.checkStringValue(userName)) {
            showAlertToUser(Constants.DIALOG_WARNING, "User Name required");
        } else if (!MyUtils.checkStringValue(password)) {
            showAlertToUser(Constants.DIALOG_WARNING, "Password required");
        } else if (MyUtils.isInternetConnected(context)) {
            userLogin(userName, password);
        } else {
            showAlertToUser(Constants.DIALOG_WARNING, getString(R.string.no_internet));
        }

    }


    private void userLogin(String userName, String password) {
        try {
            progressDialog = MyUtils.showProgressLoader(context, "Loading...");
            ApiInterface apiService = HttpRequest.getScalarsInstance().create(ApiInterface.class);
            Call<String> call = apiService.getLoginResponse(JsonInput.getLoginJsonObject(userName, password, fcmToken, APIS.DEVICE_TYPE));
            call.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    MyUtils.dismissProgressLoader(progressDialog);
                    try {
                        if (response != null) {
                            Gson gson = new GsonBuilder().serializeNulls().create();
                            LogInResponse logInResponse = gson.fromJson(response.body(), new TypeToken<LogInResponse>() {
                            }.getType());

                            Log.e("code", ": " + response.code());
                            Log.e("message", ": " + response.message());

                            if (logInResponse != null) {
                                String status = logInResponse.getStatus();
                                if (MyUtils.checkStringValue(status) && status.equals(Constants.SUCCESS)) {

                                    SharedPreferences.Editor pre = MyUtils.getPreferences(context).edit();
                                    pre.putString(RequestParamUtils.CUSTOMER, "");
                                    pre.putString(RequestParamUtils.ID, logInResponse.getUser().id + "");
                                    pre.putString(RequestParamUtils.DISPLAY_NAME, logInResponse.getUser().displayname + "");
                                    pre.putString(RequestParamUtils.DISPLAY_EMAIL, logInResponse.getUser().email + "");
                                    pre.putString(RequestParamUtils.PROFILE_PIC, logInResponse.getUser().url + "");
                                    pre.commit();
                                    isUserLogin(true);

                                } else {
                                    String message = logInResponse.getMessage();
                                    if (MyUtils.checkStringValue(message)) {
                                        MyUtils.ShowLongToast(context, "" + message);
                                    } else {
                                        MyUtils.ShowLongToast(context, "Unable to login,try again later");
                                    }

                                    //isUserLogin(false);
                                }
                            }
                        }

                    } catch (Exception e) {
                        Log.e("exception33", ":" + e.getMessage());
                        isUserLogin(false);
                    }
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    isUserLogin(false);
                    Log.e("exception11", ":" + t.getMessage());
                    MyUtils.dismissProgressLoader(progressDialog);
                }
            });
        } catch (Exception e) {
            isUserLogin(false);
            e.printStackTrace();
            MyUtils.dismissProgressLoader(progressDialog);
        }
    }


    private void showAlertToUser(String statusCode, String statusMessage) {
        CustomDialog customDialog = new CustomDialog(LoginActivity.this, statusCode, statusMessage, new DialogInterface() {
            @Override
            public void onButtonClicked() {

            }

            @Override
            public void onDismissedClicked() {

            }
        });
        customDialog.showDialog();
    }





    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.e("requestCode", ":" + requestCode);
        Log.e("resultCode", ":" + resultCode);
        if (requestCode == Constants.GOOGLE_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                GoogleSignInAccount account = task.getResult(ApiException.class);
                firebaseAuthWithGoogle(account);
            } catch (ApiException e) {
                Log.w(TAG, "Google sign in failed", e);
            }
        }
    }

    private void firebaseAuthWithGoogle(GoogleSignInAccount acct) {
        Log.d(TAG, "firebaseAuthWithGoogle:" + acct.getId());
        //showProgressBar();
        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithCredential:success");
                            FirebaseUser user = mAuth.getCurrentUser();

                            if (user!=null){

                                try {
                                    JSONObject jsonObject = new JSONObject();
                                    jsonObject.put(RequestParamUtils.socialId, user.getUid());
                                    jsonObject.put(RequestParamUtils.email, user.getEmail());
                                    jsonObject.put(RequestParamUtils.firstName, user.getDisplayName());
                                    jsonObject.put(RequestParamUtils.lastName, "Raghu");
                                    jsonObject.put(RequestParamUtils.imgUrlSocial, user.getPhotoUrl());

                                    socialImgUrl = user.getPhotoUrl().toString();
                                    social_displayName = user.getDisplayName();


                                    socialLogin(jsonObject);
                                } catch (Exception e) {
                                    Log.e("error", e.getMessage());
                                }

                            }

                            Log.e("email", ":" + user.getEmail());
                            Log.e("uid", ":" + user.getUid());
                        }
                        //hideProgressBar();
                    }
                });
    }

    public void socialLogin(final JSONObject object) {
        if (MyUtils.isInternetConnected(context)) {
            //progressDialog = MyUtils.showProgressLoader(context, "Connecting..");
            final PostApi postApi = new PostApi(LoginActivity.this, RequestParamUtils.socialLogin, this, "");

            try {
                object.put(RequestParamUtils.deviceType, Constants.DEVICE_TYPE);
                object.put(RequestParamUtils.deviceToken, fcmToken);
                postApi.callPostApi(new URLS().SOCIAL_LOGIN, object.toString());

            } catch (Exception e) {
                //MyUtils.dismissProgressLoader(progressDialog);
                Log.e("error", e.getMessage());
            }
        } else {
            Toast.makeText(LoginActivity.this, R.string.no_internet, Toast.LENGTH_LONG).show();
        }
    }



    @Override
    public void onResponse(String response, final String methodName) {

        if (methodName.equals(RequestParamUtils.login) || methodName.equals(RequestParamUtils.socialLogin)) {
            if (response != null && response.length() > 0) {
                try {
                    JSONObject jsonObj = new JSONObject(response);

                    Log.e("resposne", ":  " + response);
                    String status = jsonObj.getString("status");
                    if (status.equals("success")) {
                        final LogInResponse loginRider = new Gson().fromJson(
                                response, new TypeToken<LogInResponse>() {
                                }.getType());

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (loginRider.status.equals("success")) {
                                    if (methodName.equals(RequestParamUtils.socialLogin)) {
                                        //MyUtils.dismissProgressLoader(progressDialog);
                                    }
                                    SharedPreferences.Editor pre = MyUtils.getPreferences(context).edit();
                                    pre.putString(RequestParamUtils.CUSTOMER, "");
                                    pre.putString(RequestParamUtils.ID, loginRider.user.id + "");
                                    pre.putString(RequestParamUtils.SOCIAL_IMG_URL, socialImgUrl);
                                    pre.putString(RequestParamUtils.SOCIAL_IMG_URL_FB, facbookImageUrl);
                                    pre.putString(RequestParamUtils.DISPLAY_NAME, social_displayName + "");
                                    if (methodName.equals(RequestParamUtils.socialLogin)) {
                                        pre.putString(RequestParamUtils.SOCIAL_SIGNIN, "1");
                                    }
                                    pre.commit();
                                    isUserLogin(true);
                                } else {
                                    Toast.makeText(getApplicationContext(), R.string.enter_proper_detail, Toast.LENGTH_SHORT).show(); //display in long period of time
                                    isUserLogin(false);
                                }
                            }
                        });
                    } else {
                        isUserLogin(false);
                        String msg = jsonObj.getString("message");
                        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    isUserLogin(false);
                    Log.e(methodName + "Gson Exception is ", e.getMessage());
                    Toast.makeText(getApplicationContext(), R.string.something_went_wrong, Toast.LENGTH_SHORT).show(); //display in long period of time
                }
            }
        }

    }


    class getBitmap extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            progressDialog = MyUtils.showProgressLoader(context, "Loading...");
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... strings) {
            return getBitmap();
        }

        @Override
        protected void onPostExecute(String encoded) {
            super.onPostExecute(encoded);
            MyUtils.dismissProgressLoader(progressDialog);
            try {
                JSONObject jsonObject = new JSONObject();

                jsonObject.put(RequestParamUtils.data, encoded);
                jsonObject.put(RequestParamUtils.name, "image.jpg");

                Log.e("name", fbjsonObject.getString("name"));
                Log.e("email", fbjsonObject.getString("email"));
                if (fbjsonObject.has(RequestParamUtils.gender)) {
                    Log.e("gender", fbjsonObject.getString(RequestParamUtils.gender));
                }

                fbjsonObject.put(RequestParamUtils.userImage, jsonObject);
                fbjsonObject.put(RequestParamUtils.socialId, fbjsonObject.getString("id"));
                socialLogin(fbjsonObject);


            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onBackPressed() {
        isUserLogin(false);
    }
}
