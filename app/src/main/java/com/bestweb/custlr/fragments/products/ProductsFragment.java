package com.bestweb.custlr.fragments.products;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bestweb.custlr.R;
import com.bestweb.custlr.activities.LoginActivity;
import com.bestweb.custlr.activities.products.ProductDetailActivity;
import com.bestweb.custlr.adapters.PrdPopUpAdapter;
import com.bestweb.custlr.adapters.product.ProductsListAdapter;
import com.bestweb.custlr.interfaces.CatSelectionListener;
import com.bestweb.custlr.interfaces.RecyclerClickListener;
import com.bestweb.custlr.interfaces.WishListListener;
import com.bestweb.custlr.models.MainCategories;
import com.bestweb.custlr.models.Products;
import com.bestweb.custlr.models.WishList;
import com.bestweb.custlr.utils.Constants;
import com.bestweb.custlr.utils.MyUtils;
import com.bestweb.custlr.utils.RequestParamUtils;
import com.bestweb.custlr.utils.custom_view.shimmer_recycler.ShimmerRecyclerView;
import com.bestweb.custlr.utils.custom_view.spark_button.SparkButton;
import com.bestweb.custlr.web_service.ApiInterface;
import com.bestweb.custlr.web_service.HttpRequest;
import com.bestweb.custlr.web_service.JsonInput;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProductsFragment extends Fragment {

    private Context context;
    private ShimmerRecyclerView recyclerView;
    private GridLayoutManager gridLayoutManager;
    private ProgressBar bottomProgressBar;
    private ProgressDialog progressDialog = null;

    private RelativeLayout emptyViewRelative;
    private LinearLayout productsLinear, linearSortType, sortPriceLinear;
    private PopupWindow kindsPopupWindow;
    private TextView txtSortType, sortPriceTxt;

    private String subCatName;
    private String subCatId;
    private ArrayList<MainCategories> mainCategoriesList = new ArrayList<>();
    private CatSelectionListener catSelectionListener = null;
    private ArrayList<String> wishListIdsArrayList = new ArrayList<>();


    private boolean isPriceLowToHigh = true;
    private ArrayList<Products> productsArrayList = new ArrayList<>();
    private ProductsListAdapter productsListAdapter = null;
    private boolean isLoading = true;
    private int pageNumber = 1;
    private String sortType = "";


    private static final String LOW_TO_HIGH = "price";
    private static final String HIGH_TO_LOW = "price-desc";


    public ProductsFragment(String subCatName, String subCatId, ArrayList<MainCategories> mainCategoriesList, CatSelectionListener catSelectionListener) {
        this.subCatName = subCatName;
        this.subCatId = subCatId;
        this.mainCategoriesList = mainCategoriesList;
        this.catSelectionListener = catSelectionListener;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.new_arrival_fragment, container, false);
        context = getActivity();

        productsListAdapter = null;


        productsLinear = view.findViewById(R.id.na_productsLinear);

        linearSortType = view.findViewById(R.id.na_linearSortType);
        txtSortType = view.findViewById(R.id.na_txtSortType);
        sortPriceLinear = view.findViewById(R.id.na_sortPriceLinear);
        sortPriceTxt = view.findViewById(R.id.na_sortPriceTxt);

        recyclerView = view.findViewById(R.id.na_shimmer_recycler_view);
        bottomProgressBar = view.findViewById(R.id.na_bottomProgressBar);

        emptyViewRelative = view.findViewById(R.id.na_emptyViewRelative);

        recyclerView.setHasFixedSize(true);
        gridLayoutManager = new GridLayoutManager(getActivity(), 3);
        recyclerView.setLayoutManager(gridLayoutManager);


        showWishListEnabledProducts();


        linearSortType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (mainCategoriesList.size() > 0) {
                    showCategoriesPopupWindow(mainCategoriesList);
                }

            }
        });

        setUpCategoryName(subCatName);

        return view;
    }


    private void showWishListEnabledProducts() {
        String userId = MyUtils.getPreferences(context).getString(RequestParamUtils.ID, "");
        Log.e("userId", ":" + userId);
        if (MyUtils.checkIfUserLoggedIn(context) && MyUtils.checkStringValue(userId)) {
            getWishListIds(userId);
        } else {
            getProducts();
        }
    }


    private void getWishListIds(String userId) {
        try {
            ApiInterface apiService = HttpRequest.getScalarsInstance().create(ApiInterface.class);
            Call<String> call = apiService.getWishListIds(JsonInput.getWishListIds(userId));
            call.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    try {
                        String jsonResponse = response.body();

                        Log.e("ids_response", ":" + jsonResponse);

                        if (jsonResponse != null) {

                            JSONObject jsonObject = new JSONObject(jsonResponse);

                            String status = jsonObject.optString("status");

                            if (MyUtils.checkStringValue(status) && status.equalsIgnoreCase("success")) {

                                if (jsonObject.has("sync_list")) {
                                    JSONArray jsonArray = jsonObject.optJSONArray("sync_list");

                                    if (jsonArray != null && jsonArray.length() > 0) {

                                        wishListIdsArrayList.clear();
                                        for (int i = 0; i < jsonArray.length(); i++) {
                                            String prod_id = jsonArray.optJSONObject(i).optString("prod_id");
                                            wishListIdsArrayList.add(prod_id);
                                        }
                                    }
                                }
                            }

                            getProducts();
                        } else {
                            getProducts();
                        }

                    } catch (Exception e) {
                        Log.e("exception33", ":" + e.getMessage());
                    }
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    Log.e("exception11", ":" + t.getMessage());
                }
            });
        } catch (Exception e) {
            Log.e("exception22", ":" + e.getMessage());
        }
    }


    private void getProducts() {
        sortType = LOW_TO_HIGH;
        pageNumber = 1;
        getProductsList(pageNumber, true);
    }


    private void getProductsList(int pageNumber, final boolean isLoadingForFirstTime) {
        try {

            if (isLoadingForFirstTime) {
                productsArrayList.clear();
                recyclerView.showShimmerAdapter();
            } else {
                bottomProgressBar.setVisibility(View.VISIBLE);
            }
            ApiInterface apiService = HttpRequest.getScalarsInstance().create(ApiInterface.class);
            Log.e("subCatId", ":" + subCatId + " -- " + "pageNumber: " + pageNumber + " -- " + "sortType :" + sortType);

            Call<String> call = apiService.getProductsList(JsonInput.productList(subCatId, pageNumber, sortType));
            call.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    hideShimmerView(isLoadingForFirstTime);
                    try {
                        String jsonResponse = response.body();

                        Log.e("prdResponse", ":" + jsonResponse);

                        if (jsonResponse != null) {

                            //{"status":"error","message":"No product found"}

                            Type listType = new TypeToken<ArrayList<Products>>() {
                            }.getType();
                            Gson gson = new GsonBuilder().serializeNulls().create();
                            ArrayList<Products> arrayList = gson.fromJson(jsonResponse, listType);

                            productsArrayList.addAll(arrayList);

                            adaptProductsList(isLoadingForFirstTime);


                            sortPriceLinear.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    setUpPriceSort();
                                }
                            });


                        } else {
                            showErrorCase(isLoadingForFirstTime);
                        }

                    } catch (Exception e) {
                        if (!isLoadingForFirstTime) {
                            isLoading = isLoadingForFirstTime;
                        }

                        Log.e("exception33", ":" + e.getMessage());
                        showErrorCase(isLoadingForFirstTime);
                    }
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    Log.e("exception11", ":" + t.getMessage());
                    hideShimmerView(isLoadingForFirstTime);
                    showErrorCase(isLoadingForFirstTime);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            hideShimmerView(isLoadingForFirstTime);
            showErrorCase(isLoadingForFirstTime);
        }

    }

    private void hideShimmerView(boolean isLoadingForFirstTime) {
        if (isLoadingForFirstTime) {
            recyclerView.hideShimmerAdapter();
        } else {
            hideBottomProgressBar();
        }
    }


    private void adaptProductsList(boolean isLoadingForFirstTime) {

        if (productsArrayList.size() > 0) {

            resetView(isLoadingForFirstTime);

            Log.e("size", ":" + productsArrayList.size());

            if (productsListAdapter != null) {
                productsListAdapter.notifyDataSetChanged();
                isLoading = true;

                Log.e("productsListAdapter", "not null");

            } else {

                RecyclerClickListener recyclerClickListener = new RecyclerClickListener() {
                    @Override
                    public void onItemClicked(int position) {

                        String productId = "" + productsArrayList.get(position).getId();
                        String productName = productsArrayList.get(position).getName();
                        String productSalePrice = "" + productsArrayList.get(position).getSalePrice();
                        String productRegularPrice = "" + productsArrayList.get(position).getRegularPrice();

                        if (MyUtils.checkStringValue(productId)) {
                            Bundle bundle = new Bundle();
                            bundle.putString(Constants.PRODUCT_ID, productId);
                            bundle.putString(Constants.PRODUCT_NAME, productName);
                            bundle.putString(Constants.PRODUCT_REGULAR_PRICE, productRegularPrice);
                            bundle.putString(Constants.PRODUCT_SALE_PRICE, productSalePrice);

                            Intent intent = new Intent(context, ProductDetailActivity.class);
                            intent.putExtras(bundle);
                            startActivityForResult(intent, Constants.PRODUCTS);

                            MyUtils.openOverrideAnimation(true, getActivity());
                        }

                    }
                };

                WishListListener wishListClickListener = new WishListListener() {
                    @Override
                    public void onWishListListen(SparkButton wishListButton, String productId) {

                        String userId = MyUtils.getPreferences(context).getString(RequestParamUtils.ID, "");

                        if (MyUtils.checkIfUserLoggedIn(context) && MyUtils.checkStringValue(userId)) {

                            addProductToWishList(wishListButton.isChecked(), wishListButton, productId, userId);

                        } else {
                            disableWishListButton(wishListButton);

                            intentToLoginPage();
                        }


                        if (wishListButton.isChecked()) {
                            Log.e("wishlist", "addProduct:" + productId);
                        } else {
                            Log.e("wishlist", "removeProduct:" + productId);
                        }

                    }
                };


                productsListAdapter = new ProductsListAdapter(context, productsArrayList, wishListIdsArrayList, recyclerClickListener, wishListClickListener);
                recyclerView.setAdapter(productsListAdapter);

            }

            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                    if (dy > 0) //check for scroll down
                    {
                        int visibleItemCount = gridLayoutManager.getChildCount();
                        int totalItemCount = gridLayoutManager.getItemCount();
                        int pastVisibleItems = gridLayoutManager.findFirstVisibleItemPosition();

                        if (isLoading) {
                            if ((visibleItemCount + pastVisibleItems) >= totalItemCount) {
                                isLoading = false;
                                Log.e("recyclerView", "bottom_reached");
                                pageNumber = pageNumber + 1;

                                getProductsList(pageNumber, false);

                                Log.e("page_number", "number:" + pageNumber);
                            }
                        }
                    }
                }
            });


        } else {
            showErrorCase(isLoadingForFirstTime);
        }
    }


    private void showCategoriesPopupWindow(final ArrayList<MainCategories> mainCategoriesList) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.item_sort_type_layout, null);
        kindsPopupWindow = new PopupWindow(view, LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT, false);
        ListView listView = view.findViewById(R.id.prd_popUpList);
        PrdPopUpAdapter adapter = new PrdPopUpAdapter(context, mainCategoriesList);
        listView.setAdapter(adapter);
        kindsPopupWindow.setOutsideTouchable(true);
        kindsPopupWindow.showAsDropDown(linearSortType);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                kindsPopupWindow.dismiss();

                setUpCategoryName(mainCategoriesList.get(i).getMainCatName());
                catSelectionListener.onCategorySelected(i);

            }
        });
    }


    private void setUpPriceSort() {
        if (isPriceLowToHigh) {
            isPriceLowToHigh = false;
            sortPriceTxt.setText("Highest Price");

            sortType = HIGH_TO_LOW;
            pageNumber = 1;
            getProductsList(pageNumber, true);

        } else {
            isPriceLowToHigh = true;
            sortPriceTxt.setText("Lowest Price");


            getProducts();

            /*sortType = LOW_TO_HIGH;
            pageNumber = 1;
            getProductsList(pageNumber, true);*/
        }
    }


    private void setUpCategoryName(String categoryName) {
        txtSortType.setText(categoryName);
    }


    private void showErrorCase(boolean isLoadingForFirstTime) {

        if (isLoadingForFirstTime) {
            if (productsLinear.getVisibility() == View.VISIBLE) {
                productsLinear.setVisibility(View.GONE);
            }

            if (emptyViewRelative.getVisibility() == View.GONE) {
                emptyViewRelative.setVisibility(View.VISIBLE);
            }
        } else {
            hideBottomProgressBar();
        }
    }

    private void hideBottomProgressBar() {
        if (bottomProgressBar.getVisibility() == View.VISIBLE) {
            bottomProgressBar.setVisibility(View.GONE);
        }
    }

    private void resetView(boolean isLoadingForFirstTime) {

        if (isLoadingForFirstTime) {
            if (productsLinear.getVisibility() == View.GONE) {
                productsLinear.setVisibility(View.VISIBLE);
            }

            if (emptyViewRelative.getVisibility() == View.VISIBLE) {
                emptyViewRelative.setVisibility(View.GONE);
            }
        }
    }


    //========wishlist add / remove========================


    private void addProductToWishList(final boolean state, final SparkButton wishListButton, final String productId, String userId) {
        try {
            progressDialog = MyUtils.showProgressLoader(context, state ? "Adding to WishList" : "Removing from WishList");
            Call<String> call = getWishListCall(state, productId, userId);
            call.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    MyUtils.dismissProgressLoader(progressDialog);
                    try {
                        if (response != null) {
                            Log.e("wishlist", "resp:" + response.body());


                            Type type = new TypeToken<WishList>() {
                            }.getType();
                            Gson gson = new GsonBuilder().serializeNulls().create();
                            WishList wishList = gson.fromJson(response.body(), type);

                            if (wishList != null) {

                                String status = wishList.getStatus();

                                if (MyUtils.checkStringValue(status)) {
                                    if (status.equalsIgnoreCase(Constants.SUCCESS)) {

                                        String message = wishList.getMessage();

                                        MyUtils.ShowLongToast(context, "" + message);

                                        /*if (state) {
                                            if (!ProductMainActivity.wishListIdsArrayList.contains(productId)) {
                                                ProductMainActivity.wishListIdsArrayList.add(productId);
                                            }
                                        } else {
                                            if (ProductMainActivity.wishListIdsArrayList.contains(productId)) {
                                                ProductMainActivity.wishListIdsArrayList.remove(productId);
                                            }
                                        }*/


                                    } else if (status.equalsIgnoreCase(Constants.ERROR)) {

                                        String error = wishList.getMessage();

                                        if (MyUtils.checkStringValue(error)) {

                                            MyUtils.ShowLongToast(context, "" + error);

                                            if (!error.equalsIgnoreCase("exists")) {
                                                disableWishListButton(wishListButton);
                                            }
                                        } else {
                                            disableWishListButton(wishListButton);
                                        }

                                    }
                                } else {
                                    disableWishListButton(wishListButton);
                                }
                            }
                        }
                    } catch (Exception e) {
                        Log.e("exception33", ":" + e.getMessage());
                        disableWishListButton(wishListButton);
                    }
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    Log.e("exception11", ":" + t.getMessage());
                    MyUtils.dismissProgressLoader(progressDialog);
                    disableWishListButton(wishListButton);
                }
            });
        } catch (Exception e) {
            MyUtils.dismissProgressLoader(progressDialog);
            disableWishListButton(wishListButton);
        }
    }

    private Call getWishListCall(boolean status, String productId, String userId) {
        ApiInterface apiService = HttpRequest.getScalarsInstance().create(ApiInterface.class);
        if (status) {
            return apiService.addToWishList(JsonInput.wishListInput(userId, productId));
        } else {
            return apiService.removeFromWishList(JsonInput.wishListInput(userId, productId));
        }
    }

    private void disableWishListButton(SparkButton wishListButton) {
        if (wishListButton.isChecked()) {
            wishListButton.setChecked(false);
        }
    }

    private void intentToLoginPage() {
        Intent intent = new Intent(context, LoginActivity.class);
        startActivityForResult(intent, Constants.LOGIN_CODE);
        MyUtils.openOverrideAnimation(false, getActivity());
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constants.LOGIN_CODE) {
            if (resultCode == Constants.LOGIN_SUCCESS) {
                showWishListEnabledProducts();
            }
        }
    }


}
