package com.bestweb.custlr.fragments.mycart;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.bestweb.custlr.R;
import com.bestweb.custlr.utils.MyUtils;

public class Payment extends Fragment {

    private TextView txtHeader;
    private LinearLayout linearNext, linearBack, linearBorderSeanang, linearBorderPaypal, linearBorderBank;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_payment, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        init();
        listener();


    }

    private void init() {

        linearBack = getActivity().findViewById(R.id.fp_linearBack);
        linearNext = getActivity().findViewById(R.id.fp_linearNext);
        linearBorderSeanang = getActivity().findViewById(R.id.fp_linear_senang_border);
        linearBorderPaypal = getActivity().findViewById(R.id.fp_linear_paypal_border);
        linearBorderBank = getActivity().findViewById(R.id.fp_linear_bank_border);

        txtHeader = getActivity().findViewById(R.id.cart_txtHeader);
        txtHeader.setText("PAYMENT");


    }

    private void listener() {


        linearBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MyUtils.ReplaceFragmentClose(getActivity(), new Delivery());
            }
        });

        linearNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MyUtils.ReplaceFragmentOpen(getActivity(), new Order());

            }
        });

        linearBorderSeanang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                linearBorderSeanang.setBackgroundResource(R.drawable.payment_border);
                linearBorderPaypal.setBackground(null);
                linearBorderBank.setBackground(null);

            }
        });

        linearBorderPaypal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                linearBorderPaypal.setBackgroundResource(R.drawable.payment_border);
                linearBorderSeanang.setBackground(null);
                linearBorderBank.setBackground(null);

            }
        });

        linearBorderBank.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                linearBorderBank.setBackgroundResource(R.drawable.payment_border);
                linearBorderPaypal.setBackground(null);
                linearBorderSeanang.setBackground(null);


            }
        });
    }


}
