package com.bestweb.custlr.fragments.bottom_sheet_dialog;

import android.app.Dialog;
import android.util.Log;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;

import com.bestweb.custlr.R;
import com.bestweb.custlr.adapters.CustomExpandableListAdapter;
import com.bestweb.custlr.models.StyleModel;
import com.bestweb.custlr.utils.MyUtils;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import java.lang.reflect.Field;
import java.util.ArrayList;

public class BottomSheetExpendable extends BottomSheetDialogFragment {

    ExpandableListView expandableListView;
    CustomExpandableListAdapter customExpandableListAdapter;
    private ImageView imgClose;
    private LinearLayout saveChangesLinear;

    private int lastExpandedPosition = -1;

    private ArrayList<StyleModel> styleModelArrayList = new ArrayList<>();

    public BottomSheetExpendable(ArrayList<StyleModel> styleModelArrayList) {
        this.styleModelArrayList = styleModelArrayList;
    }

    @Override
    public void setupDialog(Dialog dialog, int style) {
        BottomSheetDialog bottomSheetDialog = (BottomSheetDialog) dialog;
        bottomSheetDialog.setContentView(R.layout.fragment_bottom_sheet_expendable);

        try {
            Field behaviorField = bottomSheetDialog.getClass().getDeclaredField("behavior");
            behaviorField.setAccessible(true);
            final BottomSheetBehavior behavior = (BottomSheetBehavior) behaviorField.get(bottomSheetDialog);

            expandableListView = bottomSheetDialog.findViewById(R.id.fbe_expandableListView);
            imgClose = bottomSheetDialog.findViewById(R.id.fbe_imgClose);
            saveChangesLinear = bottomSheetDialog.findViewById(R.id.fbs_linearBottom);

            if (styleModelArrayList.size() > 0) {
                customExpandableListAdapter = new CustomExpandableListAdapter(getActivity(), styleModelArrayList);
                expandableListView.setAdapter(customExpandableListAdapter);


                expandableListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
                    @Override
                    public void onGroupExpand(int groupPosition) {

                        if (lastExpandedPosition != -1 && groupPosition != lastExpandedPosition) {
                            expandableListView.collapseGroup(lastExpandedPosition);
                        }
                        lastExpandedPosition = groupPosition;
                    }
                });


                expandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {

                    public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {


                        if (isSelectedChildCleared(groupPosition)) {

                            styleModelArrayList.get(groupPosition).getValue().get(childPosition).setSelected(true);
                            customExpandableListAdapter.notifyDataSetChanged();
                           /* String name = styleModelArrayList.get(groupPosition).getValue().get(childPosition).getDetails();
                            MyUtils.ShowLongToast(getActivity(),":"+name);*/

                            //parent.collapseGroup(groupPosition);

                            /*int nextGroupPosition = groupPosition + 1;
                            if (styleModelArrayList.size() != nextGroupPosition) {
                                expandableListView.expandGroup(nextGroupPosition);
                            }*/

                        }

                        return false;
                    }
                });
            }


            imgClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dismiss();
                }
            });

            saveChangesLinear.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dismiss();
                    //TODO SAVE ALL THE SELECTED VALUES
                }
            });


            behavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {

                @Override
                public void onStateChanged(@NonNull View bottomSheet, int newState) {
                    if (newState == BottomSheetBehavior.STATE_DRAGGING) {
                        behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                    }
                }

                @Override
                public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                }
            });
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }


    private boolean isSelectedChildCleared(int groupPosition) {
        ArrayList<StyleModel.Value> childArrayList = styleModelArrayList.get(groupPosition).getValue();


        for (int i = 0; i < childArrayList.size(); i++) {

            boolean isAlreadySelected = childArrayList.get(i).isSelected();

            if (isAlreadySelected) {
                styleModelArrayList.get(groupPosition).getValue().get(i).setSelected(false);

                Log.e("isAlreadySelected", ":" + "groupPosition:" + groupPosition + " -- " + "childPosition:" + i);

                return true;
            }
        }
        return true;
    }


}
