package com.bestweb.custlr.fragments.bottom_sheet_dialog;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bestweb.custlr.R;
import com.bestweb.custlr.adapters.MeasureSizeInchAdapter;
import com.bestweb.custlr.interfaces.OnSizeMeasure;
import com.bestweb.custlr.models.MeasureSizeInch;
import com.bestweb.custlr.utils.Constants;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import java.util.ArrayList;

public class BottomSheetSizeDialog /*extends BottomSheetDialogFragment*/ {

    /*private String isFrom;
    private OnSizeMeasure onSizeMeasureBody;

    public BottomSheetSizeDialog(String isFrom,OnSizeMeasure onSizeMeasureBody) {
        this.isFrom = isFrom;
        this.onSizeMeasureBody = onSizeMeasureBody;
    }

    private TextView countTxt;
    private RecyclerView recyclerView;
    private LinearLayout linearNext, linearBack;
    private ArrayList<MeasureSizeInch> arrayList = new ArrayList<>();
    private ImageView imgClose;
    private int startPosition = 0;

    private static final int startLimit = 0;
    private int endLimit;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_bottom_sheet_dialog, container, false);

        countTxt = view.findViewById(R.id.fbs_countTxt);
        recyclerView = view.findViewById(R.id.fbs_recycler);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));

        linearNext = view.findViewById(R.id.fbs_linearNext);
        linearBack = view.findViewById(R.id.fbs_linearBack);
        imgClose = view.findViewById(R.id.fbs_imgClose);


        arrayList.add(new MeasureSizeInch(R.drawable.measure_shoulder_size, "Shoulder Size", "Please enter your Shoulder Size(Inch)", "Enter shoulder size(Inch)", 0));
        arrayList.add(new MeasureSizeInch(R.drawable.measure_chest_size, "Chest Size", "Please enter your Chest Size(Inch)", "Enter chest size(Inch)", 1));
        arrayList.add(new MeasureSizeInch(R.drawable.measure_arm_size, "Arm Size", "Please enter your Arm Size(Inch)", "Enter arm size(Inch)", 2));
        arrayList.add(new MeasureSizeInch(R.drawable.measure_arm_length, "Arm Length", "Please enter your Arm Length(Inch)", "Enter arm length(Inch)", 3));
        arrayList.add(new MeasureSizeInch(R.drawable.measure_waist_size, "Waist Size", "Please enter your Waist Size(Inch)", "Enter waist size(Inch)", 4));

        endLimit = arrayList.size() > 0 ? (arrayList.size() - 1) : 0;

         OnSizeMeasure onSizeMeasure = new OnSizeMeasure() {
            @Override
            public void onSizeMeasure(int index, String value) {

                startPosition = index;
                populateCountTxt(index);
                onSizeMeasureBody.onSizeMeasure(index,value);
            }
        };

        MeasureSizeInchAdapter adapter = new MeasureSizeInchAdapter(getActivity(), arrayList,onSizeMeasure);
        recyclerView.setAdapter(adapter);

        if (isFrom.equalsIgnoreCase(Constants.SHOULDER)) {
            startPosition = 0;
        } else if (isFrom.equalsIgnoreCase(Constants.CHEST)) {
            startPosition = 1;
        } else if (isFrom.equalsIgnoreCase(Constants.ARM)) {
            startPosition = 2;
        } else if (isFrom.equalsIgnoreCase(Constants.ARM_LENGTH)) {
            startPosition = 3;
        } else if (isFrom.equalsIgnoreCase(Constants.WAIST)) {
            startPosition = 4;
        }
        recyclerView.scrollToPosition(startPosition);
        populateCountTxt(startPosition);

        linearNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (startPosition < endLimit) {
                    startPosition = startPosition + 1;
                    populateCountTxt(startPosition);
                    recyclerView.scrollToPosition(startPosition);

                } else {
                    new BottomSheetExpendable().show(getActivity().getSupportFragmentManager(), "tag");
                    dismiss();
                }

            }
        });
        linearBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (startPosition > startLimit) {
                    startPosition = startPosition - 1;
                    populateCountTxt(startPosition);
                    recyclerView.scrollToPosition(startPosition);
                }

            }
        });

        recyclerView.addOnItemTouchListener(new RecyclerView.SimpleOnItemTouchListener() {
            @Override
            public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
                return rv.getScrollState() == RecyclerView.SCROLL_STATE_DRAGGING;
            }
        });

        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });


        return view;

    }

    private void populateCountTxt(int position) {
        countTxt.setText("step " + (position + 1) + "/" + (endLimit + 1));
        if (position == startLimit) {
            if (linearBack.getVisibility() == View.VISIBLE) {
                linearBack.setVisibility(View.INVISIBLE);
            }
        } else {
            if (linearBack.getVisibility() == View.INVISIBLE) {
                linearBack.setVisibility(View.VISIBLE);
            }
        }
    }*/


    /*private void populateCountTxt(int position) {
        countTxt.setText("step " + (position + 1) + "/" + (endLimit + 1));
        if (position == startLimit) {
            if (linearBack.getVisibility() == View.VISIBLE) {
                linearBack.setVisibility(View.INVISIBLE);
            }
        } else {
            if (linearBack.getVisibility() == View.INVISIBLE) {
                linearBack.setVisibility(View.VISIBLE);
            }
        }
    }*/


}