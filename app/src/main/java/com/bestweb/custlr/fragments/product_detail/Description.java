package com.bestweb.custlr.fragments.product_detail;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.bestweb.custlr.R;
import com.bestweb.custlr.utils.Constants;
import com.bestweb.custlr.utils.MyUtils;

import org.sufficientlysecure.htmltextview.HtmlHttpImageGetter;
import org.sufficientlysecure.htmltextview.HtmlTextView;

public class Description extends Fragment {

    private Context context;
    private RelativeLayout whatsAppRelative;
    private HtmlTextView descriptionHtmlTxt;

    private String descriptionContent;
    private String permaLink;

    public Description(String descriptionContent, String permaLink) {
        this.descriptionContent = descriptionContent;
        this.permaLink = permaLink;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_description, container, false);
        context = getActivity();

        try {
            descriptionHtmlTxt = view.findViewById(R.id.fd_descriptionTxt);
            whatsAppRelative = view.findViewById(R.id.fd_whatsAppRelative);

            if (MyUtils.checkStringValue(descriptionContent)) {
                descriptionHtmlTxt.setHtml(descriptionContent, new HtmlHttpImageGetter(descriptionHtmlTxt));
            }

            whatsAppRelative.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    MyUtils.openWhatsApp(getActivity(), Constants.WHATSAPP_NUMBER, (MyUtils.checkStringValue(permaLink) ? permaLink : ""));
                }
            });


        } catch (Exception e) {
            e.printStackTrace();
        }

        return view;
    }

}
