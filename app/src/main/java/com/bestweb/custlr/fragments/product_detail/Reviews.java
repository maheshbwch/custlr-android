package com.bestweb.custlr.fragments.product_detail;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.bestweb.custlr.R;
import com.bestweb.custlr.activities.products.AddReviewActivity;
import com.bestweb.custlr.adapters.ReviewsAdapter;
import com.bestweb.custlr.models.ReviewModel;
import com.bestweb.custlr.utils.Constants;
import com.bestweb.custlr.utils.MyUtils;
import com.bestweb.custlr.web_service.ApiInterface;
import com.bestweb.custlr.web_service.HttpRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Reviews extends Fragment {

    private Context context;
    private LinearLayout emptyReviewLinear;
    private Button writeReviewButton;
    private ProgressBar progressBar;
    private RecyclerView recyclerView;

    private ArrayList<ReviewModel> reviewsArrayList = new ArrayList<>();


    private Bundle bundle;

    public Reviews(Bundle bundle) {
        this.bundle = bundle;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_reviews, container, false);
        context = getActivity();

        progressBar = view.findViewById(R.id.fr_progressBar);
        recyclerView = view.findViewById(R.id.fr_reviewRecycler);
        emptyReviewLinear = view.findViewById(R.id.fr_emptyReviewLinear);
        writeReviewButton = view.findViewById(R.id.fr_writeReviewButton);

        String productId = bundle.getString(Constants.PRODUCT_ID);
        String productName = bundle.getString(Constants.PRODUCT_NAME);
        String productPrice = bundle.getString(Constants.PRODUCT_PRICE);
        String imgUrl = bundle.getString(Constants.PRODUCT_IMAGE_URL);

        Log.e("bundle", "value:" + productId);

        init(productId);

        return view;
    }

    private void init(final String productId) {
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(linearLayoutManager);

        getReviewsList(productId);

        writeReviewButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(context, AddReviewActivity.class);
                intent.putExtras(bundle);
                startActivity(intent);
                MyUtils.openOverrideAnimation(true, getActivity());
            }
        });

    }


    private void getReviewsList(String productId) {

        Log.e("productId", ":" + productId);

        try {
            progressBar.setVisibility(View.VISIBLE);
            ApiInterface apiService = HttpRequest.getScalarsInstance().create(ApiInterface.class);
            //wp-json/wc/v1/products/4735/reviews
            String url = "wp-json/wc/v1/products/" + productId + "/reviews";
            Call<String> call = apiService.getReviewsList(url);
            call.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    progressBar.setVisibility(View.GONE);
                    try {

                        String jsonResponse = response.body();

                        Log.e("jsonResponse", ":" + jsonResponse);

                        if (jsonResponse != null) {

                            Type listType = new TypeToken<ArrayList<ReviewModel>>() {
                            }.getType();
                            Gson gson = new GsonBuilder().serializeNulls().create();
                            reviewsArrayList = gson.fromJson(jsonResponse, listType);

                            if (reviewsArrayList.size() > 0) {

                                ReviewsAdapter reviewsAdapter = new ReviewsAdapter(context, reviewsArrayList);
                                recyclerView.setAdapter(reviewsAdapter);


                            } else {
                                showErrorCase();
                            }

                        }
                    } catch (Exception e) {
                        Log.e("exception33", ":" + e.getMessage());
                        showErrorCase();
                    }
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    Log.e("exception11", ":" + t.getMessage());
                    showErrorCase();
                    progressBar.setVisibility(View.GONE);
                }
            });
        } catch (Exception e) {
            Log.e("exception00", ":" + e.getMessage());
            progressBar.setVisibility(View.GONE);
            showErrorCase();
        }
    }

    private void showErrorCase() {
        emptyReviewLinear.setVisibility(View.VISIBLE);
    }

}
