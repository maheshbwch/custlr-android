package com.bestweb.custlr.adapters;


import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bestweb.custlr.R;
import com.bestweb.custlr.models.StyleModel;
import com.bestweb.custlr.utils.MyUtils;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class CustomExpandableListAdapter extends BaseExpandableListAdapter {

    private Context context;
    ArrayList<StyleModel> styleModelArrayList = new ArrayList<>();


    public CustomExpandableListAdapter(Context context, ArrayList<StyleModel> styleModelArrayList) {
        this.context = context;
        this.styleModelArrayList = styleModelArrayList;
    }

    @Override
    public Object getChild(int listPosition, int expandedListPosition) {
        return this.styleModelArrayList.get(listPosition).getValue().get(expandedListPosition);
    }

    @Override
    public long getChildId(int listPosition, int expandedListPosition) {
        return expandedListPosition;
    }

    @Override
    public View getChildView(int listPosition, final int expandedListPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        final String expandedListText = styleModelArrayList.get(listPosition).getValue().get(expandedListPosition).getDetails();
        final String iconUrl = styleModelArrayList.get(listPosition).getValue().get(expandedListPosition).getImage();
        final boolean isSelected = styleModelArrayList.get(listPosition).getValue().get(expandedListPosition).isSelected();

        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.list_item, null);
        }
        LinearLayout childLinear = convertView.findViewById(R.id.li_childLinear);
        TextView expandedListTextView = convertView.findViewById(R.id.li_listTitle);
        ImageView iconImage = convertView.findViewById(R.id.li_imgSide);

        expandedListTextView.setText(expandedListText);
        if (MyUtils.checkStringValue(iconUrl)) {
            Glide.with(context).load(iconUrl).into(iconImage);
        }

        if (isSelected) {
            childLinear.setBackgroundColor(context.getResources().getColor(R.color.silver));
        } else {
            childLinear.setBackgroundColor(context.getResources().getColor(R.color.white));
        }

        return convertView;
    }

    @Override
    public int getChildrenCount(int listPosition) {
        return styleModelArrayList.get(listPosition).getValue().size();
        //return this.expandableListDetail.get(this.expandableListTitle.get(listPosition)).size();
    }

    @Override
    public Object getGroup(int listPosition) {
        return styleModelArrayList.get(listPosition);
    }

    @Override
    public int getGroupCount() {
        return styleModelArrayList.size();
    }

    @Override
    public long getGroupId(int listPosition) {
        return listPosition;
    }

    @Override
    public View getGroupView(int listPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        String listTitle = styleModelArrayList.get(listPosition).getName();

        ArrayList<StyleModel.Value> childArrayList = styleModelArrayList.get(listPosition).getValue();


        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.list_group, null);
        }
        TextView listTitleTextView = (TextView) convertView.findViewById(R.id.lg_titleTxt);
        ImageView imageView = convertView.findViewById(R.id.lg_imgSide);
        ImageView arrowImage = convertView.findViewById(R.id.lg_arrowImage);
        LinearLayout linearLayout = convertView.findViewById(R.id.lg_linearTop);

        listTitleTextView.setTypeface(null, Typeface.BOLD);
        listTitleTextView.setText(listTitle);

        if (childArrayList != null && childArrayList.size() > 0) {
            String defaultIconUrl = childArrayList.get(0).getImage();

            if (MyUtils.checkStringValue(defaultIconUrl)) {
                Glide.with(context).load(defaultIconUrl).into(imageView);
            }
        }


        if (isExpanded) {
            arrowImage.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_expand));
            linearLayout.setBackgroundColor(context.getResources().getColor(R.color.color_prdDetail));
            listTitleTextView.setTextColor(context.getResources().getColor(R.color.white));
        } else {
            arrowImage.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_expand_less));
            linearLayout.setBackgroundColor(context.getResources().getColor(R.color.white));
            listTitleTextView.setTextColor(context.getResources().getColor(R.color.black));
        }


        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int listPosition, int expandedListPosition) {
        return true;
    }
}