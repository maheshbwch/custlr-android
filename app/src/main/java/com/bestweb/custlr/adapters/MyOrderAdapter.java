package com.bestweb.custlr.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bestweb.custlr.R;
import com.bestweb.custlr.interfaces.RecyclerClickListener;
import com.bestweb.custlr.models.Orders;
import com.bestweb.custlr.utils.MyUtils;

import java.util.ArrayList;

public class MyOrderAdapter extends RecyclerView.Adapter<MyOrderAdapter.ViewHolder> {

    private Context context;
    private ArrayList<Orders> ordersArrayList = new ArrayList<>();
    private RecyclerClickListener recyclerClickListener = null;

    public MyOrderAdapter(Context context, ArrayList<Orders> ordersArrayList, RecyclerClickListener recyclerClickListener) {
        this.context = context;
        this.ordersArrayList = ordersArrayList;
        this.recyclerClickListener = recyclerClickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.item_my_orders, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        String orderNumber = "" + ordersArrayList.get(position).getNumber();
        String orderDate = "" + ordersArrayList.get(position).getDateCreated();
        String orderStatus = "" + ordersArrayList.get(position).getStatus();
        String paymentMethod = "" + ordersArrayList.get(position).getPaymentMethodTitle();
        String currency = "" + ordersArrayList.get(position).getCurrency();
        String orderTotal = "" + ordersArrayList.get(position).getTotal();

        holder.orderNumberTxt.setText(MyUtils.checkStringValue(orderNumber) ? ("#" + orderNumber) : "-");
        holder.orderDateTxt.setText(MyUtils.checkStringValue(orderDate) ? (MyUtils.getFormattedDate(orderDate)) : "-");
        holder.orderStatusTxt.setText(MyUtils.checkStringValue(orderStatus) ? (orderStatus) : "-");
        holder.orderPaymentMethodTxt.setText(MyUtils.checkStringValue(paymentMethod) ? (paymentMethod) : "-");
        holder.orderTotalTxt.setText(MyUtils.checkStringValue(orderTotal) ? (MyUtils.checkStringValue(currency) ? (currency + " " + orderTotal) : orderTotal) : "-");
    }

    @Override
    public int getItemCount() {
        return ordersArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView orderNumberTxt, orderDateTxt, orderStatusTxt, orderPaymentMethodTxt, orderTotalTxt;
        public LinearLayout showDetailsLinear;

        public ViewHolder(View itemView) {
            super(itemView);
            this.orderNumberTxt = itemView.findViewById(R.id.imo_orderNumberTxt);
            this.orderDateTxt = itemView.findViewById(R.id.imo_orderDateTxt);
            this.orderStatusTxt = itemView.findViewById(R.id.imo_orderStatusTxt);
            this.orderPaymentMethodTxt = itemView.findViewById(R.id.imo_orderPaymentMethodTxt);
            this.orderTotalTxt = itemView.findViewById(R.id.imo_orderTotalTxt);
            this.showDetailsLinear = itemView.findViewById(R.id.imo_showDetailsLinear);


        }
    }
}
