package com.bestweb.custlr.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bestweb.custlr.R;
import com.bestweb.custlr.interfaces.RecyclerClickListener;
import com.bestweb.custlr.models.Search;
import com.bestweb.custlr.utils.MyUtils;
import com.bumptech.glide.Glide;
import com.github.siyamed.shapeimageview.mask.PorterShapeImageView;

import java.util.ArrayList;

public class SearchHomeAdapter extends RecyclerView.Adapter<SearchHomeAdapter.ViewHolder> {

    private ArrayList<Search> arrayList;
    private Context context;
    private RecyclerClickListener recyclerClickListener;

    public SearchHomeAdapter(ArrayList<Search> arrayList, Context context, RecyclerClickListener recyclerClickListener) {
        this.arrayList = arrayList;
        this.context = context;
        this.recyclerClickListener = recyclerClickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.item_layout_filter, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        String prdName = arrayList.get(position).getName();
        String prdImageUrl = arrayList.get(position).getImage();


        holder.prdNameTxt.setText(MyUtils.checkStringValue(prdName) ? prdName : "-");

        Glide.with(context).load(prdImageUrl).into(holder.imgProduct);

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView prdNameTxt;
        private PorterShapeImageView imgProduct;

        public ViewHolder(View itemView) {
            super(itemView);
            this.imgProduct = itemView.findViewById(R.id.ilf_productImage);
            this.prdNameTxt = itemView.findViewById(R.id.ilf_productNameTxt);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    recyclerClickListener.onItemClicked(getAdapterPosition());
                }
            });

        }
    }
}
