package com.bestweb.custlr.adapters;

import android.content.Context;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bestweb.custlr.R;
import com.bestweb.custlr.interfaces.OnChooseSize;
import com.bestweb.custlr.utils.ToggleImageButton;

import java.util.ArrayList;

public class StandardSizeAdapter extends RecyclerView.Adapter<StandardSizeAdapter.ViewHolder> {

    private ArrayList<String> arrayList;
    private Context context;
    private OnChooseSize onChooseSize;
    private int focusedItem = -1;

    public StandardSizeAdapter(ArrayList<String> listdata, Context context, OnChooseSize onChooseSize) {
        this.arrayList = listdata;
        this.context = context;
        this.onChooseSize = onChooseSize;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.item_standard_size, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        holder.txtSizeName.setText(arrayList.get(position));


        boolean isItemSelected = (focusedItem == position);

        holder.itemView.setSelected(isItemSelected);
        if (isItemSelected) {
            holder.toggleImage.setChecked(true);
            holder.toggleImage.setEnabled(false);

        } else {
            holder.toggleImage.setChecked(false);
            holder.toggleImage.setEnabled(false);
        }


    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView txtSizeName;
        private ToggleImageButton toggleImage;

        public ViewHolder(View itemView) {
            super(itemView);

            this.txtSizeName = itemView.findViewById(R.id.is_txtSize);
            this.toggleImage = itemView.findViewById(R.id.sdd_toggleImg1);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    notifyItemRangeChanged(0, arrayList.size());
                    notifyItemChanged(focusedItem);
                    focusedItem = getLayoutPosition();
                    notifyItemChanged(focusedItem);
                    onChooseSize.onClick(getAdapterPosition());


                }
            });


        /* toggleImage.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    if(b){
                        toggleImage.setChecked(true);
                    }else {
                        toggleImage.setChecked(false);
                    }
                }
            });*/

        }
    }

    //------For Hihlighting the item layout of recyclerview------------------------------------------------------
    @Override
    public void onAttachedToRecyclerView(final RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        recyclerView.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                RecyclerView.LayoutManager lm = recyclerView.getLayoutManager();
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    if (keyCode == KeyEvent.KEYCODE_DPAD_DOWN) {
                        return tryMoveSelection(lm, 1);
                    } else if (keyCode == KeyEvent.KEYCODE_DPAD_UP) {
                        return tryMoveSelection(lm, -1);
                    }
                }
                return false;
            }
        });
    }

    private boolean tryMoveSelection(RecyclerView.LayoutManager lm, int direction) {
        int tryFocusItem = focusedItem + direction;
        if (tryFocusItem >= 0 && tryFocusItem < getItemCount()) {
            notifyItemChanged(focusedItem);
            focusedItem = tryFocusItem;
            notifyItemChanged(focusedItem);
            lm.scrollToPosition(focusedItem);
            return true;
        }
        return false;
    }

}
