package com.bestweb.custlr.adapters;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.bestweb.custlr.fragments.product_detail.Description;
import com.bestweb.custlr.fragments.product_detail.Reviews;

public class ProductDetailTabAdapter extends FragmentStatePagerAdapter {

    private int mNumOfTabs;
    private String descriptionContent,permaLink;
    private Bundle bundle;

    public ProductDetailTabAdapter(FragmentManager fm, int NoofTabs, String descriptionContent,String permaLink, Bundle bundle) {
        super(fm);
        this.mNumOfTabs = NoofTabs;
        this.permaLink = permaLink;
        this.descriptionContent = descriptionContent;
        this.bundle = bundle;
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                Description description = new Description(descriptionContent,permaLink);
                return description;
            case 1:
                Reviews reviews = new Reviews(bundle);
                return reviews;
            default:
                return null;
        }
    }
}
