package com.bestweb.custlr.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bestweb.custlr.R;
import com.bestweb.custlr.models.ReviewModel;
import com.bestweb.custlr.utils.MyUtils;

import java.util.ArrayList;

public class ReviewsAdapter extends RecyclerView.Adapter<ReviewsAdapter.ViewHolder> {

    private ArrayList<ReviewModel> reviewsArrayList;
    private Context context;

    public ReviewsAdapter(Context context, ArrayList<ReviewModel> reviewsArrayList) {
        this.context = context;
        this.reviewsArrayList = reviewsArrayList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.reviews_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        String ratingCount = "" + reviewsArrayList.get(position).getRating();
        String name = "" + reviewsArrayList.get(position).getName();
        String comments = "" + reviewsArrayList.get(position).getReview();
        String date = "" + reviewsArrayList.get(position).getDateCreated();

        holder.ratingCountTxt.setText(MyUtils.checkStringValue(ratingCount) ? ratingCount : "");
        holder.userNameTxt.setText(MyUtils.checkStringValue(name) ? name : "-");
        holder.commentsTxt.setText(MyUtils.checkStringValue(comments) ? comments : "-");
        holder.dateTxt.setText(MyUtils.checkStringValue(date) ? (MyUtils.getFormattedDate(date)) : "-");


    }


    @Override
    public int getItemCount() {
        return reviewsArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView ratingCountTxt, userNameTxt, commentsTxt, dateTxt;

        public ViewHolder(View itemView) {
            super(itemView);

            this.ratingCountTxt = itemView.findViewById(R.id.ri_ratingCountTxt);
            this.userNameTxt = itemView.findViewById(R.id.ri_userNameTxt);
            this.commentsTxt = itemView.findViewById(R.id.ri_commentsTxt);
            this.dateTxt = itemView.findViewById(R.id.ri_dateTxt);
        }
    }
}