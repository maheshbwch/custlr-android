package com.bestweb.custlr.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bestweb.custlr.R;
import com.bestweb.custlr.interfaces.CartItemClickListener;
import com.bestweb.custlr.models.Cart;
import com.bestweb.custlr.utils.MyUtils;
import com.bumptech.glide.Glide;
import com.github.siyamed.shapeimageview.mask.PorterShapeImageView;

import java.util.ArrayList;

public class MyCartListAdapter extends RecyclerView.Adapter<MyCartListAdapter.ViewHolder> {

    private Context context;
    private ArrayList<Cart> cartArrayList = new ArrayList<>();
    private CartItemClickListener cartItemClickListener = null;

    public MyCartListAdapter(Context context, ArrayList<Cart> cartArrayList, CartItemClickListener cartItemClickListener) {
        this.context = context;
        this.cartArrayList = cartArrayList;
        this.cartItemClickListener = cartItemClickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.item_layout_cart, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        String productImageUrl = cartArrayList.get(position).getImage();
        String productName = cartArrayList.get(position).getProductName();
        String price = cartArrayList.get(position).getPrice();
        int quantity = cartArrayList.get(position).getQuantity();

        Glide.with(context).load(productImageUrl).into(holder.productImageView);

        holder.productNameTxt.setText(MyUtils.checkStringValue(productName) ? productName : "-");
        holder.productPriceTxt.setText(MyUtils.checkStringValue(price) ? ("RM" + price) : "-");
        setQuantity(holder.quantityTxt, quantity);

    }

    private void setQuantity(TextView textView, int quantity) {
        textView.setText("" + quantity);
    }


    @Override
    public int getItemCount() {
        return cartArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView productNameTxt, productPriceTxt, editTxt, deleteTxt, quantityTxt;
        private PorterShapeImageView productImageView;
        private ImageView decreaseQuantityImg, increaseQuantityImg;

        public ViewHolder(View itemView) {
            super(itemView);

            this.productImageView = itemView.findViewById(R.id.ilc_productImageView);

            this.productNameTxt = itemView.findViewById(R.id.ilc_productNameTxt);
            this.productPriceTxt = itemView.findViewById(R.id.ilc_productPriceTxt);
            this.editTxt = itemView.findViewById(R.id.ilc_editTxt);
            this.deleteTxt = itemView.findViewById(R.id.ilc_deleteTxt);
            this.decreaseQuantityImg = itemView.findViewById(R.id.ilc_decreaseQuantityImg);
            this.quantityTxt = itemView.findViewById(R.id.ilc_quantityTxt);
            this.increaseQuantityImg = itemView.findViewById(R.id.ilc_increaseQuantityImg);


            increaseQuantityImg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    int quntity = Integer.parseInt(quantityTxt.getText().toString());
                    quntity = quntity + 1;

                    setQuantity(quantityTxt, quntity);

                    cartArrayList.get(getAdapterPosition()).setQuantity(quntity);

                }
            });


            decreaseQuantityImg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    int quntity = Integer.parseInt(quantityTxt.getText().toString());
                    quntity = quntity - 1;
                    if (quntity < 1) {
                        quntity = 1;
                    }

                    setQuantity(quantityTxt, quntity);

                    cartArrayList.get(getAdapterPosition()).setQuantity(quntity);

                }
            });


            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    cartItemClickListener.onCartItemClicked(getAdapterPosition());
                }
            });

            editTxt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    cartItemClickListener.onEditClicked();
                }
            });

            deleteTxt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String productId = cartArrayList.get(getAdapterPosition()).getProductId();
                    cartItemClickListener.onDeleteCartItem(getAdapterPosition(), productId);
                }
            });
        }
    }
}
