package com.bestweb.custlr.adapters;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bestweb.custlr.R;
import com.bestweb.custlr.activities.products.ProductDetailActivity;
import com.bestweb.custlr.interfaces.SubCategoryClickListener;
import com.bestweb.custlr.interfaces.WishListListener;
import com.bestweb.custlr.models.Subcategory;
import com.bestweb.custlr.utils.Constants;
import com.bestweb.custlr.utils.MyUtils;
import com.bestweb.custlr.utils.custom_view.spark_button.SparkButton;
import com.bestweb.custlr.utils.custom_view.spark_button.SparkEventListener;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class HomeSubCatAdapter extends RecyclerView.Adapter<HomeSubCatAdapter.MyViewHolder> {

    private ArrayList<Subcategory> arrayList;
    private Context context;
    private WishListListener wishListListener = null;
    private SubCategoryClickListener subCategoryClickListener = null;

    public HomeSubCatAdapter(Context context, ArrayList<Subcategory> arrayList, WishListListener wishListListener, SubCategoryClickListener subCategoryClickListener) {
        this.context = context;
        this.arrayList = arrayList;
        this.wishListListener = wishListListener;
        this.subCategoryClickListener = subCategoryClickListener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_layout_home, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        String name = arrayList.get(position).getProduct_name();
        String imageUrl = arrayList.get(position).getProduct_url();
        String salePrice = "" + arrayList.get(position).getSalePrice();
        String regularPrice = "" + arrayList.get(position).getRegularPrice();
        boolean isWishList = arrayList.get(position).isWishList();

        Glide.with(context).load(imageUrl).into(holder.imgProduct);
        holder.prdNameTxt.setText(name);

        if (MyUtils.checkStringValue(salePrice)) {

            holder.prdNamePrice.setText("RM " + salePrice);

            if (MyUtils.checkStringValue(regularPrice)) {
                holder.prdDiscPrice.setVisibility(View.VISIBLE);
                holder.prdDiscPrice.setText("RM " + regularPrice);
                strikeRegularPrice(holder.prdDiscPrice);
            } else {
                holder.prdDiscPrice.setVisibility(View.GONE);
            }

        } else {
            holder.prdNamePrice.setText(MyUtils.checkStringValue(regularPrice) ? ("RM " + regularPrice) : "");
            holder.prdDiscPrice.setVisibility(View.GONE);
        }


        if (isWishList) {
            holder.wishIcon.setChecked(true);
        } else {
            holder.wishIcon.setChecked(false);
        }


    }


    private void strikeRegularPrice(TextView textView) {
        textView.setPaintFlags(textView.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
    }


    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView prdNameTxt, prdNamePrice, prdDiscPrice;
        public ImageView imgProduct;
        public SparkButton wishIcon;

        public MyViewHolder(View itemView) {
            super(itemView);

            prdNameTxt = itemView.findViewById(R.id.its_prdNameTxt);
            prdNamePrice = itemView.findViewById(R.id.its_prdNamePrice);
            imgProduct = itemView.findViewById(R.id.imgProduct);
            wishIcon = itemView.findViewById(R.id.il_WishIcon);
            prdDiscPrice = itemView.findViewById(R.id.its_txtPrdDisPrice);


            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    String productId = arrayList.get(getAdapterPosition()).getProductId();
                    String productName = arrayList.get(getAdapterPosition()).getProduct_name();
                    String productSalePrice = "" + arrayList.get(getAdapterPosition()).getSalePrice();
                    String productRegularPrice = "" + arrayList.get(getAdapterPosition()).getRegularPrice();

                    if (MyUtils.checkStringValue(productId)) {
                        subCategoryClickListener.onProductClicked(productId, productName, productSalePrice, productRegularPrice);
                    }

                }
            });

            wishIcon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (wishIcon.isChecked()) {
                        wishIcon.setChecked(false);
                        wishIcon.playAnimation();
                    } else {
                        wishIcon.setChecked(true);
                        wishIcon.playAnimation();
                    }
                }
            });

            wishIcon.setEventListener(new SparkEventListener() {
                @Override
                public void onEvent(ImageView button, boolean buttonState) {

                }

                @Override
                public void onEventAnimationEnd(ImageView button, boolean buttonState) {

                }

                @Override
                public void onEventAnimationStart(ImageView button, boolean buttonState) {
                    String productId = arrayList.get(getAdapterPosition()).getProductId();
                    wishListListener.onWishListListen(wishIcon, productId);
                }
            });


        }

    }
}