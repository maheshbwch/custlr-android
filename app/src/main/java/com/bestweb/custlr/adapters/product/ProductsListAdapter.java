package com.bestweb.custlr.adapters.product;

import android.content.Context;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bestweb.custlr.R;
import com.bestweb.custlr.activities.products.ProductMainActivity;
import com.bestweb.custlr.interfaces.RecyclerClickListener;
import com.bestweb.custlr.interfaces.WishListListener;
import com.bestweb.custlr.models.Products;
import com.bestweb.custlr.utils.MyUtils;
import com.bestweb.custlr.utils.custom_view.spark_button.SparkButton;
import com.bestweb.custlr.utils.custom_view.spark_button.SparkEventListener;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class ProductsListAdapter extends RecyclerView.Adapter<ProductsListAdapter.ViewHolder> {


    private Context context;
    private ArrayList<Products> productsArrayList;
    private ArrayList<String> wishListIdsArrayList;
    private RecyclerClickListener recyclerClickListener;
    private WishListListener wishListListener;

    public ProductsListAdapter(Context context, ArrayList<Products> productsArrayList, ArrayList<String> wishListIdsArrayList, RecyclerClickListener recyclerClickListener, WishListListener wishListListener) {
        this.context = context;
        this.productsArrayList = productsArrayList;
        this.wishListIdsArrayList = wishListIdsArrayList;
        this.recyclerClickListener = recyclerClickListener;
        this.wishListListener = wishListListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.item_layout_product, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        String productId = "" + productsArrayList.get(position).getId();
        String productImageUrl = "" + productsArrayList.get(position).getAppThumbnail();
        String productName = "" + productsArrayList.get(position).getName();
        String salePrice = "" + productsArrayList.get(position).getSalePrice();
        String regularPrice = "" + productsArrayList.get(position).getRegularPrice();
        //boolean isWishList = productsArrayList.get(position).isWishList();
        boolean isWishList = wishListIdsArrayList.size() > 0 && wishListIdsArrayList.contains(productId);

        Glide.with(context).load(productImageUrl).into(holder.prdImage);

        holder.prdNameTxt.setText(productName);


        if (MyUtils.checkStringValue(salePrice)) {

            holder.prdSalePriceTxt.setText("RM " + salePrice);

            if (MyUtils.checkStringValue(regularPrice)) {
                holder.prdRegularPriceTxt.setVisibility(View.VISIBLE);
                holder.prdRegularPriceTxt.setText("RM " + regularPrice);
                strikeRegularPrice(holder.prdRegularPriceTxt);
            } else {
                holder.prdRegularPriceTxt.setVisibility(View.GONE);
            }


        } else {
            holder.prdSalePriceTxt.setText(MyUtils.checkStringValue(regularPrice) ? ("RM " + regularPrice) : "");
            holder.prdRegularPriceTxt.setVisibility(View.GONE);
        }


        if (isWishList) {
            holder.wishListButton.setChecked(true);
        } else {
            holder.wishListButton.setChecked(false);
        }

    }

    private void strikeRegularPrice(TextView textView) {
        textView.setPaintFlags(textView.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
    }


    @Override
    public int getItemCount() {
        return productsArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private SparkButton wishListButton;
        private ImageView prdImage;
        public TextView prdNameTxt, prdSalePriceTxt, prdRegularPriceTxt;


        public ViewHolder(View itemView) {
            super(itemView);

            this.prdImage = itemView.findViewById(R.id.ilp_prdImage);
            this.wishListButton = itemView.findViewById(R.id.ilp_prdWishListButton);
            this.prdNameTxt = itemView.findViewById(R.id.ilp_prdNameTxt);
            this.prdSalePriceTxt = itemView.findViewById(R.id.ilp_prdSalePriceTxt);
            this.prdRegularPriceTxt = itemView.findViewById(R.id.ilp_prdRegularPriceTxt);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    recyclerClickListener.onItemClicked(getAdapterPosition());
                }
            });

            wishListButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (wishListButton.isChecked()) {
                        wishListButton.setChecked(false);
                        wishListButton.playAnimation();
                    } else {
                        wishListButton.setChecked(true);
                        wishListButton.playAnimation();
                    }
                }
            });

            wishListButton.setEventListener(new SparkEventListener() {
                @Override
                public void onEvent(ImageView button, boolean buttonState) {

                }

                @Override
                public void onEventAnimationEnd(ImageView button, boolean buttonState) {

                }

                @Override
                public void onEventAnimationStart(ImageView button, boolean buttonState) {
                    String productId = "" + productsArrayList.get(getAdapterPosition()).getId();
                    wishListListener.onWishListListen(wishListButton, productId);
                }
            });

        }


    }
}