package com.bestweb.custlr.adapters;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.viewpager.widget.PagerAdapter;

import com.bestweb.custlr.R;
import com.bestweb.custlr.interfaces.OnSizeMeasure;
import com.bestweb.custlr.models.MeasureSizeInch;
import com.bestweb.custlr.utils.MyUtils;

import java.util.ArrayList;

public class SizeAdapter extends PagerAdapter {

    private LayoutInflater layoutInflater;
    Activity activity;
    private ArrayList<MeasureSizeInch> arrayList;
    private OnSizeMeasure onSizeMeasure;
    private AlertDialog alertDialog;

    public SizeAdapter(Activity activity, ArrayList<MeasureSizeInch> arrayList, OnSizeMeasure onSizeMeasure, AlertDialog alertDialog) {
        this.activity = activity;
        this.arrayList = arrayList;
        this.onSizeMeasure = onSizeMeasure;
        this.alertDialog = alertDialog;
    }


    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        layoutInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View view = layoutInflater.inflate(R.layout.item_measure_size_layout, container, false);

        TextView txtTitle = view.findViewById(R.id.im_txtTitle);
        TextView txtLabel = view.findViewById(R.id.im_txtLabel);
        ImageView imgDrawable = view.findViewById(R.id.im_imgDrawable);
        EditText edtSize = view.findViewById(R.id.edtSize);

        txtTitle.setText(arrayList.get(position).getTitle());
        txtLabel.setText(arrayList.get(position).getLabel());
        edtSize.setHint(arrayList.get(position).getEdtHint());

        String value = arrayList.get(position).getValue();
        if (MyUtils.checkStringValue(value)) {
            edtSize.setText(value);
        }

        imgDrawable.setImageDrawable(activity.getResources().getDrawable(arrayList.get(position).getId()));


        edtSize.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    alertDialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
                    alertDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
                }
            }
        });


        edtSize.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

                int index = arrayList.get(position).getIndex();
                onSizeMeasure.onSizeMeasure(index, editable.toString());
            }
        });


        container.addView(view);

        return view;
    }

    @Override
    public int getCount() {
        return arrayList.size();
    }


    @Override
    public boolean isViewFromObject(View view, Object obj) {
        return view == obj;
    }


    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        View view = (View) object;
        container.removeView(view);
    }
}

