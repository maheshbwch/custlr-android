package com.bestweb.custlr.adapters;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bestweb.custlr.R;
import com.bestweb.custlr.interfaces.OnSizeMeasure;
import com.bestweb.custlr.models.MeasureSizeInch;

import java.util.ArrayList;

public class MeasureSizeInchAdapter extends RecyclerView.Adapter<MeasureSizeInchAdapter.MyViewHolder> {
    private ArrayList<MeasureSizeInch> arrayList;
    private Context context;
    private OnSizeMeasure onSizeMeasure;

    public MeasureSizeInchAdapter(Context context, ArrayList<MeasureSizeInch> arrayList, OnSizeMeasure onSizeMeasure) {
        this.context = context;
        this.arrayList = arrayList;
        this.onSizeMeasure = onSizeMeasure;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_measure_size_layout, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        holder.txtTitle.setText(arrayList.get(position).getTitle());
        holder.txtLabel.setText(arrayList.get(position).getLabel());
        holder.edtSize.setHint(arrayList.get(position).getEdtHint());

        holder.imgDrawable.setImageDrawable(context.getResources().getDrawable(arrayList.get(position).getId()));

    }


    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView txtTitle, txtLabel;
        public ImageView imgDrawable;
        public EditText edtSize;

        public MyViewHolder(View itemView) {
            super(itemView);

            txtTitle = itemView.findViewById(R.id.im_txtTitle);
            txtLabel = itemView.findViewById(R.id.im_txtLabel);
            imgDrawable = itemView.findViewById(R.id.im_imgDrawable);
            edtSize = itemView.findViewById(R.id.edtSize);


            edtSize.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {

                    int index = arrayList.get(getAdapterPosition()).getIndex();
                    onSizeMeasure.onSizeMeasure(index, editable.toString());
                }
            });

        }
    }
}
