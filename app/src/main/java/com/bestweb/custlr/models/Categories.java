package com.bestweb.custlr.models;

import java.util.ArrayList;

public class Categories {

    String categoryName;
    ArrayList<Subcategory> subCategoryList;

    public Categories(String categoryName, ArrayList<Subcategory> subCategoryList) {
        this.categoryName = categoryName;
        this.subCategoryList = subCategoryList;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public ArrayList<Subcategory> getSubCategoryList() {
        return subCategoryList;
    }
}
