package com.bestweb.custlr.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MainCategories {

    @SerializedName("main_cat_id")
    @Expose
    private String mainCatId;
    @SerializedName("main_cat_name")
    @Expose
    private String mainCatName;
    @SerializedName("main_cat_image")
    @Expose
    private String mainCatImage;

    public String getMainCatId() {
        return mainCatId;
    }

    public void setMainCatId(String mainCatId) {
        this.mainCatId = mainCatId;
    }

    public String getMainCatName() {
        return mainCatName;
    }

    public void setMainCatName(String mainCatName) {
        this.mainCatName = mainCatName;
    }

    public String getMainCatImage() {
        return mainCatImage;
    }

    public void setMainCatImage(String mainCatImage) {
        this.mainCatImage = mainCatImage;
    }
}
