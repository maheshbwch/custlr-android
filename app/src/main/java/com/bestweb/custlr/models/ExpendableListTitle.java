package com.bestweb.custlr.models;

import java.util.ArrayList;

public class ExpendableListTitle {

    private int id;
    private String title;
    private ArrayList<ExpendableListChildItem> subTitleList;


    public ExpendableListTitle(int id, String title, ArrayList<ExpendableListChildItem> subTitleList) {
        this.id = id;
        this.title = title;
        this.subTitleList = subTitleList;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ArrayList<ExpendableListChildItem> getSubTitle() {
        return subTitleList;
    }

    public void setSubTitle(ArrayList<ExpendableListChildItem> subTitle) {
        this.subTitleList = subTitle;
    }


}
