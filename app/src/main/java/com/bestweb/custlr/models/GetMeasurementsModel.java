package com.bestweb.custlr.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;


public class GetMeasurementsModel {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("body_mesurments")
    @Expose
    private List<BodyMesurment> bodyMesurments = null;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<BodyMesurment> getBodyMesurments() {
        return bodyMesurments;
    }

    public void setBodyMesurments(List<BodyMesurment> bodyMesurments) {
        this.bodyMesurments = bodyMesurments;
    }

    public class BodyMesurment {

        @SerializedName("measurements_shirt_shoulder")
        @Expose
        private String measurementsShirtShoulder;
        @SerializedName("measurements_shirt_chest")
        @Expose
        private String measurementsShirtChest;
        @SerializedName("measurements_shirt_armSize")
        @Expose
        private String measurementsShirtArmSize;
        @SerializedName("measurements_shirt_armLength")
        @Expose
        private String measurementsShirtArmLength;
        @SerializedName("measurements_shirt_waist")
        @Expose
        private String measurementsShirtWaist;
        @SerializedName("measurements_shirt_remarks")
        @Expose
        private String measurementsShirtRemarks;
        @SerializedName("measurements_pant_hip")
        @Expose
        private String measurementsPantHip;
        @SerializedName("measurements_pant_crotch")
        @Expose
        private String measurementsPantCrotch;
        @SerializedName("measurements_pant_thigh")
        @Expose
        private String measurementsPantThigh;
        @SerializedName("measurements_pant_length")
        @Expose
        private String measurementsPantLength;
        @SerializedName("measurements_pant_knee")
        @Expose
        private String measurementsPantKnee;
        @SerializedName("measurements_pant_ankle")
        @Expose
        private String measurementsPantAnkle;

        public String getMeasurementsShirtShoulder() {
            return measurementsShirtShoulder;
        }

        public void setMeasurementsShirtShoulder(String measurementsShirtShoulder) {
            this.measurementsShirtShoulder = measurementsShirtShoulder;
        }

        public String getMeasurementsShirtChest() {
            return measurementsShirtChest;
        }

        public void setMeasurementsShirtChest(String measurementsShirtChest) {
            this.measurementsShirtChest = measurementsShirtChest;
        }

        public String getMeasurementsShirtArmSize() {
            return measurementsShirtArmSize;
        }

        public void setMeasurementsShirtArmSize(String measurementsShirtArmSize) {
            this.measurementsShirtArmSize = measurementsShirtArmSize;
        }

        public String getMeasurementsShirtArmLength() {
            return measurementsShirtArmLength;
        }

        public void setMeasurementsShirtArmLength(String measurementsShirtArmLength) {
            this.measurementsShirtArmLength = measurementsShirtArmLength;
        }

        public String getMeasurementsShirtWaist() {
            return measurementsShirtWaist;
        }

        public void setMeasurementsShirtWaist(String measurementsShirtWaist) {
            this.measurementsShirtWaist = measurementsShirtWaist;
        }

        public String getMeasurementsShirtRemarks() {
            return measurementsShirtRemarks;
        }

        public void setMeasurementsShirtRemarks(String measurementsShirtRemarks) {
            this.measurementsShirtRemarks = measurementsShirtRemarks;
        }

        public String getMeasurementsPantHip() {
            return measurementsPantHip;
        }

        public void setMeasurementsPantHip(String measurementsPantHip) {
            this.measurementsPantHip = measurementsPantHip;
        }

        public String getMeasurementsPantCrotch() {
            return measurementsPantCrotch;
        }

        public void setMeasurementsPantCrotch(String measurementsPantCrotch) {
            this.measurementsPantCrotch = measurementsPantCrotch;
        }

        public String getMeasurementsPantThigh() {
            return measurementsPantThigh;
        }

        public void setMeasurementsPantThigh(String measurementsPantThigh) {
            this.measurementsPantThigh = measurementsPantThigh;
        }

        public String getMeasurementsPantLength() {
            return measurementsPantLength;
        }

        public void setMeasurementsPantLength(String measurementsPantLength) {
            this.measurementsPantLength = measurementsPantLength;
        }

        public String getMeasurementsPantKnee() {
            return measurementsPantKnee;
        }

        public void setMeasurementsPantKnee(String measurementsPantKnee) {
            this.measurementsPantKnee = measurementsPantKnee;
        }

        public String getMeasurementsPantAnkle() {
            return measurementsPantAnkle;
        }

        public void setMeasurementsPantAnkle(String measurementsPantAnkle) {
            this.measurementsPantAnkle = measurementsPantAnkle;
        }

    }

}



