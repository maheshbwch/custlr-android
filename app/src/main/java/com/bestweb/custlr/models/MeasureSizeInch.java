package com.bestweb.custlr.models;

public class MeasureSizeInch {

    private int id;
    private String title;
    private String label;
    private String edtHint;
    private int index;
    private String value;

    public MeasureSizeInch(int id, String title, String label, String edtHint, int index, String value) {
        this.id = id;
        this.title = title;
        this.label = label;
        this.edtHint = edtHint;
        this.index = index;
        this.value = value;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getEdtHint() {
        return edtHint;
    }

    public void setEdtHint(String edtHint) {
        this.edtHint = edtHint;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }


}
