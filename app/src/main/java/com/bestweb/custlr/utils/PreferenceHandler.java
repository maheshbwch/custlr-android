package com.bestweb.custlr.utils;

import android.content.Context;
import android.content.SharedPreferences;

public class PreferenceHandler {

    public static boolean storePreference(Context context, String key, String data) {
        SharedPreferences mySharedPreferences;
        try {
            mySharedPreferences = context.getSharedPreferences(Constants.PREFS_NAME, 0);
            SharedPreferences.Editor editor = mySharedPreferences.edit();
            editor.putString(key, data);
            editor.commit();
        } catch (Exception e) {
            System.out.println(e);
            return false;
        }
        return true;
    }

    public static boolean storePreference(Context context, String key, int data) {
        SharedPreferences mySharedPreferences;
        try {
            mySharedPreferences = context.getSharedPreferences(Constants.PREFS_NAME, 0);
            SharedPreferences.Editor editor = mySharedPreferences.edit();
            editor.putInt(key, data);
            editor.commit();
        } catch (Exception e) {
            System.out.println(e);
            return false;
        }
        return true;
    }

    public static String getPreferenceFromString(Context context, String key) {
        String data;
        SharedPreferences mySharedPreferences;
        try {
            mySharedPreferences = context.getSharedPreferences(Constants.PREFS_NAME, 0);
            data = mySharedPreferences.getString(key, null);
        } catch (Exception e) {
            return null;
        }
        return data;
    }

    public static boolean getPreferenceFromBoolean(Context context, String key) {
        boolean data;
        SharedPreferences mySharedPreferences;
        try {
            mySharedPreferences = context.getSharedPreferences(Constants.PREFS_NAME, 0);
            data = mySharedPreferences.getBoolean(key, false);
        } catch (Exception e) {
            return false;
        }
        return data;
    }

    public static int getPreferenceFromInt(Context context, String key) {
        int data;
        SharedPreferences mySharedPreferences;
        try {
            mySharedPreferences = context.getSharedPreferences(Constants.PREFS_NAME, 0);
            data = mySharedPreferences.getInt(key, 0);
            //data = mySharedPreferences.getString(key, "");
        } catch (Exception e) {
            return 0;
        }
        return data;
    }

    public static boolean storeBooleanValue(Context context, String key, Boolean value) {
        SharedPreferences preferences = context.getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(key, value);
        editor.commit();
        return true;
    }


    public static boolean clearPreferenceData(Context context, String key) {
        SharedPreferences mySharedPreferences;
        String data = null;
        try {
            mySharedPreferences = context.getSharedPreferences(Constants.PREFS_NAME, 0);
            SharedPreferences.Editor editor = mySharedPreferences.edit();
            data = mySharedPreferences.getString(key, data);
            editor.clear();
            editor.commit();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public static boolean clearPreferenceParticularData(Context context, String key) {
        SharedPreferences settings = context.getSharedPreferences(Constants.PREFS_NAME, 0);
        if (settings.contains(key)) {
            SharedPreferences.Editor editor = settings.edit();
            editor.remove(key);
            editor.commit();
        }
        return true;
    }

    public static boolean ClearPreferences(Context context) {
        SharedPreferences mySharedPreferences;
        try {
            mySharedPreferences = context.getSharedPreferences(Constants.PREFS_NAME, 0);
            SharedPreferences.Editor editor = mySharedPreferences.edit();
            editor.clear();
            editor.commit();
        } catch (Exception e) {
            return false;
        }
        return true;
    }
}
