package com.bestweb.custlr.utils;

import java.util.ArrayList;
import java.util.List;

public class Constants {

    public static final String IS_FROM = "is_from";
    public static final String IS_FROM_MEASUREMENT = "measurement_page";
    public static final String IS_FROM_A4FIT = "a4fit_page";
    public static final String IS_FROM_CART_PAGE = "cart_page";

    public static final String WHATSAPP_NUMBER = "+60123900643"; //TODO REMOVE THE NUMBER

    public static final String SHOULDER = "shoulder";
    public static final String CHEST = "chest";
    public static final String ARM = "arm";
    public static final String ARM_LENGTH = "arm_length";
    public static final String WAIST = "waist";

    public static final String SIZE = "size";

    public static final String REMARKS = "remarks";


    public static final String STYLE_MAP = "style_map";

    public static final String HEIGHT_WEIGHT = "height_weight";

    public static final String WEIGHT = "weight";
    public static final String HEIGHT = "height";

    public static final String IMAGE_FRONT = "image_front";
    public static final String IMAGE_SIDE = "image_side";

    public static final String CATEGORIES_JSON = "categories_json";
    public static final String PRODUCT_ID = "product_id";
    public static final String PRODUCT_NAME = "product_name";
    public static final String PRODUCT_PRICE = "product_price";
    public static final String PRODUCT_IMAGE_URL = "product_image_url";
    public static final String FEATURED_IMAGE_URL = "featured_image";
    public static final String STYLE_JSON_ARRAY = "style_json_array";
    public static final String MEASUREMENT_VALUES = "measurement_values";
    public static final String SELECT = "select";
    public static final String UPLOAD = "upload";


    public static final String BODY_MEASURE_POST = "body_measure_post";
    public static final String A4FIT_MEASURE_POST = "a4fit_measure_post";
    public static final String STANDARD_SIZE_POST = "standard_size_post";

    public static final String PRODUCT_REGULAR_PRICE = "product_regular_price";
    public static final String PRODUCT_SALE_PRICE = "product_sale_price";

    public static final String PREFS_NAME = "pref_name";
    public static final String USER_LOGIN = "USER_LOGIN";

    public static final String UPLOAD_URL = "upload_url";

    public static final int PICK_IMAGE_FROM_FILES = 1001;
    public static final int CAPTURE_IMAGE_FROM_CAMERA = 1002;

    public static final int WRITE_STORAGE_FILES_PERMISSION = 2002;
    public static final int WRITE_STORAGE_CAMERA_PERMISSION = 2003;

    public static final int LOGIN_CODE = 1011;
    public static final int LOGIN_SUCCESS = 1012;
    public static final int LOGIN_FAILED = 1013;
    public static final int PRODUCTS = 1014;

    public static final int GOOGLE_SIGN_IN = 1015;

    public static final int SUCCESS_START = 200;
    public static final int SUCCESS_END = 300;
    public static final int NETWORK_ERR = 0;
    public static final int UNAUTHORIZED_ERR = 401;
    public static final int SERVER_ERR_START = 500;
    public static final int SERVER_ERR_END = 600;
    public static final int CLIENT_ERR_START = 400;
    public static final int CLIENT_ERR_END = 500;


    public static final String MyPREFERENCES = "com.bestweb.custlr"; // Add your package name

    public static final String DEVICE_TYPE = "2";

    public static final String FCM_TOKEN = "fcm_token";

    public static List<String> CheckoutURL = new ArrayList<>();

    public static final String SUCCESS = "success";
    public static final String ERROR = "error";

    public static final String DIALOG_SUCCESS = "0";
    public static final String DIALOG_FAILURE = "1";
    public static final String DIALOG_WARNING = "2";
    public static final String DIALOG_GENERAL = "3";


    public static final int SHOULDER_INDEX = 0;
    public static final int CHEST_INDEX = 1;
    public static final int ARM_INDEX = 2;
    public static final int ARM_LENGTH_INDEX = 3;
    public static final int WAIST_INDEX = 4;

    public static final String APP_VERSION_NAME = "app_version_name";
    public static final String APP_VERSION_CODE = "app_version_code";
    public static final String APP_IS_FORCE_UPDATE = "is_force_update";
    public static final String APP_UPDATE_CONTENT = "app_update_content";

    public static final String ERROR_REMOTE_CONFIG = "Unable to proceed further,please try again later";


}
