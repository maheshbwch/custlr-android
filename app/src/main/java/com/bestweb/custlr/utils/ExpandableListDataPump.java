package com.bestweb.custlr.utils;

import com.bestweb.custlr.R;
import com.bestweb.custlr.models.ExpendableListChildItem;
import com.bestweb.custlr.models.ExpendableListTitle;

import java.util.ArrayList;

public class ExpandableListDataPump {


    public static ArrayList<ExpendableListTitle> getData() {


        ArrayList<ExpendableListTitle> expendableListTitles = new ArrayList<>();

        ArrayList<ExpendableListChildItem> collar = new ArrayList<>();
        collar.add(new ExpendableListChildItem(R.drawable.collar_classic, "Default"));
        collar.add(new ExpendableListChildItem(R.drawable.collar_classic, "Classic"));
        collar.add(new ExpendableListChildItem(R.drawable.collar_button_down, "Button Down"));
        collar.add(new ExpendableListChildItem(R.drawable.collar_mandarian, "Mandarian"));
        collar.add(new ExpendableListChildItem(R.drawable.collar_italian, "Italian"));
        collar.add(new ExpendableListChildItem(R.drawable.collar_small_button_down, "Small Button Down"));

        ArrayList<ExpendableListChildItem> cuff = new ArrayList<>();
        cuff.add(new ExpendableListChildItem(R.drawable.button_1, "Default"));
        cuff.add(new ExpendableListChildItem(R.drawable.button_1, "1 Button"));
        cuff.add(new ExpendableListChildItem(R.drawable.button_2, "2 Button"));
        cuff.add(new ExpendableListChildItem(R.drawable.button_french, "French"));
        cuff.add(new ExpendableListChildItem(R.drawable.button_napolean, "Napolean"));

        ArrayList<ExpendableListChildItem> sleeve = new ArrayList<>();
        sleeve.add(new ExpendableListChildItem(R.drawable.sleeve_long, "Default"));
        sleeve.add(new ExpendableListChildItem(R.drawable.sleeve_short, "Short"));
        sleeve.add(new ExpendableListChildItem(R.drawable.sleeve_long, "Long"));

        ArrayList<ExpendableListChildItem> fitting = new ArrayList<>();
        fitting.add(new ExpendableListChildItem(R.drawable.fit_normal, "Default"));
        fitting.add(new ExpendableListChildItem(R.drawable.fit_slim, "Slim"));
        fitting.add(new ExpendableListChildItem(R.drawable.fit_normal, "Normal"));
        fitting.add(new ExpendableListChildItem(R.drawable.fit_loose, "Loose"));

        ArrayList<ExpendableListChildItem> check_pocket = new ArrayList<>();
        check_pocket.add(new ExpendableListChildItem(R.drawable.chest_pocket_no, "Default"));
        check_pocket.add(new ExpendableListChildItem(R.drawable.chest_pocket_no, "No"));
        check_pocket.add(new ExpendableListChildItem(R.drawable.chest_pocket_1, "One Pocket"));
        check_pocket.add(new ExpendableListChildItem(R.drawable.chest_pocket_2, "Two Pocket"));


        ArrayList<ExpendableListChildItem> shoulder = new ArrayList<>();
        shoulder.add(new ExpendableListChildItem(R.drawable.style_collar, "Default"));


        ArrayList<ExpendableListChildItem> chest = new ArrayList<>();
        chest.add(new ExpendableListChildItem(R.drawable.style_collar, "Default"));


        ArrayList<ExpendableListChildItem> armSize = new ArrayList<>();
        armSize.add(new ExpendableListChildItem(R.drawable.style_collar, "Default"));


        ArrayList<ExpendableListChildItem> armLength = new ArrayList<>();
        armLength.add(new ExpendableListChildItem(R.drawable.style_collar, "Default"));


        ArrayList<ExpendableListChildItem> waistSize = new ArrayList<>();
        waistSize.add(new ExpendableListChildItem(R.drawable.style_collar, "Default"));


        ArrayList<ExpendableListChildItem> height = new ArrayList<>();
        height.add(new ExpendableListChildItem(R.drawable.style_collar, "Default"));


        ArrayList<ExpendableListChildItem> weight = new ArrayList<>();
        weight.add(new ExpendableListChildItem(R.drawable.style_collar, "Default"));


        ArrayList<ExpendableListChildItem> remark = new ArrayList<>();
        remark.add(new ExpendableListChildItem(R.drawable.style_collar, "Default"));

        ArrayList<ExpendableListChildItem> size = new ArrayList<>();
        size.add(new ExpendableListChildItem(R.drawable.style_collar, "Default"));


        ArrayList<ExpendableListChildItem> frontA4 = new ArrayList<>();
        frontA4.add(new ExpendableListChildItem(R.drawable.style_collar, "Default"));


        expendableListTitles.add(new ExpendableListTitle(R.drawable.style_collar, "Collar", collar));
        expendableListTitles.add(new ExpendableListTitle(R.drawable.style_cuff, "Cuff", cuff));
        expendableListTitles.add(new ExpendableListTitle(R.drawable.style_sleeve, "Sleeve", sleeve));
        expendableListTitles.add(new ExpendableListTitle(R.drawable.style_fitting, "Fitting", fitting));
        expendableListTitles.add(new ExpendableListTitle(R.drawable.style_chest_pocket, "Chest Pocket", check_pocket));
        expendableListTitles.add(new ExpendableListTitle(R.drawable.style_shoulder, "Shoulder", shoulder));
        expendableListTitles.add(new ExpendableListTitle(R.drawable.style_chest, "Chest", chest));

        expendableListTitles.add(new ExpendableListTitle(R.drawable.style_collar, "Arm Size", armSize));
        expendableListTitles.add(new ExpendableListTitle(R.drawable.style_collar, "Arm Length", armLength));
        expendableListTitles.add(new ExpendableListTitle(R.drawable.style_collar, "Waist Size", waistSize));
        expendableListTitles.add(new ExpendableListTitle(R.drawable.style_collar, "Height", height));
        expendableListTitles.add(new ExpendableListTitle(R.drawable.style_collar, "Weight", weight));
        expendableListTitles.add(new ExpendableListTitle(R.drawable.style_collar, "Remark", remark));
        expendableListTitles.add(new ExpendableListTitle(R.drawable.style_collar, "Size", size));
        expendableListTitles.add(new ExpendableListTitle(R.drawable.style_collar, "Front A4 Image", frontA4));

        return expendableListTitles;
    }
}