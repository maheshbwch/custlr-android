package com.bestweb.custlr.interfaces;

public interface OnSizeMeasure {

    void onSizeMeasure(int index, String value);

}
