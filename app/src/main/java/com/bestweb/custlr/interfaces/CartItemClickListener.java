package com.bestweb.custlr.interfaces;

public interface CartItemClickListener {

    void onDeleteCartItem(int position, String productId);

    void onCartItemClicked(int position);

    void onEditClicked();

}
