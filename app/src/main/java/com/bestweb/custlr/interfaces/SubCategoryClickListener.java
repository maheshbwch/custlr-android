package com.bestweb.custlr.interfaces;

public interface SubCategoryClickListener {

    void onProductClicked(String productId, String productName, String productSalePrice, String productRegularPrice);

}
