package com.bestweb.custlr.interfaces;

public interface UploadAlertListener {
    void onChooseFileClicked();

    void onCaptureCameraClicked();

}