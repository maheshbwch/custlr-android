package com.bestweb.custlr.interfaces;

public interface CustomAlertInterface {

    void onPositiveButtonClicked();

    void onNegativeButtonClicked();

}
