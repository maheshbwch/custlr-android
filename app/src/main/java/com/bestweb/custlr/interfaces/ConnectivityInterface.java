package com.bestweb.custlr.interfaces;

public interface ConnectivityInterface {
    void onConnected();
    void onNotConnected();
}
