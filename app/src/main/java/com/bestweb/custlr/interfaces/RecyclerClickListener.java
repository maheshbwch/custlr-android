package com.bestweb.custlr.interfaces;

public interface RecyclerClickListener {
    void onItemClicked(int position);
}
