package com.bestweb.custlr.interfaces;

public interface CatSelectionListener {
    void onCategorySelected(int position);
}
