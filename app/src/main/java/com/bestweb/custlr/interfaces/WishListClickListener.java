package com.bestweb.custlr.interfaces;

public interface WishListClickListener {

    void onDeleteWishList(int position, String productId);

    void onWishListItemClicked(int position);
}
