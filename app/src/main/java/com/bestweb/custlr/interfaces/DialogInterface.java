package com.bestweb.custlr.interfaces;

public interface DialogInterface {
    void onButtonClicked();

    void onDismissedClicked();
}
